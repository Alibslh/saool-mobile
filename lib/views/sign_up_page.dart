import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:saool/models/sign_up_model.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/user_view_model.dart';
import 'package:saool/widgets/form_submit_button.dart';
import 'package:saool/widgets/icon_heading.dart';
import 'package:saool/widgets/onboarding_view.dart';
import 'package:saool/widgets/outlined_text_form_field.dart';

import 'home_page.dart';
import 'sign_in_page.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  SignUpModel _signUpModel =
      new SignUpModel(username: '', email: '', password: '');
  final _formKey = new GlobalKey<FormState>();
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Form(
          key: _formKey,
          child: OnboardingView(
            formTitle: 'حساب جديد',
            children: [
              OutlinedTextFormField(
                hintText: 'اسم_المستخدم |',
                onChanged: (String newVal) {
                  _signUpModel.username = newVal;
                },
                validator: (String? val) {
                  if (val == null || val.isEmpty) {
                    return 'Field cannot be empty';
                  }
                },
                prefixIcon: Icon(
                  Icons.person_outline,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              OutlinedTextFormField(
                hintText: 'البريد الإلكتروني',
                onChanged: (String newVal) {
                  _signUpModel.email = newVal;
                },
                validator: (String? val) {
                  if (val == null || val.isEmpty) {
                    return 'Field cannot be empty';
                  }
                  if (!EmailValidator.validate(val)) {
                    return 'Email is not valid';
                  }
                },
                prefixIcon: Icon(Icons.alternate_email),
              ),
              SizedBox(
                height: 16,
              ),
              OutlinedTextFormField(
                hintText: 'كلمة المرور',
                obscureText: _obscureText,
                onChanged: (String newVal) {
                  _signUpModel.password = newVal;
                },
                validator: (String? val) {
                  if (val == null || val.isEmpty) {
                    return 'Field cannot be empty';
                  }
                  if (val.length < 6) {
                    return 'Password length must be greater than 6';
                  }
                },
                prefixIcon: Icon(Icons.lock),
                suffixIcon: GestureDetector(
                  onTap: () {
                    setState(() {
                      _obscureText = !_obscureText;
                    });
                  },
                  child: Icon(
                    Icons.remove_red_eye,
                    color: Colors.grey[300],
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              FormSubmitButton(
                title: 'تسجيل',
                onPressed: () async {
                  if (!_formKey.currentState!.validate()) {
                    return;
                  }
                  print(_signUpModel.toMap());
                  bool res = await StaticProvider.of<UserViewModel>(context)
                      .signUp(context, _signUpModel);
                  if (res) {
                    StaticProvider.of<UserViewModel>(context)
                        .initialize(context)
                        .then(
                          (_) => Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                builder: (context) => HomePage(),
                              ),
                              (route) => false),
                        );
                  }
                },
              ),
            ],
            iconHeading: IconHeading(
              title: 'عندك حساب؟',
              iconPath: 'assets/gift.png',
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => SignInPage())),
            ),
          ),
        ),
      ),
    );
  }
}
