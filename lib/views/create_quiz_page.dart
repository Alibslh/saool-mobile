import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:saool/constants/custom_styles.dart';
import 'package:saool/views/add_question_page.dart';
import 'package:saool/views/create_quiz_step_one_page.dart';
import 'package:saool/widgets/custom_back_button.dart';

class CreateQuizPage extends StatefulWidget {
  final bool forQuiz;

  const CreateQuizPage({this.forQuiz = true});

  @override
  _CreateQuizPageState createState() => _CreateQuizPageState();
}

class _CreateQuizPageState extends State<CreateQuizPage> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
                left: 24,
                right: 24,
                top: MediaQuery.of(context).viewPadding.top + 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomBackButton(),
                          SvgPicture.asset(
                            'assets/logo-2.svg',
                            width: 83,
                            height: 32,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Text(
                        'اختر نوع الاختبار الذي تود إضافته:',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Divider(
                    color: Colors.grey[400],
                  ),
                ),
                GestureDetector(
                  onTap: () => widget.forQuiz
                      ? Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CreateQuizStepOnePage(
                              isCategorical: false,
                            ),
                          ),
                        )
                      : Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => AddQuestionPage(
                              isCategorical: false,
                            ),
                          ),
                        ),
                  child: Container(
                    decoration: CustomStyles.roundShadowBoxDecoration(),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 24.0,
                        right: 24.0,
                        top: 30.0,
                        bottom: 33.0,
                      ),
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Expanded(
                                  child: SvgPicture.asset('assets/high-score.svg')),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 16.0,
                                  vertical: 8.0,
                                ),
                                child: Text(
                                  'اختبار أعلى درجة',
                                  style: TextStyle(
                                    fontSize: 28,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12.0),
                            child: Column(
                              children: [
                                Text(
                                  'النوع هذا تتحدى فيه مين يقدر يجيب الدرجة الكاملة.',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                                Text(
                                  'مثال: “تقدر تجيب الدرجة الكاملة في اختبار ون بيس؟"',
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: CustomStyles.secondaryTextColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                GestureDetector(
                  onTap: () => widget.forQuiz
                      ? Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CreateQuizStepOnePage(
                              isCategorical: true,
                            ),
                          ),
                        )
                      : Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => AddQuestionPage(
                              isCategorical: true,
                            ),
                          ),
                        ),
                  child: Container(
                    decoration: CustomStyles.roundShadowBoxDecoration(),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 24.0,
                        right: 24.0,
                        top: 30.0,
                        bottom: 33.0,
                      ),
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              SvgPicture.asset('assets/mask.svg'),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 16.0,
                                  vertical: 8.0,
                                ),
                                child: Text(
                                  'اختبار تصنيفي',
                                  style: TextStyle(
                                    fontSize: 28,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12.0),
                            child: Column(
                              children: [
                                Text(
                                  'النوع هذا تحط فيه نتائج مختلفة والنتيجة على حسب الجواب',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                                Text(
                                  'مثال: “جربي حظك وشوفي مين زوجك المستقبلي"',
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: CustomStyles.secondaryTextColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
