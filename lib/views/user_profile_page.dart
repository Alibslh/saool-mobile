import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:saool/constants/custom_styles.dart';
import 'package:saool/models/category_model.dart';
import 'package:saool/models/label.dart';
import 'package:saool/models/quiz_model.dart';
import 'package:saool/models/user_model.dart';
import 'package:saool/models/user_permissions_user.dart';
import 'package:saool/utils/common_utils.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/quiz_view_model.dart';
import 'package:saool/view_models/user_view_model.dart';
import 'package:saool/views/create_quiz_page.dart';
import 'package:saool/views/quiz_overview_page.dart';
import 'package:saool/widgets/category_grid_item.dart';
import 'package:saool/widgets/category_title.dart';
import 'package:saool/widgets/custom_back_button.dart';
import 'package:saool/widgets/home_category_container.dart';
import 'package:url_launcher/url_launcher.dart';

import 'home_page.dart';
import 'profile_settings_page.dart';

class UserProfilePage extends StatefulWidget {
  final UserPermissionsUser? userPermissionsUser;

  const UserProfilePage({this.userPermissionsUser});
  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  late bool isPublic;
  static const double publicProfileHeightDiff = 0.1;

  @override
  void initState() {
    super.initState();
    isPublic = widget.userPermissionsUser != null;
    if (isPublic) {
      var userVM = StaticProvider.of<UserViewModel>(context);
      WidgetsBinding.instance!.addPostFrameCallback((_) {
        userVM.getPublicUserProfile(
            context, widget.userPermissionsUser!.id, userVM.userModel!.jwt);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Consumer<UserViewModel>(
        builder: (context, userVM, _) => Scaffold(
          body: Stack(
            children: [
              (isPublic && userVM.publicUserModel == null)
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : SingleChildScrollView(
                      child: Stack(
                        children: [
                          _buildTopView(context, userVM),
                          _buildMainView(context, userVM),
                        ],
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTopView(BuildContext context, UserViewModel userVM) {
    return Stack(
      children: [
        Opacity(
          opacity: 0.45,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height *
                (0.47 - (!isPublic ? 0.0 : publicProfileHeightDiff)),
            child: buildProfileImage(userVM),
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            top: MediaQuery.of(context).viewPadding.top +
                (MediaQuery.of(context).size.height * 0.05),
            bottom: 36.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 28.0),
                child: CustomBackButton(),
              ),
              SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 28.0),
                child: SvgPicture.asset(
                  'assets/logo-2.svg',
                  width: 83,
                ),
              ),
              SizedBox(
                height: 8.0,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'مرحبا ${(isPublic ? userVM.getPublicUserName() : userVM.getUserName()) ?? ''} 👋',
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 14,
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.only(bottom: 14.0),
                    //   child: Text(
                    //     '@_XxSniperScooperxX_@',
                    //     style: TextStyle(
                    //         fontSize: 14, fontWeight: FontWeight.bold),
                    //   ),
                    // ),
                    // TODO: Get and show actual total points of user from backend
                    // !isPublic && userVM.userModel != null
                    //     ? Padding(
                    //         padding: const EdgeInsets.only(bottom: 14.0),
                    //         child: Row(
                    //           children: [
                    //             Text(
                    //               'مجموع النقاط :',
                    //               style: TextStyle(
                    //                   fontSize: 14,
                    //                   fontWeight: FontWeight.bold),
                    //             ),
                    //             SizedBox(
                    //               width: 5,
                    //             ),
                    //             Text(
                    //               '4,456',
                    //               style: TextStyle(
                    //                   fontSize: 14,
                    //                   fontWeight: FontWeight.bold),
                    //             ),
                    //           ],
                    //         ),
                    //       )
                    //     : Container(),
                    // TODO: Get and show actual values
                    userVM.userModel != null
                        ? Row(
                            children: [
                              Text(
                                'التفاعل:',
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                '١ مختبر',
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                            ],
                          )
                        : Container(),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 28.0),
                child: Column(
                  children: [
                    Row(
                      children: [
                        !isPublic
                            ? RoundedContainer(
                                text: 'اختبارات سابقة',
                                iconText: '📖',
                                onPressed: () {},
                              )
                            : _buildSocialMediaIcons(
                                context,
                                isPublic
                                    ? userVM.publicUserModel
                                    : userVM.userModel),
                        SizedBox(
                          width: 16.0,
                        ),
                        !isPublic && userVM.userModel != null
                            ? RoundedContainer(
                                text: 'خروج',
                                onPressed: () {
                                  userVM.signOut(context).then((success) {
                                    if (success) {
                                      Navigator.of(context).pushAndRemoveUntil(
                                          MaterialPageRoute(
                                            builder: (context) => HomePage(),
                                          ),
                                          (route) => false);
                                    }
                                  });
                                },
                              )
                            : Container(),
                        // RoundedContainer(
                        //   text: 'الأوسمة',
                        //   iconText: '🏅',
                        //   onPressed: () {},
                        // ),
                      ],
                    ),
                    !isPublic
                        ? Padding(
                            padding: const EdgeInsets.only(top: 16.0),
                            child: Row(
                              children: [
                                RoundedContainer(
                                  text: 'تعديل الملف',
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ProfileSettingsPage()));
                                  },
                                  color:
                                      Theme.of(context).scaffoldBackgroundColor,
                                ),
                                SizedBox(
                                  width: 16.0,
                                ),
                                _buildSocialMediaIcons(
                                    context,
                                    isPublic
                                        ? userVM.publicUserModel
                                        : userVM.userModel),
                              ],
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
              SizedBox(
                height: 24,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Image buildProfileImage(UserViewModel userVM) {
    String? publicUserImageUrl =
        userVM.publicUserModel?.userPermissionsUser?.profileImageUrl;
    String? authUserImageUrl =
        userVM.userModel?.userPermissionsUser?.profileImageUrl;
    var dummyImage = Image.asset(
      'assets/profile-photo.png',
      fit: BoxFit.cover,
    );

    if (isPublic && (publicUserImageUrl != null))
      return Image.network(
        CommonUtils.toHostUrl(publicUserImageUrl),
        fit: BoxFit.cover,
      );
    if (!isPublic && (authUserImageUrl != null))
      return Image.network(
        CommonUtils.toHostUrl(authUserImageUrl),
        fit: BoxFit.cover,
      );
    return dummyImage;
  }

  Container _buildSocialMediaIcons(BuildContext context, UserModel? userModel) {
    return Container(
      child: Wrap(
        children: [
          GestureDetector(
            onTap: () async {
              if (userModel?.userPermissionsUser?.twitter != null) {
                String twitterUrl = 'https://twitter.com/' +
                    userModel!.userPermissionsUser!.twitter!;
                print(twitterUrl);
                await canLaunch(twitterUrl)
                    ? await launch(twitterUrl)
                    : throw 'Could not launch $twitterUrl';
              }
            },
            child: RoundedIcon(
              imagePath: 'assets/twitter.png',
            ),
          ),
          SizedBox(
            width: 4,
          ),
          GestureDetector(
            onTap: () async {
              if (userModel?.userPermissionsUser?.instagram != null) {
                String instagramUrl = 'https://www.instagram.com/' +
                    userModel!.userPermissionsUser!.instagram!;
                print(instagramUrl);
                await canLaunch(instagramUrl)
                    ? await launch(instagramUrl)
                    : throw 'Could not launch $instagramUrl';
              }
            },
            child: RoundedIcon(
              imagePath: 'assets/instagram.png',
            ),
          ),
          SizedBox(
            width: 4,
          ),
          GestureDetector(
            onTap: () async {
              if (userModel?.userPermissionsUser?.snapchat != null) {
                String snapchatUrl = 'https://www.snapchat.com/add/' +
                    userModel!.userPermissionsUser!.snapchat!;
                print(snapchatUrl);
                await canLaunch(snapchatUrl)
                    ? await launch(snapchatUrl)
                    : throw 'Could not launch $snapchatUrl';
              }
            },
            child: RoundedIcon(
              imagePath: 'assets/snapchat.png',
            ),
          ),
          SizedBox(
            width: 4,
          ),
          GestureDetector(
            onTap: () async {
              if (userModel?.userPermissionsUser?.facebook != null) {
                String facebookUrl = 'https://www.facebook.com/' +
                    userModel!.userPermissionsUser!.facebook!;
                print(facebookUrl);
                await canLaunch(facebookUrl)
                    ? await launch(facebookUrl)
                    : throw 'Could not launch $facebookUrl';
              }
            },
            child: RoundedIcon(
              imagePath: 'assets/facebook-2.png',
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildMainView(BuildContext context, UserViewModel userVM) {
    return Padding(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height *
            (0.48 - (!isPublic ? 0.0 : publicProfileHeightDiff)),
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(14),
            topRight: Radius.circular(14),
          ),
        ),
        padding: EdgeInsets.symmetric(vertical: 24.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                'اختباراتي ✍️',
                style: CustomStyles.boldHeadingText().copyWith(fontSize: 28.0),
              ),
            ),
            Consumer<QuizViewModel>(
              builder: (context, quizVM, _) {
                if (userVM.userModel == null) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                final List<QuizModel> newQuizzes = quizVM.quizzes
                        ?.where((q) =>
                            q.userId ==
                            (isPublic
                                ? userVM.publicUserModel!.id
                                : userVM.userModel!.id))
                        .toList() ??
                    [];
                List<CategoryModel>? categories =
                    quizVM.getQuizCategories(newQuizzes);
                if (categories == null) {
                  return Container();
                }
                return SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: categories
                          .asMap()
                          .map(
                            (i, e) => MapEntry(
                                i,
                                Column(
                                  children: [
                                    i == 1
                                        ? ProfileAddQuizButton()
                                        : Container(),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: i == 1 ? 0.0 : 24.0),
                                      child: CategoryTitle(
                                        title: e.name,
                                      ),
                                    ),
                                    // TODO: Use users_permissions_user here
                                    userVM.userModel?.id != null
                                        ? _buildCategoryTileGrid(
                                            newQuizzes
                                                .where((q) =>
                                                    q.category.id == e.id)
                                                .toList(),
                                          )
                                        : Container(),
                                  ],
                                )),
                          )
                          .values
                          .toList(),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> getCategoriesWidgets(
      QuizViewModel quizVM, List<CategoryModel>? categories) {
    if (categories == null || categories.isEmpty) {
      return [
        Center(
          child: Text('لم يتم العثور على اختبارات'),
        )
      ];
    }

    List<Widget> list = [];
    Widget first = HomeCategoryContainer(
      title: categories[0].name,
      isDense: false,
      imageUrl: quizVM.getCategoryImageUrl(categories[0].id),
      id: categories[0].id,
      onTap: () => _handleFAB(),
    );

    Widget second = Container();
    if (categories.length >= 2) {
      second = HomeCategoryContainer(
        title: categories[1].name,
        isDense: false,
        imageUrl: quizVM.getCategoryImageUrl(categories[1].id),
        id: categories[1].id,
        onTap: () => _handleFAB(),
      );
    }
    list.add(
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [first, second],
      ),
    );
    list.add(
      SizedBox(height: 15),
    );

    for (int i = 2; i < categories.length; i = i + 3) {
      Widget? e1, e2, e3;
      e1 = e2 = e3 = Container(
        width: MediaQuery.of(context).size.width * 0.283,
        height: MediaQuery.of(context).size.height * 0.113,
      );
      var cat = categories[i];
      e1 = HomeCategoryContainer(
        title: cat.name,
        isDense: true,
        imageUrl: quizVM.getCategoryImageUrl(cat.id),
        id: cat.id,
        onTap: () => _handleFAB(),
      );
      if ((i + 1) < categories.length) {
        var cat = categories[i + 1];
        e2 = HomeCategoryContainer(
          title: cat.name,
          isDense: true,
          imageUrl: quizVM.getCategoryImageUrl(cat.id),
          id: cat.id,
          onTap: () => _handleFAB(),
        );
      }
      if ((i + 2) < categories.length) {
        var cat = categories[i + 2];
        e3 = HomeCategoryContainer(
          title: cat.name,
          isDense: true,
          imageUrl: quizVM.getCategoryImageUrl(cat.id),
          id: cat.id,
          onTap: () => _handleFAB(),
        );
      }
      List<Widget> rowChildren = [];
      rowChildren.add(e1);
      rowChildren.add(e2);
      rowChildren.add(e3);

      list.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: rowChildren,
      ));
      list.add(
        SizedBox(height: 15),
      );
    }

    return list;
  }

  void _handleFAB() {
    // setState(() {
    //   showOverlay = false;
    // });
  }

  Padding _buildCategoryTileGrid(List<QuizModel> catQuizzes) {
    List<Widget> list = [];
    for (int i = 0; i < catQuizzes.length; i = i + 2) {
      var first = catQuizzes[i];
      QuizModel? second;
      if ((i + 1) < catQuizzes.length) {
        second = catQuizzes[i + 1];
      }
      list.add(
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CategoryGridItem(
                title: first.title,
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => QuizOverviewPage(
                            quizId: first.id.toString(),
                          )));
                },
                labels: [
                  Label(
                    title: 'تحدي أعلى نتيجة',
                    backgroundColor: Color(0xFFFED766),
                    textColor: Colors.black,
                  ),
                ],
                imageUrl: CommonUtils.toHostUrl(first.imageUrl),
                type: first.type,
                loadingAssetPath: first.loadingAssetPath,
              ),
              second != null
                  ? CategoryGridItem(
                      title: second.title,
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => QuizOverviewPage(
                                  quizId: second!.id.toString(),
                                )));
                      },
                      labels: [
                        Label(
                          title: 'تحدي التصنيف',
                          backgroundColor: Color(0xFF2AB7CA),
                          textColor: Colors.white,
                        ),
                      ],
                      imageUrl: CommonUtils.toHostUrl(second.imageUrl),
                      type: second.type,
                      loadingAssetPath: second.loadingAssetPath,
                    )
                  : Container(),
            ],
          ),
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(children: list),
    );
  }
}

class ProfileAddQuizButton extends StatelessWidget {
  const ProfileAddQuizButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => CreateQuizPage())),
      child: Stack(
        children: [
          Image.asset('assets/profile-quiz-add-icon-group.png'),
          Positioned(
            top: 32.0,
            right: 44.0,
            child: Container(
              width: MediaQuery.of(context).size.width * 0.6,
              child: Text(
                'أنشئ اختبارك الخاص على سؤول من هنا 🪄',
                style: CustomStyles.extraBoldText(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class RoundedIcon extends StatelessWidget {
  const RoundedIcon({
    Key? key,
    required this.imagePath,
  }) : super(key: key);
  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.04,
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Image.asset(imagePath),
    );
  }
}

class RoundedContainer extends StatelessWidget {
  const RoundedContainer({
    Key? key,
    required this.text,
    required this.onPressed,
    this.color,
    this.iconText,
  }) : super(key: key);

  final String text;
  final Function() onPressed;
  final Color? color;
  final String? iconText;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: color == null
              ? LinearGradient(
                  colors: [
                    CustomStyles.tabBorderColor,
                    Color(0xFFFEB666),
                  ],
                )
              : null,
          color: color,
        ),
        width: MediaQuery.of(context).size.width * 0.4,
        height: MediaQuery.of(context).size.height * 0.05,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                text,
                style: CustomStyles.boldSmallText(),
              ),
              iconText != null
                  ? Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: Text(
                        iconText!,
                        style: TextStyle(fontSize: 22),
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
