import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:saool/constants/custom_styles.dart';
import 'package:saool/models/category_model.dart';
import 'package:saool/models/label.dart';
import 'package:saool/models/quiz_model.dart';
import 'package:saool/utils/common_utils.dart';
import 'package:saool/view_models/quiz_view_model.dart';
import 'package:saool/views/quiz_overview_page.dart';
import 'package:saool/widgets/category_grid_item.dart';
import 'package:saool/widgets/category_title.dart';
import 'package:saool/widgets/custom_back_button.dart';
import 'package:saool/widgets/round_category_tile.dart';

class QuizListPage extends StatefulWidget {
  final String categoryId;

  const QuizListPage({required this.categoryId});

  @override
  _QuizListPageState createState() => _QuizListPageState();
}

class _QuizListPageState extends State<QuizListPage> {
  final ScrollController _scrollController = ScrollController();
  final ScrollController _tabScrollController = ScrollController();

  String selectedCategoryId = '1';
  double _scrollOffset = 0.0;
  Map<String, double> categoryOffsetMap = {};
  Map<String, int> categoryTabMap = {};

  late List<CategoryModel> categories;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _scrollController.animateTo(
        categoryOffsetMap[widget.categoryId] ?? 0,
        duration: Duration(milliseconds: 1500),
        curve: Curves.fastOutSlowIn,
      );
      _scrollTabController(
        categoryTabMap[widget.categoryId] ?? 0,
        context,
        Duration(milliseconds: 1500),
      );
    });
    selectedCategoryId = widget.categoryId;
  }

  @override
  Widget build(BuildContext context) {
    _scrollOffset = 0.0;
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Consumer<QuizViewModel>(
          builder: (context, vm, _) {
            if (vm.quizzes == null)
              return Center(child: CircularProgressIndicator());
            else {
              categories =
                  vm.quizzes?.map((e) => e.category).toSet().toList() ?? [];
              return Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).viewPadding.top + 24),
                child: Column(
                  children: [
                    Stack(
                      alignment: Alignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: CustomBackButton(),
                        ),
                        Center(
                          child: Text(
                            'جميع الأقسام',
                            style: CustomStyles.boldSmallText()
                                .copyWith(fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    _buildCategories(context),
                    Expanded(
                      child: SingleChildScrollView(
                        controller: _scrollController,
                        child: Container(
                          child: Column(
                            children: categories.map(
                              (e) {
                                categoryOffsetMap[e.id] = _scrollOffset;
                                return Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 24.0),
                                      child: CategoryTitle(
                                        title: e.name,
                                      ),
                                    ),
                                    _buildCategoryTileGrid(
                                      vm.quizzes
                                              ?.where(
                                                  (q) => q.category.id == e.id)
                                              .toList() ??
                                          [],
                                    ),
                                  ],
                                );
                              },
                            ).toList(),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }

  Container _buildCategories(BuildContext context) {
    return Container(
      color: Theme.of(context).accentColor,
      child: SingleChildScrollView(
        controller: _tabScrollController,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 16.0),
        scrollDirection: Axis.horizontal,
        child: Row(
          children: categories
              .asMap()
              .map((i, e) {
                categoryTabMap[e.id] = i;
                return MapEntry(
                    i,
                    RoundTabItem(
                      text: e.name,
                      onPressed: () {
                        setState(() {
                          selectedCategoryId = e.id;
                        });
                        print(categoryOffsetMap);

                        _scrollTabController(
                            i, context, Duration(milliseconds: 500));
                        _scrollController.animateTo(
                            categoryOffsetMap[e.id] ?? 0,
                            duration: Duration(milliseconds: 500),
                            curve: Curves.fastOutSlowIn);
                      },
                      isSelected: selectedCategoryId == e.id,
                    ));
              })
              .values
              .toList(),
        ),
      ),
    );
  }

  void _scrollTabController(int i, BuildContext context, Duration duration) {
    _tabScrollController.animateTo(
        i == 0
            ? 0
            : ((i) * (MediaQuery.of(context).size.width * 0.45 + 8)) -
                (MediaQuery.of(context).size.width / 2) +
                48,
        duration: duration,
        curve: Curves.fastOutSlowIn);
  }

  Padding _buildCategoryTileGrid(List<QuizModel> catQuizzes) {
    _incrementScrollOffsetTitleText();
    List<Widget> list = [];
    for (int i = 0; i < catQuizzes.length; i = i + 2) {
      var first = catQuizzes[i];
      QuizModel? second;
      if ((i + 1) < catQuizzes.length) {
        second = catQuizzes[i + 1];
      }
      list.add(
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CategoryGridItem(
                title: first.title,
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => QuizOverviewPage(
                            quizId: first.id.toString(),
                          )));
                },
                labels: [
                  Label(
                    title: 'تحدي أعلى نتيجة',
                    backgroundColor: Color(0xFFFED766),
                    textColor: Colors.black,
                  ),
                ],
                imageUrl: CommonUtils.toHostUrl(first.imageUrl),
                type: first.type,
                loadingAssetPath: first.loadingAssetPath,
              ),
              second != null
                  ? CategoryGridItem(
                      title: second.title,
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => QuizOverviewPage(
                                  quizId: second!.id.toString(),
                                )));
                      },
                      labels: [
                        Label(
                          title: 'تحدي التصنيف',
                          backgroundColor: Color(0xFF2AB7CA),
                          textColor: Colors.white,
                        ),
                      ],
                      imageUrl: CommonUtils.toHostUrl(second.imageUrl),
                      type: second.type,
                      loadingAssetPath: second.loadingAssetPath,
                    )
                  : Container(),
            ],
          ),
        ),
      );
      _incrementScrollOffsetTile();
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(children: list),
    );
  }

  void _incrementScrollOffsetTitleText() {
    _scrollOffset += 78;
  }

  void _incrementScrollOffsetTile() {
    _scrollOffset += MediaQuery.of(context).size.height * 0.32 + (32);
  }

  // ignore: unused_element
  SingleChildScrollView _buildHorizontalCategoryTileGrid() {
    return SingleChildScrollView(
      padding: EdgeInsets.only(bottom: 34.0),
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          CategoryDetailContainer(
            title: 'هل تعرف ميكاسا فعلا؟',
            labels: [
              Label(
                title: 'تحدي أعلى نتيجة',
                backgroundColor: Color(0xFFFED766),
                textColor: Colors.black,
              ),
              Label(
                title: 'مروج',
                backgroundColor: Color(0xFFE6E6EA),
                textColor: Colors.black,
              ),
            ],
          ),
          SizedBox(
            width: 16,
          ),
          CategoryDetailContainer(
            title: 'هل تعرف ميكاسا فعلا؟',
            labels: [
              Label(
                title: 'تحدي أعلى نتيجة',
                backgroundColor: Color(0xFFFED766),
                textColor: Colors.black,
              ),
              Label(
                title: 'مروج',
                backgroundColor: Color(0xFFE6E6EA),
                textColor: Colors.black,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class CategoryDetailContainer extends StatelessWidget {
  const CategoryDetailContainer({
    Key? key,
    required this.title,
    this.imagePath = '',
    required this.labels,
  }) : super(key: key);

  final String title;
  final String imagePath;
  final List<Label> labels;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.29,
      decoration: CustomStyles.roundShadowBoxDecoration(),
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width - 24,
                height: MediaQuery.of(context).size.height * 0.2,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [0.4, 1],
                      colors: [Color(0xFF2AB7CA), Color(0xFFFED766)]),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                      bottomLeft: Radius.circular(0),
                      bottomRight: Radius.circular(0)),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 28.0),
                child: Text(
                  title,
                  style: CustomStyles.boldSmallText(),
                ),
              )
            ],
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.18,
            child: Row(
              children: labels
                  .map(
                    (e) => Container(
                      width: MediaQuery.of(context).size.width * 0.3,
                      height: MediaQuery.of(context).size.height * 0.04,
                      margin: EdgeInsets.symmetric(horizontal: 8),
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                      decoration: BoxDecoration(
                        color: e.backgroundColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                          e.title,
                          style: CustomStyles.boldSmallText()
                              .copyWith(fontSize: 12),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }
}
