import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/quiz_view_model.dart';
import 'package:saool/view_models/user_view_model.dart';
import 'package:saool/views/home_page.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    getUserData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset('assets/logo-2.svg'),
            SizedBox(
              height: 24,
            ),
            CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }

  Future<void> getUserData() async {
    StaticProvider.of<QuizViewModel>(context).getQuizzes(context);
    StaticProvider.of<UserViewModel>(context).initialize(context).then(
          (_) => Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (context) => HomePage(),
              ),
              (route) => false),
        );
  }
}
