import 'package:flutter/material.dart';
import 'package:saool/models/sign_in_model.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/user_view_model.dart';
import 'package:saool/widgets/form_submit_button.dart';
import 'package:saool/widgets/icon_heading.dart';
import 'package:saool/widgets/onboarding_view.dart';
import 'package:saool/widgets/outlined_text_form_field.dart';

import 'home_page.dart';
import 'sign_up_page.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  SignInModel _signInModel = new SignInModel(identifier: '', password: '');
  final _formKey = new GlobalKey<FormState>();
  bool _obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Form(
          key: _formKey,
          child: OnboardingView(
            formTitle: 'تسجيل الدخول',
            children: [
              OutlinedTextFormField(
                hintText: 'البريد الإلكتروني',
                onChanged: (String newVal) {
                  _signInModel.identifier = newVal;
                },
                validator: (String? val) {
                  if (val == null || val.isEmpty) {
                    return 'Field cannot be empty';
                  }
                },
                prefixIcon: Icon(
                  Icons.alternate_email,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              OutlinedTextFormField(
                hintText: 'كلمة المرور',
                obscureText: _obscureText,
                onChanged: (String newVal) {
                  _signInModel.password = newVal;
                },
                validator: (String? val) {
                  if (val == null || val.isEmpty) {
                    return 'Field cannot be empty';
                  }
                  if (val.length < 6) {
                    return 'Password length must be greater than 6';
                  }
                },
                prefixIcon: Icon(Icons.lock),
                suffixIcon: GestureDetector(
                  onTap: () {
                    setState(() {
                      _obscureText = !_obscureText;
                    });
                  },
                  child: Icon(
                    Icons.remove_red_eye,
                    color: Colors.grey[300],
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              FormSubmitButton(
                title: 'دخول',
                onPressed: () async {
                  if (!_formKey.currentState!.validate()) {
                    return;
                  }
                  print(_signInModel.toMap());
                  bool res = await StaticProvider.of<UserViewModel>(context)
                      .signIn(context, _signInModel);
                  if (res) {
                    StaticProvider.of<UserViewModel>(context)
                        .initialize(context)
                        .then(
                          (_) => Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                builder: (context) => HomePage(),
                              ),
                              (route) => false),
                        );
                  }
                },
              ),
            ],
            iconHeading: IconHeading(
              title: 'ماعندك حساب؟',
              iconPath: 'assets/gift.png',
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => SignUpPage())),
            ),
          ),
        ),
      ),
    );
  }
}
