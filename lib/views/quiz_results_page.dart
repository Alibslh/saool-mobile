import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import 'package:saool/constants/custom_styles.dart';
import 'package:saool/constants/quiz_type_constants.dart';
import 'package:saool/constants/rating.dart';
import 'package:saool/models/label.dart';
import 'package:saool/models/quiz_attempt_model.dart';
import 'package:saool/models/quiz_model.dart';
import 'package:saool/models/rating_model.dart';
import 'package:saool/utils/common_utils.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/quiz_view_model.dart';
import 'package:saool/view_models/user_view_model.dart';
import 'package:saool/views/home_page.dart';
import 'package:saool/views/quiz_overview_page.dart';
import 'package:saool/views/quiz_result_answers_page.dart';
import 'package:saool/widgets/category_grid_item.dart';
import 'package:saool/widgets/category_title.dart';
import 'package:saool/widgets/loading_container.dart';
import 'package:saool/widgets/share_quiz_button.dart';

class QuizResultsPage extends StatefulWidget {
  @override
  _QuizResultsPageState createState() => _QuizResultsPageState();
}

class _QuizResultsPageState extends State<QuizResultsPage> {
  bool readMore = true;
  String selectedRatingId = '5';
  RatingModel ratingModel = new RatingModel();
  List<QuizModel>? randomQuizzes;
  bool isCategorical = false;
  final TextEditingController textEditingController =
      new TextEditingController();
  final LoadingContainerController _loadingContainerController =
      new LoadingContainerController();
  late bool allowRating;
  late int randomImageIndex;

  @override
  void initState() {
    super.initState();
    int randInt = Random().nextInt(6);
    randomImageIndex = randInt == 0 ? 1 : randInt;
    allowRating = true;
    var quizVM = StaticProvider.of<QuizViewModel>(context);
    if (quizVM.quiz != null) {
      isCategorical = quizVM.quiz?.type == QuizTypeConstants.categoricalType;
      if (quizVM.quiz?.id != null) quizVM.getRating(context, quizVM.quiz!.id);
      // TODO: What to post for categorical quizzes
      quizVM.postQuizAttempts(
        context,
        QuizAttemptModel(
          result: quizVM.correctAnswersCount,
          questionsCount: quizVM.quiz!.questions.length,
        ),
        StaticProvider.of<UserViewModel>(context).userModel?.id,
      );
    }
    if (quizVM.quizzes != null && quizVM.quiz != null) {
      randomQuizzes = quizVM.quizzes!
          .where((q) => q.category.id == quizVM.quiz!.category.id)
          .toList();
      if (randomQuizzes != null && randomQuizzes!.isNotEmpty) {
        randomQuizzes!.removeWhere((q) => q.id == quizVM.quiz!.id);
        randomQuizzes!.shuffle();
        // if (randomQuizzes!.length > 12)
        //   randomQuizzes = randomQuizzes!.sublist(0, 12);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Consumer<QuizViewModel>(
        builder: (context, vm, _) => Scaffold(
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (_) {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: SingleChildScrollView(
              child: Stack(
                children: [
                  _buildTopView(context, vm),
                  _buildMainView(context, vm),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Container _buildTopView(BuildContext context, QuizViewModel vm) {
    final bool isCategorical =
        vm.quiz!.type == QuizTypeConstants.categoricalType;
    String? categoricalImageUrl;
    double percentage = -1;
    if (vm.quiz != null && !isCategorical) {
      percentage = (vm.correctAnswersCount / vm.quiz!.questions.length) * 100;
    }

    if ((vm.quiz?.categoricalResult?.length ?? 0) > 0) {
      categoricalImageUrl = vm.quiz?.categoricalResult?.first.image.url;
    }
    return Container(
      color: Colors.red,
      height: 310,
      child: Stack(
        children: [
          SizedBox(
              height: 310,
              width: MediaQuery.of(context).size.width,
              child: Image.network(
                isCategorical
                    ? (categoricalImageUrl != null
                        ? CommonUtils.toHostUrl(categoricalImageUrl)
                        : CommonUtils.getCategoricalResultImageUrl(
                            randomImageIndex))
                    : CommonUtils.getNumericalResultImageUrl(
                        percentage, randomImageIndex),
                fit: BoxFit.cover,
                loadingBuilder: (BuildContext context, Widget child,
                    ImageChunkEvent? loadingProgress) {
                  if (loadingProgress == null) return child;
                  return Center(
                      child: CircularProgressIndicator(
                    value: loadingProgress.expectedTotalBytes != null
                        ? loadingProgress.cumulativeBytesLoaded /
                            loadingProgress.expectedTotalBytes!
                        : null,
                  ));
                },
              )),
          Opacity(
            opacity: 0.2,
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xFFFED788),
                    Color(0x00FEB666),
                  ],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                  stops: [0.35, 1],
                ),
              ),
              height: 310,
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.1,
            right: MediaQuery.of(context).size.width * 0.05,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.white),
                foregroundColor: MaterialStateProperty.all(Colors.black),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                      builder: (context) => HomePage(),
                    ),
                    (route) => false);
              },
              child: Row(
                children: [
                  Icon(Icons.home),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'الصفحة الرئيسية',
                    style: TextStyle(
                      fontSize: 14,
                      // color: Color(0xFF2AB7CA),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildMainView(BuildContext context, QuizViewModel vm) {
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 295),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(14),
                topRight: Radius.circular(14),
              ),
            ),
            padding: EdgeInsets.symmetric(vertical: 24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 32.0, bottom: 36.0),
                  child: Text(
                    'نتيجتك في اختبار البوكيمون:',
                    style: CustomStyles.boldHeadingText(),
                  ),
                ),
                !isCategorical
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            vm.correctAnswersCount.toString(),
                            style: CustomStyles.boldHeadingText().copyWith(
                              fontSize: 40,
                              color: Color(0xFFFED766),
                            ),
                          ),
                          SizedBox(
                            width: 32,
                          ),
                          Text(
                            'من',
                            style: CustomStyles.boldHeadingText().copyWith(
                              fontSize: 40,
                              color: Theme.of(context).accentColor,
                            ),
                          ),
                          SizedBox(
                            width: 32,
                          ),
                          vm.quiz != null
                              ? Text(
                                  vm.quiz!.questions.length.toString(),
                                  style:
                                      CustomStyles.boldHeadingText().copyWith(
                                    fontSize: 40,
                                    color: Theme.of(context).accentColor,
                                  ),
                                )
                              : Container(),
                        ],
                      )
                    : Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24.0),
                        child: Text(
                          vm.getCategoricalResult(),
                          style: CustomStyles.boldHeadingText().copyWith(
                            color: Theme.of(context).accentColor,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                SizedBox(
                  height: 36,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 24.0),
                  child: ShareQuizButton(
                    quiz: vm.quiz,
                  ),
                ),
                SizedBox(
                  height: 24,
                ),
                Text(
                  'صور الشاشة وشارك نتيجتك!',
                  style: CustomStyles.boldSmallText(),
                ),
                !isCategorical
                    ? GestureDetector(
                        onTap: () => Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => QuizResultAnswersPage())),
                        child: Container(
                          decoration: CustomStyles.roundShadowBoxDecoration()
                              .copyWith(
                                  borderRadius: BorderRadius.circular(20)),
                          padding: EdgeInsets.symmetric(
                              horizontal: 36.0, vertical: 28.0),
                          margin: EdgeInsets.symmetric(
                              horizontal: 24.0, vertical: 32.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'استعراض الأجوبة',
                                style: CustomStyles.extraBoldText(),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: Theme.of(context).accentColor,
                                  borderRadius: BorderRadius.circular(4.0),
                                  shape: BoxShape.rectangle,
                                ),
                                padding: EdgeInsets.all(8.0),
                                child: Icon(
                                  Icons.search_rounded,
                                  color: Color(0xFFFDD835),
                                  size: 35,
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    : SizedBox(
                        height: 40,
                      ),
                allowRating
                    ? Column(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 4.0),
                            child: CategoryTitle(
                              title: 'كيف كان الاختبار؟',
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 28.0, vertical: 24.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: RatingTypes.ratingTypes
                                  .map((e) => RatingEmojiTile(
                                        emoji: e.emoji,
                                        text: e.text,
                                        count: vm.ratings
                                                ?.where(
                                                    (r) => r.value == e.value)
                                                .length ??
                                            -1,
                                        isSelected: selectedRatingId ==
                                            e.value.toString(),
                                        onPressed: () {
                                          setState(() {
                                            selectedRatingId =
                                                e.value.toString();
                                          });
                                          ratingModel.value = e.value;
                                        },
                                      ))
                                  .toList(),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 4.0),
                            child: CategoryTitle(
                              title: 'شاركنا رأيك 🧑‍🏫',
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 24.0, vertical: 16.0),
                            child: TextFormField(
                              controller: textEditingController,
                              minLines: 4,
                              maxLines: 4,
                              onChanged: (newVal) {
                                ratingModel.text = newVal;
                              },
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    width: 2,
                                    color: Color(0xFFE0E0E0),
                                  ),
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                hintText: 'اكتب هنا…',
                                hintStyle: CustomStyles.regularText().copyWith(
                                  fontSize: 16,
                                  color: Color(0xFFDFDFDF),
                                ),
                              ),
                            ),
                          ),
                          _buildSubmitRatingButton(context),
                          SizedBox(
                            height: 24,
                          ),
                        ],
                      )
                    : Container(),
                randomQuizzes != null && randomQuizzes!.isNotEmpty
                    ? Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4.0),
                        child: CategoryTitle(
                          title: 'جرب ذي كمان، حتعجبك!',
                        ),
                      )
                    : Container(),
                randomQuizzes != null && randomQuizzes!.isNotEmpty
                    ? _buildCategoryTileGrid(randomQuizzes!)
                    : Container(),
              ],
            ),
          ),
        ),
        Positioned(
          top: 277,
          left: 20,
          child: SvgPicture.asset('assets/saool-logo-small.svg'),
        ),
      ],
    );
  }

  Widget _buildSubmitRatingButton(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.07,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: TextButton(
          style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all(Theme.of(context).accentColor),
            foregroundColor: MaterialStateProperty.all(Colors.white),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
            ),
          ),
          onPressed: () {
            _loadingContainerController.loading();
            StaticProvider.of<QuizViewModel>(context)
                .addRating(context, ratingModel)
                .then((value) {
              if (value) {
                textEditingController.clear();
                _loadingContainerController.idle();
                setState(() {
                  allowRating = false;
                });
              }
            });
          },
          child: LoadingContainer(
              indicatorColor: Colors.white,
              // size: 20,
              child: Text(
                'أرسل',
                style: CustomStyles.regularText(),
              ),
              loadingContainerController: _loadingContainerController),
        ),
      ),
    );
  }

  Padding _buildCategoryTileGrid(List<QuizModel> catQuizzes) {
    List<Widget> list = [];
    for (int i = 0; i < catQuizzes.length; i = i + 2) {
      var first = catQuizzes[i];
      QuizModel? second;
      if ((i + 1) < catQuizzes.length) {
        second = catQuizzes[i + 1];
      }
      list.add(
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CategoryGridItem(
                title: first.title,
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => QuizOverviewPage(
                            quizId: first.id.toString(),
                          )));
                },
                labels: [
                  Label(
                    title: 'تحدي أعلى نتيجة',
                    backgroundColor: Color(0xFFFED766),
                    textColor: Colors.black,
                  ),
                ],
                imageUrl: CommonUtils.toHostUrl(first.imageUrl),
                type: first.type,
                loadingAssetPath: first.loadingAssetPath,
              ),
              second != null
                  ? CategoryGridItem(
                      title: second.title,
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => QuizOverviewPage(
                                  quizId: second!.id.toString(),
                                )));
                      },
                      labels: [
                        Label(
                          title: 'تحدي التصنيف',
                          backgroundColor: Color(0xFF2AB7CA),
                          textColor: Colors.white,
                        ),
                      ],
                      imageUrl: CommonUtils.toHostUrl(second.imageUrl),
                      type: second.type,
                      loadingAssetPath: second.loadingAssetPath,
                    )
                  : Container(),
            ],
          ),
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(children: list),
    );
  }
}

class RatingEmojiTile extends StatelessWidget {
  const RatingEmojiTile({
    Key? key,
    required this.emoji,
    required this.text,
    required this.count,
    required this.isSelected,
    required this.onPressed,
  }) : super(key: key);

  final String emoji;
  final String text;
  final int count;
  final bool isSelected;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Opacity(
        opacity: isSelected ? 1.0 : 0.6,
        child: Container(
          width: MediaQuery.of(context).size.width * 0.16,
          height: MediaQuery.of(context).size.height * 0.17,
          decoration: BoxDecoration(
            color: Color(0xFFFED766),
            borderRadius: BorderRadius.circular(7),
            border: Border.all(
              width: 3,
              color: Color(0xFFFEB666),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 13.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  emoji,
                  style: TextStyle(fontSize: 35),
                ),
                AnimatedContainer(
                  duration: Duration(seconds: 1),
                  curve: Curves.linearToEaseOut,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.08,
                  decoration: !isSelected
                      ? BoxDecoration(
                          color: Color(0xFFFFEFC1),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(7),
                              bottomRight: Radius.circular(7)),
                        )
                      : BoxDecoration(),
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        text,
                        style:
                            CustomStyles.boldSmallText().copyWith(fontSize: 16),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        count == -1
                            ? ''
                            : (count / 1000) >= 1
                                ? ((count / 1000).toStringAsFixed(0) + 'K')
                                : count.toString(),
                        style: CustomStyles.boldSmallText().copyWith(
                          fontSize: 12,
                          color: Color(0xFF959595),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
