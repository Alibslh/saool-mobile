import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import 'package:saool/constants/custom_styles.dart';
import 'package:saool/models/answer.dart';
import 'package:saool/models/choice_model.dart';
import 'package:saool/models/question_model.dart';
import 'package:saool/utils/common_utils.dart';
import 'package:saool/view_models/quiz_view_model.dart';
import 'package:saool/views/quiz_results_page.dart';

class QuizQuestionPage extends StatefulWidget {
  @override
  _QuizQuestionPageState createState() => _QuizQuestionPageState();
}

class _QuizQuestionPageState extends State<QuizQuestionPage> {
  String selectedAnswerId = '-1';
  bool readMore = true;
  bool showResult = false;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Consumer<QuizViewModel>(
          builder: (context, vm, _) {
            QuestionModel? question = vm.quiz?.questions[vm.questionNo];
            if (question == null)
              return Center(child: CircularProgressIndicator());
            else
              return SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).viewPadding.top + 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                  onTap: () => Navigator.pop(context),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.arrow_back_ios,
                                        color: CustomStyles.tertiaryTextColor,
                                        size: 14,
                                      ),
                                      Text(
                                        'الانسحاب من الاختبار',
                                        style: CustomStyles.boldSmallText()
                                            .copyWith(
                                                color: CustomStyles
                                                    .tertiaryTextColor),
                                      ),
                                    ],
                                  ),
                                ),
                                SvgPicture.asset(
                                  'assets/logo-2.svg',
                                  width: 83,
                                  height: 32,
                                ),
                              ],
                            ),
                            question.image != null
                                ? SizedBox(
                                    height: 40,
                                  )
                                : Container(),
                            question.image != null
                                ? Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.3,
                                    child: Center(
                                      child: Image.network(
                                        CommonUtils.toHostUrl(
                                            question.image!.url),
                                        loadingBuilder: (BuildContext context,
                                            Widget child,
                                            ImageChunkEvent? loadingProgress) {
                                          if (loadingProgress == null)
                                            return child;
                                          return Center(
                                            child: CircularProgressIndicator(
                                              value: loadingProgress
                                                          .expectedTotalBytes !=
                                                      null
                                                  ? loadingProgress
                                                          .cumulativeBytesLoaded /
                                                      loadingProgress
                                                          .expectedTotalBytes!
                                                  : null,
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                  )
                                : Container(),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 24.0),
                              child: Text(
                                question.statement,
                                style: CustomStyles.boldHeadingText(),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'سؤال رقم ${vm.questionNo + 1} من ${vm.quiz!.questions.length}',
                                  style: CustomStyles.boldSmallText().copyWith(
                                      color: Theme.of(context).accentColor),
                                ),
                                // INFO: Time to be handled in phase 2
                                // Text(
                                //   'الوقت المتبقي: 10:30',
                                //   style: CustomStyles.boldSmallText(),
                                // ),
                              ],
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Container(
                              child: Column(
                                children: question.choices
                                    .map(
                                      (choice) => AnswerTile(
                                        answer: Answer(
                                            id: choice.id, text: choice.text),
                                        isSelected:
                                            selectedAnswerId == choice.id,
                                        onPressed: () {
                                          if (selectedAnswerId != '-1') {
                                            return;
                                          }
                                          _handleSelection(
                                              choice, vm, question, context);
                                        },
                                        showResult: showResult,
                                        isCorrect: choice.isCorrect,
                                        animatedContainerDuration:
                                            Duration(milliseconds: 300),
                                      ),
                                    )
                                    .toList(),
                              ),
                            ),
                          ],
                        ),
                      ),
                      // INFO: Removed until its backend funcitionality is complete in next phase
                      // Container(
                      //   decoration: CustomStyles.roundShadowBoxDecoration(),
                      //   margin: EdgeInsets.only(top: 28.0),
                      //   padding: EdgeInsets.symmetric(
                      //       horizontal: 36.0, vertical: 24.0),
                      //   child: Column(
                      //     crossAxisAlignment: CrossAxisAlignment.start,
                      //     children: [
                      //       Center(
                      //         child: Text(
                      //           'أدوات التغشيش 😈',
                      //           style: CustomStyles.extraBoldText(),
                      //         ),
                      //       ),
                      //       SizedBox(
                      //         height: 16,
                      //       ),
                      //       Row(
                      //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //         children: [
                      //           CheatToolContainer(
                      //             assetPath: 'assets/powerup-negative.png',
                      //             title: 'حذف إجابتين',
                      //             isAvailable: true,
                      //           ),
                      //           CheatToolContainer(
                      //             assetPath: 'assets/powerup-medium.png',
                      //             title: 'الجواب الصح',
                      //             isAvailable: false,
                      //           ),
                      //           CheatToolContainer(
                      //             assetPath: 'assets/powerup-plus.png',
                      //             title: 'فرصة ثانية',
                      //             isAvailable: false,
                      //           ),
                      //         ],
                      //       ),
                      //     ],
                      //   ),
                      // ),
                    ],
                  ),
                ),
              );
          },
        ),
      ),
    );
  }

  void _handleSelection(ChoiceModel choice, QuizViewModel vm,
      QuestionModel question, BuildContext context) {
    final bool isCategorical = vm.quiz?.type == 'categorical';
    setState(() {
      selectedAnswerId = choice.id;
    });
    vm.setSelectedAnswer(question.id, choice.id);

    if (isCategorical) {
      Future.delayed(Duration(milliseconds: 750)).then((value) {
        bool success = vm.incrementQuestionNo();
        if (success) {
          setState(() {
            selectedAnswerId = '-1';
          });
        } else {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => QuizResultsPage()));
        }
      });
    } else {
      // numerical
      Future.delayed(Duration(milliseconds: 500)).then((value) {
        setState(() {
          showResult = true;
        });
        Future.delayed(Duration(seconds: 1)).then((value) {
          if (selectedAnswerId ==
              question.choices
                  .firstWhere((e) => e.isCorrect,
                      orElse: () => ChoiceModel.invalid())
                  .id) {
            vm.incrementCorrectAnswersCount();
          }
          print(vm.correctAnswersCount.toString() +
              '/' +
              vm.quiz!.questions.length.toString());
          bool success = vm.incrementQuestionNo();
          if (success) {
            setState(() {
              showResult = false;
              selectedAnswerId = '-1';
            });
          } else {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => QuizResultsPage()));
          }
        });
      });
    }
  }
}

class AnswerTile extends StatelessWidget {
  AnswerTile({
    Key? key,
    required this.answer,
    required this.isSelected,
    required this.showResult,
    required this.isCorrect,
    required this.onPressed,
    required this.animatedContainerDuration,
  }) : super(key: key);

  final Answer answer;
  final bool isSelected;
  final bool showResult;
  final bool isCorrect;
  final Function() onPressed;
  final Duration animatedContainerDuration;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: !showResult ? onPressed : null,
      child: AnimatedContainer(
        duration: animatedContainerDuration,
        curve: Curves.fastOutSlowIn,
        decoration: _buildAnswerDecoration(context),
        padding: EdgeInsets.symmetric(
            vertical: isSelected || (showResult && isCorrect) ? 16.0 : 8.0),
        margin: const EdgeInsets.symmetric(vertical: 6.0),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16.0),
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: isSelected || (showResult && isCorrect)
                    ? Colors.white
                    : Color(0xFFE6E6EA),
              ),
              child: _buildAnswerIcon(context),
            ),
            Expanded(
              child: Text(
                answer.text,
                style: CustomStyles.regularText().copyWith(
                  fontSize: 20,
                  fontWeight: isSelected ? FontWeight.bold : FontWeight.normal,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAnswerIcon(BuildContext context) {
    if (showResult && isCorrect)
      return Icon(
        Icons.check,
        color: isSelected ? Colors.green : Color(0xFFFED766),
        size: 26,
      );

    if (isSelected) {
      return Center(
        child: Container(
          width: 18,
          height: 18,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Theme.of(context).accentColor,
          ),
        ),
      );
    }

    return Container();
  }

  BoxDecoration _buildAnswerDecoration(BuildContext context) {
    BoxDecoration decoration = BoxDecoration();
    if (!showResult) {
      if (isSelected) {
        decoration = CustomStyles.roundSimpleBoxDecoration()
            .copyWith(color: Color(0xFFFED766));
      }
    } else {
      if (isSelected && isCorrect) {
        decoration = CustomStyles.roundSimpleBoxDecoration()
            .copyWith(color: Colors.green);
      } else if (isSelected && !isCorrect) {
        decoration = CustomStyles.roundSimpleBoxDecoration()
            .copyWith(color: Color(0xFFFE4A49));
      } else if (!isSelected && isCorrect) {
        decoration = CustomStyles.roundSimpleBoxDecoration()
            .copyWith(color: Theme.of(context).accentColor.withOpacity(0.21));
      }
    }
    return decoration;
  }
}
