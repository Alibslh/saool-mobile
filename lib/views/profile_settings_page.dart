import 'dart:io';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import 'package:saool/constants/custom_styles.dart';
import 'package:saool/constants/file_upload_constants.dart';
import 'package:saool/models/image_model.dart';
import 'package:saool/models/profile_settings_model.dart';
import 'package:saool/models/requests/upload_file_request.dart';
import 'package:saool/utils/common_utils.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/user_view_model.dart';
import 'package:saool/widgets/add_profile_photo_container.dart';
import 'package:saool/widgets/custom_back_button.dart';
import 'package:saool/widgets/form_submit_button.dart';
import 'package:saool/widgets/outlined_text_form_field.dart';

class ProfileSettingsPage extends StatefulWidget {
  @override
  _ProfileSettingsPageState createState() => _ProfileSettingsPageState();
}

class _ProfileSettingsPageState extends State<ProfileSettingsPage> {
  ProfileSettingsModel profileSettingsModel = new ProfileSettingsModel();
  late List<TextFieldItem> informationFields;
  late List<TextFieldItem> socialMediaFields;

  var _formKey = GlobalKey<FormState>();
  File? _image;
  final picker = ImagePicker();
  ImageModel? profileImageModel;

  @override
  void initState() {
    super.initState();
    informationFields = [
      // No value for name in API so skipping this
      // TextFieldItem(
      //   hintText: 'الاسم',
      //   prefixIcon: Padding(
      //     padding: const EdgeInsets.only(left: 8.0),
      //     child: Image.asset('assets/id-card.png'),
      //   ),
      //   onChanged: (newVal) {
      //     profileSettingsModel.name = newVal;
      //   },
      // ),
      TextFieldItem(
        hintText: 'اسم المستخدم',
        prefixIcon: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Image.asset('assets/man.png'),
        ),
        onChanged: (newVal) {
          profileSettingsModel.username = newVal;
        },
        fieldType: FieldType.Name,
      ),
      TextFieldItem(
          hintText: 'البريد الإلكتروني',
          prefixIcon: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Image.asset('assets/at.png'),
          ),
          onChanged: (newVal) {
            profileSettingsModel.email = newVal;
          },
          validator: (String? val) {
            if (val != null &&
                val.isNotEmpty &&
                !EmailValidator.validate(val)) {
              return 'Email is not valid';
            }
          },
          fieldType: FieldType.Email),
      TextFieldItem(
          hintText: 'كلمة المرور',
          onChanged: (String newVal) {
            profileSettingsModel.password = newVal;
          },
          validator: (String? val) {
            if (val != null && val.isNotEmpty && val.length < 6) {
              return 'Password length must be greater than 6';
            }
          },
          prefixIcon: Icon(Icons.lock),
          suffixIcon: GestureDetector(
            onTap: () {},
            child: Icon(
              Icons.remove_red_eye,
              color: Colors.grey[300],
            ),
          ),
          fieldType: FieldType.Password),
    ];

    socialMediaFields = [
      TextFieldItem(
        hintText: 'اسم مستخدم التويتر',
        prefixIcon: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Image.asset('assets/twitter-2.png'),
        ),
        onChanged: (newVal) {
          profileSettingsModel.twitter = newVal;
        },
        fieldType: FieldType.Twitter,
      ),
      TextFieldItem(
        hintText: 'اسم مستخدم سناب شات',
        prefixIcon: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Image.asset('assets/snapchat.png'),
        ),
        onChanged: (newVal) {
          profileSettingsModel.snapchat = newVal;
        },
        fieldType: FieldType.Snapchat,
      ),
      TextFieldItem(
        hintText: 'اسم مستخدم الانستغرام',
        prefixIcon: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Image.asset('assets/instagram-2.png'),
        ),
        onChanged: (newVal) {
          profileSettingsModel.instagram = newVal;
        },
        fieldType: FieldType.Instagram,
      ),
      TextFieldItem(
        hintText: 'اسم مستخدم الفيسبوك',
        prefixIcon: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Image.asset('assets/facebook-2.png'),
        ),
        onChanged: (newVal) {
          profileSettingsModel.facebook = newVal;
        },
        fieldType: FieldType.Facebook,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'تعديل الملف',
          style: CustomStyles.boldSmallText()
              .copyWith(fontSize: 16, color: Colors.black),
        ),
        actions: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: CustomBackButton(),
              ),
            ],
          ),
        ],
      ),
      body: Consumer<UserViewModel>(
        builder: (context, vm, _) => Directionality(
          textDirection: TextDirection.rtl,
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 16.0, vertical: 40.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.2,
                              height: MediaQuery.of(context).size.width * 0.2,
                              child: _image == null
                                  ? (vm.userModel?.userPermissionsUser
                                              ?.profileImageUrl !=
                                          null
                                      ? Image.network(
                                          CommonUtils.toHostUrl(vm
                                              .userModel!
                                              .userPermissionsUser!
                                              .profileImageUrl!),
                                          fit: BoxFit.cover,
                                        )
                                      : Image.asset(
                                          'assets/profile-photo.png',
                                          fit: BoxFit.cover,
                                        ))
                                  : Image.file(
                                      _image!,
                                      fit: BoxFit.cover,
                                    ),
                            ),
                          ),
                          SizedBox(
                            width: 16.0,
                          ),
                          AddProfilePhotoContainer(
                            onPressed: () async {
                              getImage().then(
                                (_) async {
                                  if (_image != null) {
                                    profileImageModel =
                                        await StaticProvider.of<UserViewModel>(
                                                context)
                                            .uploadFile(
                                      context,
                                      UploadFileRequest(
                                        files: _image!,
                                        field: 'image',
                                        ref: '',
                                        refId: '',
                                      ),
                                      filename:
                                          FileUploadConstants.profilePicture,
                                      imageFormat: ImageFormat.THUMBNAIL,
                                    );
                                    print(profileImageModel?.toMap());
                                    profileSettingsModel.profileImage =
                                        profileImageModel;
                                  }
                                },
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 24.0),
                            child: Text(
                              'المعلومات الرئيسية',
                              style: CustomStyles.extraBoldText()
                                  .copyWith(fontSize: 22.0),
                            ),
                          ),
                          SizedBox(
                            height: 24.0,
                          ),
                          for (var e in informationFields)
                            Padding(
                              padding: const EdgeInsets.only(bottom: 16.0),
                              child: OutlinedTextFormField(
                                hintText: e.hintText,
                                initialValue: getInitialValue(e, vm),
                                prefixIcon: e.prefixIcon,
                                onChanged: e.onChanged,
                                validator: e.validator,
                              ),
                            ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 24.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 24.0),
                            child: Text(
                              'حسابات التواصل الاجتماعي',
                              style: CustomStyles.extraBoldText()
                                  .copyWith(fontSize: 22.0),
                            ),
                          ),
                          SizedBox(
                            height: 24.0,
                          ),
                          for (var e in socialMediaFields)
                            Padding(
                              padding: const EdgeInsets.only(bottom: 16.0),
                              child: OutlinedTextFormField(
                                hintText: e.hintText,
                                initialValue: getInitialValue(e, vm),
                                prefixIcon: e.prefixIcon,
                                onChanged: e.onChanged,
                                validator: e.validator,
                              ),
                            ),
                        ],
                      ),
                    ),
                    FormSubmitButton(
                      title: 'حفظ التغييرات',
                      iconPath: 'assets/tick-icon.svg',
                      onPressed: () {
                        print(profileSettingsModel.toValidMap());
                        if (!_formKey.currentState!.validate()) {
                          return;
                        }
                        StaticProvider.of<UserViewModel>(context)
                            .updateProfile(context, profileSettingsModel);
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  String? getInitialValue(TextFieldItem e, UserViewModel vm) {
    switch (e.fieldType) {
      case FieldType.Name:
        return vm.userModel?.username;
      case FieldType.Email:
        return vm.userModel?.email;
      case FieldType.Twitter:
        return vm.userModel?.userPermissionsUser?.twitter;
      case FieldType.Snapchat:
        return vm.userModel?.userPermissionsUser?.snapchat;
      case FieldType.Instagram:
        return vm.userModel?.userPermissionsUser?.instagram;
      case FieldType.Facebook:
        return vm.userModel?.userPermissionsUser?.facebook;
      default:
    }
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    // final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }
}

class TextFieldItem {
  final String hintText;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final Function(String)? onChanged;
  final String? Function(String?)? validator;
  final FieldType fieldType;

  TextFieldItem({
    required this.hintText,
    this.prefixIcon,
    this.suffixIcon,
    this.onChanged,
    this.validator,
    required this.fieldType,
  });
}

enum FieldType { Name, Email, Password, Twitter, Snapchat, Instagram, Facebook }
