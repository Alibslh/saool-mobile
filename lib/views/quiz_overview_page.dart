import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:saool/config/network_endpoints.dart';
import 'package:saool/constants/custom_styles.dart';
import 'package:saool/models/quiz_model_extended.dart';
import 'package:saool/utils/common_utils.dart';
import 'package:saool/utils/snack_utils.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/quiz_view_model.dart';
import 'package:saool/view_models/user_view_model.dart';
import 'package:saool/views/quiz_question_page.dart';
import 'package:saool/views/user_profile_page.dart';
import 'package:saool/widgets/custom_back_button.dart';
import 'package:saool/widgets/rounded_gradient_container.dart';
import 'package:saool/widgets/vertical_icon_container.dart';
import 'package:clipboard/clipboard.dart';

class QuizOverviewPage extends StatefulWidget {
  final String quizId;
  QuizOverviewPage({required this.quizId});
  @override
  _QuizOverviewPageState createState() => _QuizOverviewPageState();
}

class _QuizOverviewPageState extends State<QuizOverviewPage> {
  bool readMore = true;
  late bool canViewProfile;

  @override
  void initState() {
    super.initState();
    canViewProfile =
        StaticProvider.of<UserViewModel>(context).userModel?.jwt != null;
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      StaticProvider.of<QuizViewModel>(context).getQuiz(context, widget.quizId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Consumer<QuizViewModel>(
          builder: (context, vm, _) => vm.quiz == null
              ? Center(child: CircularProgressIndicator())
              : SingleChildScrollView(
                  child: Stack(
                    children: [
                      _buildTopView(context, vm.quiz!),
                      _buildMainView(context, vm.quiz!),
                    ],
                  ),
                ),
        ),
      ),
    );
  }

  Container _buildTopView(BuildContext context, QuizModelExtended quiz) {
    return Container(
      color: Colors.red,
      // height: 310,
      child: Stack(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.45,
            width: MediaQuery.of(context).size.width,
            child: quiz.imageUrl.isEmpty
                ? Container()
                : Image.network(
                    CommonUtils.toHostUrl(quiz.imageUrl),
                    fit: BoxFit.cover,
                  ),
          ),
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color(0xFFFED788),
                  Color(0x00FEB666),
                ],
                begin: Alignment.centerRight,
                end: Alignment.centerLeft,
                stops: [0.4, 1],
              ),
            ),
            height: MediaQuery.of(context).size.height * 0.45,
          ),
          Container(
            padding: EdgeInsets.only(
                left: 16.0,
                right: 16.0,
                top: MediaQuery.of(context).size.height * 0.23,
                bottom: 36.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.91,
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Text(
                        quiz.title.trim(),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Row(
                        children: [
                          Text(
                            'عدد المختبرين:',
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            quiz.quizAttemptsCount.toStringAsFixed(0),
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 14,
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.white),
                        foregroundColor:
                            MaterialStateProperty.all(Colors.black),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                        ),
                      ),
                      onPressed: () {
                        String quizUrl =
                            NetworkEndpoints.WEBSITE_URL + '/quiz/' + quiz.id;
                        FlutterClipboard.copy(quizUrl).then((value) {
                          print(quizUrl);
                          SnackUtils.showSnack(
                              context, 'تم نسخ رابط الاختبار إلى الحافظة');
                        });
                      },
                      child: Text(
                        'انسخ رابط الاختبار 🚀',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                    ),
                    quiz.userPermissionsUser != null
                        ? ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.white),
                              foregroundColor:
                                  MaterialStateProperty.all(Colors.black),
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(16.0),
                                ),
                              ),
                            ),
                            onPressed: () {
                              if (!canViewProfile) {
                                print(
                                    'Unauthorized user cannot view others profile');
                                return;
                              }
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => UserProfilePage(
                                        userPermissionsUser:
                                            quiz.userPermissionsUser,
                                      )));
                            },
                            child: Text(
                              'المنشئ: ' +
                                  quiz.userPermissionsUser!.username +
                                  (canViewProfile ? '  ↪' : ''),
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            right: 16.0,
            top: MediaQuery.of(context).size.height * 0.08,
            child: Container(
                decoration: CustomStyles.roundSimpleBoxDecoration(),
                child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 16.0, vertical: 6.0),
                    child: CustomBackButton())),
          ),
        ],
      ),
    );
  }

  Widget _buildMainView(BuildContext context, QuizModelExtended quiz) {
    return Stack(
      children: [
        Padding(
          padding:
              EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.45),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(14),
                topRight: Radius.circular(14),
              ),
            ),
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 4.0),
                  child: Row(
                    children: [
                      quiz.keyword.isNotEmpty
                          ? Padding(
                              padding: const EdgeInsets.only(left: 16.0),
                              child: RoundedGradientContainer(
                                child: Text(
                                  quiz.keyword,
                                  // 'تحدي أعلى نتيجة',
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            )
                          : Container(),
                      Text(
                        'مكون من ${quiz.questions.length} أسئلة',
                        style: TextStyle(
                            color: Color(0xFF2AB7CA),
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 4.0, vertical: 16.0),
                  child: Text(
                    quiz.description,
                    style: CustomStyles.regularText(),
                  ),
                ),
                Container(
                  decoration: CustomStyles.roundSimpleBoxDecoration(),
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'أضف أدوات تغشيش',
                            style: CustomStyles.extraBoldText(),
                          ),
                          Text(
                            'قريبا ⏳',
                            style: CustomStyles.extraBoldText(),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          VerticalIconContainer(
                            assetPath: 'assets/powerup-negative.png',
                            title: 'حذف إجابتين',
                            isSelected: true,
                          ),
                          VerticalIconContainer(
                            assetPath: 'assets/powerup-medium.png',
                            title: 'الجواب الصح',
                            isSelected: false,
                          ),
                          VerticalIconContainer(
                            assetPath: 'assets/powerup-plus.png',
                            title: 'فرصة ثانية',
                            isSelected: false,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                RoundedGradientContainer(
                  onPressed: () {
                    StaticProvider.of<QuizViewModel>(context).resetVariables();
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => QuizQuestionPage()));
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 24.0),
                    height: 60,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: Text(
                            'ابدأ الاختبار يا مدير',
                            style: CustomStyles.boldHeadingText(),
                          ),
                        ),
                        Image.asset('assets/fire.png'),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          top: MediaQuery.of(context).size.height * 0.45 - 18,
          left: 20,
          child: SvgPicture.asset('assets/saool-logo-small.svg'),
        ),
      ],
    );
  }
}

class FadingEffect extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Rect rect =
        Rect.fromPoints(Offset(0, 0), Offset(size.width, size.height + 10));
    LinearGradient lg = LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [
          Color.fromARGB(0, 255, 255, 255),
          Color.fromARGB(255, 255, 255, 255)
        ]);
    Paint paint = Paint()..shader = lg.createShader(rect);
    canvas.drawRect(rect, paint);
  }

  @override
  bool shouldRepaint(FadingEffect linePainter) => false;
}
