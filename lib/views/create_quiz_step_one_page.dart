import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:saool/constants/custom_styles.dart';
import 'package:saool/constants/quiz_creation_tabs.dart';
import 'package:saool/models/quiz_creation_model.dart';
import 'package:saool/models/quiz_model_extended.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/quiz_creation_view_model.dart';
import 'package:saool/widgets/custom_back_button.dart';
import 'package:saool/widgets/form_submit_button.dart';
import 'package:saool/widgets/outlined_text_form_field.dart';
import 'package:saool/widgets/round_category_tile.dart';

import 'create_quiz_step_two_page.dart';

class CreateQuizStepOnePage extends StatefulWidget {
  final bool isCategorical;

  const CreateQuizStepOnePage({required this.isCategorical});
  @override
  _CreateQuizStepOnePageState createState() => _CreateQuizStepOnePageState();
}

class _CreateQuizStepOnePageState extends State<CreateQuizStepOnePage> {
  String selectedTab = '1';
  int titleLength = 40;
  int descriptionLength = 430;
  final _formKey = new GlobalKey<FormState>();
  QuizCreationModel quizCreationModel =
      QuizCreationModel(title: '', description: '');

  final List<String> selectedTabIds = ['1'];

  TextStyle boldHeadlineStyle() =>
      CustomStyles.boldHeadingText().copyWith(fontSize: 20);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).viewPadding.top + 24),
              child: Column(
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: CustomBackButton(),
                      ),
                      Text(
                        'إنشاء اختبار تصنيفي',
                        style:
                            CustomStyles.boldSmallText().copyWith(fontSize: 16),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  _buildCategories(context),
                  SizedBox(
                    height: 24,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'ولأن كل حاجة رهيبة لازم يكون لها اسم ووصف، لذا حابين يكون اختبارك رهيب أيضاً',
                          style: boldHeadlineStyle(),
                        ),
                        SizedBox(
                          height: 32,
                        ),
                        Text(
                          'اسم الاختبار',
                          style: boldHeadlineStyle(),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 4.0, bottom: 8.0),
                          child: Text(
                            'لازم يكون جذاب عشان الناس تدخل الاختبار أكثر',
                            style: CustomStyles.regularSmallText(),
                          ),
                        ),
                        OutlinedTextFormField(
                          hintText: 'البريد الإلكتروني',
                          borderColor: Color(0xFFFED766),
                          onChanged: (newVal) {
                            setState(() {
                              quizCreationModel.title = newVal;
                            });
                          },
                          validator: (val) {
                            if (val?.isEmpty ?? true) {
                              return 'Field cannot be empty';
                            }
                          },
                          textInputFormatters: [
                            LengthLimitingTextInputFormatter(titleLength)
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                            'متبقي ${titleLength - quizCreationModel.title.length} حرف',
                            style: TextStyle(
                              color: CustomStyles.warningColor,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        Text(
                          'وصف الاختبار',
                          style: boldHeadlineStyle(),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 4.0, bottom: 8.0),
                          child: Text(
                            'عشان تعرف الجمهور على محتوى الاختبار',
                            style: CustomStyles.regularSmallText(),
                          ),
                        ),
                        OutlinedTextFormField(
                          hintText: 'وصف العظماء',
                          borderColor: Color(0xFFFED766),
                          maxLines: 10,
                          onChanged: (newVal) {
                            setState(() {
                              quizCreationModel.description = newVal;
                            });
                          },
                          validator: (val) {
                            if (val?.isEmpty ?? true) {
                              return 'Field cannot be empty';
                            }
                          },
                          textInputFormatters: [
                            LengthLimitingTextInputFormatter(descriptionLength)
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                            'متبقي ${descriptionLength - quizCreationModel.description.length} حرف',
                            style: TextStyle(
                              color: CustomStyles.warningColor,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        FormSubmitButton(
                          title: 'التالي',
                          onPressed: () {
                            if (!_formKey.currentState!.validate()) {
                              return;
                            }
                            StaticProvider.of<QuizCreationViewModel>(context)
                                .setQuiz(
                              QuizModelExtended.fromQuizCreationModel(
                                  quizCreationModel, widget.isCategorical),
                            );
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => CreateQuizStepTwoPage()));
                          },
                        ),
                        SizedBox(
                          height: 40,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Container _buildCategories(BuildContext context) {
    return Container(
      color: CustomStyles.tabColor,
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 16.0),
        scrollDirection: Axis.horizontal,
        child: Row(
          children: QuizCreationConstants.quizCreationTabs.map(
            (e) {
              return RoundTabItem(
                text: e.title,
                onPressed: () {
                  setState(() {
                    selectedTab = e.id;
                  });
                },
                isSelected: selectedTabIds.contains(e.id),
                lightTheme: true,
              );
            },
          ).toList(),
        ),
      ),
    );
  }
}

class QuizCreationTabs {
  final String id;
  final String title;
  final Function() onPressed;

  QuizCreationTabs({
    required this.id,
    required this.title,
    required this.onPressed,
  });
}
