import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import 'package:saool/constants/custom_styles.dart';
import 'package:saool/models/answer.dart';
import 'package:saool/models/question.dart';
import 'package:saool/models/quiz_model_extended.dart';
import 'package:saool/utils/common_utils.dart';
import 'package:saool/view_models/quiz_view_model.dart';
import 'package:saool/widgets/custom_back_button.dart';
import 'package:saool/widgets/share_quiz_button.dart';

class QuizResultAnswersPage extends StatefulWidget {
  @override
  _QuizResultAnswersPageState createState() => _QuizResultAnswersPageState();
}

class _QuizResultAnswersPageState extends State<QuizResultAnswersPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<QuizViewModel>(
      builder: (context, vm, _) => vm.quiz == null
          ? Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.white,
                title: Text(
                  vm.quiz!.title,
                  style: CustomStyles.boldSmallText()
                      .copyWith(fontSize: 16, color: Colors.black),
                ),
                actions: [
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: CustomBackButton(),
                      ),
                    ],
                  ),
                ],
              ),
              body: Directionality(
                textDirection: TextDirection.rtl,
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 24.0, vertical: 16.0),
                    child: Stack(
                      children: [
                        Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                              child: Container(
                                decoration:
                                    CustomStyles.roundShadowBoxDecoration(),
                                padding: EdgeInsets.symmetric(vertical: 32.0),
                                child: Column(
                                  children: [
                                    Text(
                                      'نتيجتك في اختبار البوكيمون:',
                                      style: CustomStyles.boldHeadingText(),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 36.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            vm.correctAnswersCount.toString(),
                                            style:
                                                CustomStyles.boldHeadingText()
                                                    .copyWith(
                                              fontSize: 40,
                                              color: Color(0xFFFED766),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 32,
                                          ),
                                          Text(
                                            'من',
                                            style:
                                                CustomStyles.boldHeadingText()
                                                    .copyWith(
                                              fontSize: 40,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 32,
                                          ),
                                          Text(
                                            vm.quiz!.questions.length
                                                .toString(),
                                            style:
                                                CustomStyles.boldHeadingText()
                                                    .copyWith(
                                              fontSize: 40,
                                              color:
                                                  Theme.of(context).accentColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 24,
                            ),
                            Column(
                              children: vm.quiz!.questions
                                  .asMap()
                                  .map(
                                    (i, e) {
                                      QuizModelExtended quiz = vm.quiz!;
                                      return MapEntry(
                                          i,
                                          Column(
                                            children: [
                                              QuestionDetailContainer(
                                                  totalQuestions:
                                                      quiz.questions.length,
                                                  selectedChoices:
                                                      vm.selectedChoices!,
                                                  question: Question(
                                                      quizId: quiz.id,
                                                      id: e.id,
                                                      imageUrl: e.image?.url,
                                                      answers: e.choices
                                                          .map((c) => Answer(
                                                              id: c.id,
                                                              text: c.text))
                                                          .toList(),
                                                      correctAnswerIndex: e
                                                          .choices
                                                          .firstWhere((c) =>
                                                              c.isCorrect)
                                                          .id,
                                                      question: e.statement,
                                                      questionNo: i + 1)),
                                              SizedBox(
                                                height: 72,
                                              ),
                                            ],
                                          ));
                                    },
                                  )
                                  .values
                                  .toList(),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                ShareQuizButton(
                                  quiz: vm.quiz,
                                ),
                                SizedBox(
                                  height: 24,
                                ),
                                Text(
                                  'صور الشاشة وشارك نتيجتك!',
                                  style: CustomStyles.boldSmallText(),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 72,
                            ),
                          ],
                        ),
                        Positioned(
                          top: 0,
                          left: 20,
                          child: SvgPicture.asset('assets/saool-logo-small.svg'),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}

class QuestionDetailContainer extends StatelessWidget {
  const QuestionDetailContainer({
    Key? key,
    required this.question,
    required this.totalQuestions,
    required this.selectedChoices,
  }) : super(key: key);

  final Question question;
  final int totalQuestions;
  final Map<String, String> selectedChoices;

  @override
  Widget build(BuildContext context) {
    bool correctSelection =
        selectedChoices[question.id] == question.correctAnswerIndex;
    return Column(
      children: [
        question.imageUrl != null
            ? Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.23,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Color(0xFFA9E3EA)),
                child: Image.network(CommonUtils.toHostUrl(question.imageUrl!)),
              )
            : Container(),
        Align(
          alignment: Alignment.centerRight,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 24.0),
            child: Text(
              '${correctSelection ? '✅' : '❌'} ${question.question}',
              style: CustomStyles.boldHeadingText(),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'سؤال رقم ${question.questionNo} من $totalQuestions',
              style: CustomStyles.boldSmallText()
                  .copyWith(color: Theme.of(context).accentColor),
            ),
            Row(
              children: [
                Text(
                  correctSelection ? 'جوابك صحيح' : 'جوابك خطأ',
                  style: CustomStyles.boldSmallText(),
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  correctSelection ? '🥳' : '😢',
                  style: TextStyle(fontSize: 24),
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 32,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                question.answers
                    .firstWhere((e) => e.id == selectedChoices[question.id])
                    .text,
                style: CustomStyles.regularText().copyWith(
                  fontSize: 20,
                ),
              ),
            ),
            Text(
              '✍️',
              style: TextStyle(fontSize: 24),
            ),
          ],
        ),
        SizedBox(
          height: 32,
        ),
        correctSelection
            ? Container()
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      question.answers
                          .firstWhere(
                              (e) => e.id == question.correctAnswerIndex)
                          .text,
                      style: CustomStyles.regularText().copyWith(
                        fontSize: 20,
                      ),
                    ),
                  ),
                  Text(
                    '✅',
                    style: TextStyle(fontSize: 24),
                  ),
                ],
              ),
      ],
    );
  }
}

class AnswerTile extends StatelessWidget {
  const AnswerTile({
    Key? key,
    required this.answer,
    required this.isSelected,
    required this.showResult,
    required this.isCorrect,
    required this.onPressed,
  }) : super(key: key);

  final Answer answer;
  final bool isSelected;
  final bool showResult;
  final bool isCorrect;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: !showResult ? onPressed : null,
      child: AnimatedContainer(
        duration: Duration(seconds: 1),
        curve: Curves.fastOutSlowIn,
        decoration: _buildAnswerDecoration(context),
        padding: EdgeInsets.symmetric(
            vertical: isSelected || (showResult && isCorrect) ? 16.0 : 8.0),
        margin: const EdgeInsets.symmetric(vertical: 6.0),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16.0),
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: isSelected || (showResult && isCorrect)
                    ? Colors.white
                    : Color(0xFFE6E6EA),
              ),
              child: _buildAnswerIcon(context),
            ),
            Expanded(
              child: Text(
                answer.text,
                style: CustomStyles.regularText().copyWith(
                  fontSize: 20,
                  fontWeight: isSelected ? FontWeight.bold : FontWeight.normal,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAnswerIcon(BuildContext context) {
    if (showResult && isCorrect)
      return Icon(
        Icons.check,
        color: isSelected ? Colors.green : Color(0xFFFED766),
        size: 26,
      );

    if (isSelected) {
      return Center(
        child: Container(
          width: 18,
          height: 18,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Theme.of(context).accentColor,
          ),
        ),
      );
    }

    return Container();
  }

  BoxDecoration _buildAnswerDecoration(BuildContext context) {
    BoxDecoration decoration = BoxDecoration();
    if (!showResult) {
      if (isSelected) {
        decoration = CustomStyles.roundSimpleBoxDecoration()
            .copyWith(color: Color(0xFFFED766));
      }
    } else {
      if (isSelected && isCorrect) {
        decoration = CustomStyles.roundSimpleBoxDecoration()
            .copyWith(color: Colors.green);
      } else if (isSelected && !isCorrect) {
        decoration = CustomStyles.roundSimpleBoxDecoration()
            .copyWith(color: Color(0xFFFE4A49));
      } else if (!isSelected && isCorrect) {
        decoration = CustomStyles.roundSimpleBoxDecoration()
            .copyWith(color: Theme.of(context).accentColor.withOpacity(0.21));
      }
    }
    return decoration;
  }
}
