import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import 'package:saool/constants/custom_styles.dart';
import 'package:saool/models/category_model.dart';
import 'package:saool/models/google_form_model.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/quiz_creation_view_model.dart';
import 'package:saool/view_models/user_view_model.dart';
import 'package:saool/widgets/custom_back_button.dart';
import 'package:saool/widgets/form_submit_button.dart';
import 'package:saool/widgets/outlined_text_form_field.dart';

class GoogleFormPage extends StatefulWidget {
  @override
  _GoogleFormPageState createState() => _GoogleFormPageState();
}

class _GoogleFormPageState extends State<GoogleFormPage> {
  final _formKey = new GlobalKey<FormState>();
  GoogleFormModel googleFormModel =
      GoogleFormModel(googleFormLink: '', imageName: '');
  final picker = ImagePicker();
  CategoryModel? selectedCategory;

  TextStyle boldHeadlineStyle() =>
      CustomStyles.boldHeadingText().copyWith(fontSize: 20);

  @override
  void initState() {
    super.initState();
    StaticProvider.of<QuizCreationViewModel>(context).getCategories(context);
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Consumer<QuizCreationViewModel>(
          builder: (context, quizCreationVM, _) {
            var categories = quizCreationVM.categories;
            return SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).viewPadding.top + 24),
                  child: Column(
                    children: [
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          Align(
                            alignment: Alignment.centerRight,
                            child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                child: CustomBackButton()),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              'اختبار قوقل فورمز',
                              style: CustomStyles.boldSmallText()
                                  .copyWith(fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'لو عندك اختبار جاهز على قوقل فورمز، تقدرتنشره على سؤول بكل سهولة!\n\nكل اللي عليك تسوي إنك تتبع الخطوات الشسهلة تحت:',
                              style: boldHeadlineStyle(),
                            ),
                            SizedBox(
                              height: 48,
                            ),
                            Text(
                              'رابط الاختبار',
                              style: boldHeadlineStyle(),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            OutlinedTextFormField(
                              hintText: 'قوقل فورمز',
                              borderColor: Color(0xFFFED766),
                              onChanged: (newVal) {
                                setState(() {
                                  googleFormModel.googleFormLink = newVal;
                                });
                              },
                              validator: (val) {
                                if (val?.isEmpty ?? true) {
                                  return 'Field cannot be empty';
                                }
                              },
                            ),
                            SizedBox(
                              height: 24,
                            ),
                            Text(
                              'صورة عرض للاختبار',
                              style: boldHeadlineStyle(),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 4.0, bottom: 8.0),
                              child: Text(
                                'عشان تعرف الجمهور على محتوى الاختبار',
                                style: CustomStyles.regularSmallText(),
                              ),
                            ),
                            OutlinedTextFormField(
                              hintText: 'اسم الصورة',
                              borderColor: Color(0xFFFED766),
                              prefixIcon: GestureDetector(
                                  onTap: () async {
                                    googleFormModel.image = await getImage();
                                  },
                                  child: googleFormModel.image != null
                                      ? Image.file(googleFormModel.image!)
                                      : SvgPicture.asset(
                                          'assets/add-photo-icon.svg')),
                              onChanged: (newVal) {
                                setState(() {
                                  googleFormModel.imageName = newVal;
                                });
                              },
                              validator: (val) {
                                if (val?.isEmpty ?? true) {
                                  return 'Field cannot be empty';
                                }
                              },
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Text(
                                'تدعم الصيغ: png - jpg - jpeg - gif\n\nحد أقصى: 2 ميقا بايت',
                                style: TextStyle(
                                  color: CustomStyles.warningColor,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 24,
                            ),
                            Text(
                              'التصنيفات',
                              style: boldHeadlineStyle(),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            categories == null
                                ? Center(child: CircularProgressIndicator())
                                : Container(
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    decoration: BoxDecoration(
                                      color: CustomStyles.tabBorderColor2,
                                      border: Border.all(
                                        width: 1,
                                        color: CustomStyles.tabColor,
                                      ),
                                      borderRadius: BorderRadius.circular(
                                        8.0,
                                      ),
                                    ),
                                    child: DropdownButton<CategoryModel>(
                                      value:
                                          selectedCategory ?? categories.first,
                                      isExpanded: true,
                                      iconSize: 30,
                                      underline: Container(),
                                      onChanged: (newVal) {
                                        setState(() {
                                          selectedCategory = newVal;
                                        });
                                      },
                                      items: categories
                                          .map<DropdownMenuItem<CategoryModel>>(
                                            (e) =>
                                                DropdownMenuItem<CategoryModel>(
                                              child: Text(
                                                e.name,
                                                style:
                                                    TextStyle(fontSize: 20.0),
                                              ),
                                              value: e,
                                            ),
                                          )
                                          .toList(),
                                    ),
                                  ),
                            SizedBox(
                              height: 40,
                            ),
                            FormSubmitButton(
                              title: 'استورد الكويز',
                              onPressed: () async {
                                if (!_formKey.currentState!.validate()) {
                                  return;
                                }
                                googleFormModel.category =
                                    selectedCategory ?? (categories?.first);
                                print(googleFormModel.toMap());
                                var userModel =
                                    StaticProvider.of<UserViewModel>(context)
                                        .userModel;
                                bool success = await StaticProvider.of<
                                        QuizCreationViewModel>(context)
                                    .addGoogleForm(
                                  context: context,
                                  googleFormModel: googleFormModel,
                                  jwt: userModel?.jwt,
                                  userId: userModel?.id,
                                );
                                print(success);
                              },
                            ),
                            SizedBox(
                              height: 40,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Future<File?> getImage() async {
    File? file;
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        file = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
    return file;
  }
}

class QuizCreationTabs {
  final String id;
  final String title;
  final Function() onPressed;

  QuizCreationTabs({
    required this.id,
    required this.title,
    required this.onPressed,
  });
}
