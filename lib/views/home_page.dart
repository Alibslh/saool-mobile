import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:saool/constants/constants.dart';
import 'package:saool/models/category_model.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/quiz_view_model.dart';
import 'package:saool/view_models/user_view_model.dart';
import 'package:saool/views/create_quiz_page.dart';
import 'package:saool/views/sign_in_page.dart';
import 'package:saool/widgets/home_category_container.dart';

import '../widgets/expandable_fab.dart';
import 'google_form_page.dart';
import 'sign_up_page.dart';
import 'user_profile_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  int _selectedIndex = 1;
  bool showOverlay = false;
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      value: 0.0,
      duration: const Duration(milliseconds: 250),
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    Constants.resetIndex();
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.white,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: _buildExpandableFab(context),
        body: Stack(
          children: [
            SingleChildScrollView(
              physics:
                  showOverlay ? const NeverScrollableScrollPhysics() : null,
              child: Stack(
                children: [
                  _buildTopView(context),
                  _buildMainView(context),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: _buildBottomNavigationBar(),
            ),
            showOverlay
                ? Opacity(
                    opacity: 0.4,
                    child: Container(
                      color: Colors.black,
                      width: MediaQuery.of(context).size.width,
                      height: double.infinity,
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  ExpandableFab _buildExpandableFab(BuildContext context) {
    return ExpandableFab(
      controller: _controller,
      onToggle: (newVal) {
        setState(() {
          showOverlay = newVal;
        });
      },
      distance: 60.0,
      children: [
        ActionButton(
          onPressed: () {
            _handleFAB();
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => CreateQuizPage(
                      forQuiz: false,
                    )));
          },
          icon: SvgPicture.asset(
            'assets/nav-option-chat.svg',
          ),
          label: 'إضافة سؤال',
        ),
        ActionButton(
          onPressed: () {
            _handleFAB();
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => CreateQuizPage()));
          },
          icon: SvgPicture.asset(
            'assets/nav-option-text.svg',
          ),
          // icon: Image.asset(
          //   'assets/nav-option-text.svg',
          // ),
          label: 'إضافة اختبار',
        ),
        ActionButton(
          onPressed: () {
            _handleFAB();
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => GoogleFormPage()));
          },
          icon: SvgPicture.asset(
            'assets/nav-option-google.svg',
          ),
          label: 'قوقل فورم',
        ),
      ],
    );
  }

  BottomNavigationBar _buildBottomNavigationBar() {
    return BottomNavigationBar(
      selectedLabelStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            'assets/nav-account.svg',
            color: _selectedIndex == 0 ? Color(0xFF2AB7CA) : Colors.black,
          ),
          label: 'الملف الشخصي',
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            'assets/nav-home.svg',
            color: _selectedIndex == 1 ? Color(0xFF2AB7CA) : Colors.black,
          ),
          label: 'الرئيسية',
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Color(0xFF2AB7CA),
      onTap: _onItemTapped,
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    if (_selectedIndex == 0) {
      _handleFAB();
      bool isSignedIn = StaticProvider.of<UserViewModel>(context).isSignedIn();
      if (!isSignedIn) {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => SignInPage()))
            .then((value) {
          _resetIndexToHome();
        });
      } else {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => UserProfilePage()))
            .then((value) {
          _resetIndexToHome();
        });
      }
    }
  }

  void _resetIndexToHome() {
    setState(() {
      _selectedIndex = 1;
    });
  }

  Widget _buildTopView(BuildContext context) {
    return Consumer<UserViewModel>(
      builder: (context, userVM, _) => Container(
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.25,
              child: Image.asset(
                'assets/home-top-bg.jpg',
                fit: BoxFit.cover,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0xFFFED788),
                    Color(0x00FEB666),
                  ],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft,
                  stops: [0.35, 1],
                ),
              ),
              height: MediaQuery.of(context).size.height * 0.25,
            ),
            Container(
              padding: EdgeInsets.only(
                  left: 20.0,
                  right: 20.0,
                  top: MediaQuery.of(context).viewPadding.top,
                  bottom: 36.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Image.asset(
                        'assets/saool logo.png',
                        width: 83,
                      ),
                      Text(
                        'مرحبا ${userVM.getUserName() ?? ''} 👋',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      // INFO: Removed until backend is done
                      // userVM.isSignedIn()
                      //     ? Row(
                      //         children: [
                      //           Text(
                      //             'مجموع النقاط :',
                      //             style: TextStyle(
                      //                 fontSize: 14,
                      //                 fontWeight: FontWeight.bold),
                      //           ),
                      //           SizedBox(
                      //             width: 5,
                      //           ),
                      //           Text(
                      //             '4,456',
                      //             style: TextStyle(
                      //                 fontSize: 14,
                      //                 fontWeight: FontWeight.bold),
                      //           ),
                      //         ],
                      //       )
                      //     : Text(
                      //         'أنشئ حسابك الآن من هنا',
                      //         style: TextStyle(
                      //             fontSize: 14, fontWeight: FontWeight.bold),
                      //       ),
                      // SizedBox(
                      //   height: 15,
                      // ),
                    ],
                  ),
                  userVM.isSignedIn()
                      ? Container()
                      : ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.white),
                            foregroundColor:
                                MaterialStateProperty.all(Colors.black),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                            ),
                          ),
                          onPressed: () {
                            if (!userVM.isSignedIn()) {
                              // Create new account
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => SignUpPage()));
                            }
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.38,
                            padding: EdgeInsets.symmetric(vertical: 8.0),
                            alignment: Alignment.center,
                            child:
                                // INFO: Removed until backend is done
                                // userVM.isSignedIn()
                                //     ?
                                //     Row(
                                //         mainAxisAlignment: MainAxisAlignment.center,
                                //         children: [
                                //           Text(
                                //             'أكسب أوسمة أكثر',
                                //             style: TextStyle(
                                //                 fontSize: 14,
                                //                 fontWeight: FontWeight.bold),
                                //           ),
                                //           SizedBox(
                                //             width: 5,
                                //           ),
                                //           Text(
                                //             '✨',
                                //             style: TextStyle(
                                //                 fontSize: 20,
                                //                 fontWeight: FontWeight.bold),
                                //           ),
                                //         ],
                                //       )
                                //     :
                                Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'حساب جديد',
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  '📲',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMainView(BuildContext context) {
    return Consumer<QuizViewModel>(
      builder: (context, quizVM, _) {
        List<CategoryModel>? categories = quizVM.getCategories();
        return Padding(
          padding: EdgeInsets.only(
            top: MediaQuery.of(context).size.height * 0.225,
          ),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(14),
                topRight: Radius.circular(14),
              ),
            ),
            padding: EdgeInsets.symmetric(vertical: 24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text(
                              'تحدى ناس ثانيين وكسر راسهم',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  ?.copyWith(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Text(
                            'قريبا ⏳',
                            style: Theme.of(context)
                                .textTheme
                                .headline6
                                ?.copyWith(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    // Padding(
                    //     padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    //     child: Text(
                    //       'قريبا ⏳',
                    //       style: Theme.of(context)
                    //       .textTheme
                    //       .headline6
                    //       ?.copyWith(fontWeight: FontWeight.bold)
                    //     )),
                    SizedBox(
                      height: 10,
                    ),
                    Stack(
                      alignment: Alignment.center,
                      children: [
                        Image.asset(
                          'assets/head-to-head-image.png',
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 60.0),
                          child: Text(
                            'الاختبار اللايڤ!',
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w800),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Text(
                          'مالك في المنافسة؟ جرب مجموعتنا',
                          style: Theme.of(context)
                              .textTheme
                              .headline6
                              ?.copyWith(fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      quizVM.quizzes == null
                          ? Container(
                              height: MediaQuery.of(context).size.height * 0.25,
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            )
                          : Column(
                              children:
                                  getCategoriesWidgets(quizVM, categories),
                            ),
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  List<Widget> getCategoriesWidgets(
      QuizViewModel quizVM, List<CategoryModel>? categories) {
    if (categories == null || categories.isEmpty) {
      return [
        Center(
          child: Text('لم يتم العثور على اختبارات'),
        )
      ];
    }

    List<Widget> list = [];
    Widget first = HomeCategoryContainer(
      title: categories[0].name,
      isDense: false,
      imageUrl: categories[0].imageUrl ??
          quizVM.getCategoryImageUrl(categories[0].id),
      id: categories[0].id,
      onTap: () => _handleFAB(),
    );

    Widget second = Container();
    if (categories.length >= 2) {
      second = HomeCategoryContainer(
        title: categories[1].name,
        isDense: false,
        imageUrl: categories[1].imageUrl ??
            quizVM.getCategoryImageUrl(categories[1].id),
        id: categories[1].id,
        onTap: () => _handleFAB(),
      );
    }
    list.add(
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [first, second],
      ),
    );
    list.add(
      SizedBox(height: 15),
    );

    for (int i = 2; i < categories.length; i = i + 3) {
      Widget? e1, e2, e3;
      e1 = e2 = e3 = Container(
        width: MediaQuery.of(context).size.width * 0.283,
        height: MediaQuery.of(context).size.height * 0.113,
      );
      var cat = categories[i];
      e1 = HomeCategoryContainer(
        title: cat.name,
        isDense: true,
        imageUrl: cat.imageUrl ?? quizVM.getCategoryImageUrl(cat.id),
        id: cat.id,
        onTap: () => _handleFAB(),
      );
      if ((i + 1) < categories.length) {
        var cat = categories[i + 1];
        e2 = HomeCategoryContainer(
          title: cat.name,
          isDense: true,
          imageUrl: cat.imageUrl ?? quizVM.getCategoryImageUrl(cat.id),
          id: cat.id,
          onTap: () => _handleFAB(),
        );
      }
      if ((i + 2) < categories.length) {
        var cat = categories[i + 2];
        e3 = HomeCategoryContainer(
          title: cat.name,
          isDense: true,
          imageUrl: cat.imageUrl ?? quizVM.getCategoryImageUrl(cat.id),
          id: cat.id,
          onTap: () => _handleFAB(),
        );
      }
      List<Widget> rowChildren = [];
      rowChildren.add(e1);
      rowChildren.add(e2);
      rowChildren.add(e3);

      list.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: rowChildren,
      ));
      list.add(
        SizedBox(height: 15),
      );
    }

    return list;
  }

  void _handleFAB() {
    // TODO: Handle the overlay as well as FAB close before going to the next screen
    setState(() {
      _controller.reverse();
      showOverlay = false;
    });
  }
}
