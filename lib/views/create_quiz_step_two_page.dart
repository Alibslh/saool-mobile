import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import 'package:saool/constants/custom_styles.dart';
import 'package:saool/constants/quiz_constants.dart';
import 'package:saool/constants/quiz_creation_tabs.dart';
import 'package:saool/models/choice_model.dart';
import 'package:saool/models/question_model.dart';
import 'package:saool/utils/snack_utils.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/quiz_creation_view_model.dart';
import 'package:saool/view_models/user_view_model.dart';
import 'package:saool/widgets/add_image_icon.dart';
import 'package:saool/widgets/choice_tile.dart';
import 'package:saool/widgets/custom_back_button.dart';
import 'package:saool/widgets/form_submit_button.dart';
import 'package:saool/widgets/outlined_text_form_field.dart';
import 'package:saool/widgets/round_category_tile.dart';
import 'package:saool/widgets/type_tile.dart';

class CreateQuizStepTwoPage extends StatefulWidget {
  @override
  _CreateQuizStepTwoPageState createState() => _CreateQuizStepTwoPageState();
}

class _CreateQuizStepTwoPageState extends State<CreateQuizStepTwoPage> {
  final _formKey = new GlobalKey<FormState>();
  final List<String> selectedTabIds = ['1', '2'];
  int choiceCount = 1;
  String selectedTab = '1';
  TextStyle boldHeadlineStyle() =>
      CustomStyles.boldHeadingText().copyWith(fontSize: 20);
  Map<String, bool> showChoicesMap = {
    '0': false,
  };
  late bool isCategorical;
  List<ChoiceType> choiceTypes = [ChoiceType(id: '1', type: '')];
  final picker = ImagePicker();

  Future<File?> getImage() async {
    File? file;
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    // final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        file = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
    return file;
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Consumer<QuizCreationViewModel>(
          builder: (context, quizCreationVM, _) {
            isCategorical = quizCreationVM.isCategorical();
            return SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).viewPadding.top + 24),
                  child: Column(
                    children: [
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            child: CustomBackButton(),
                          ),
                          Text(
                            'إنشاء اختبار تصنيفي',
                            style: CustomStyles.boldSmallText()
                                .copyWith(fontSize: 16),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      _buildTabs(context, quizCreationVM),
                      SizedBox(
                        height: 24,
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 24.0, vertical: 8.0),
                          child: Text(
                            'لنبدأ بالنتائج المراد عرضها للمختبرين:',
                            style: boldHeadlineStyle(),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 24.0, right: 24.0, bottom: 24.0),
                          child: Text(
                            'إضافة الصور إلزامية، ويفضل تكون مستطيلة',
                            style: CustomStyles.regularSmallText()
                                .copyWith(fontSize: 16.0),
                          ),
                        ),
                      ),
                      _buildContent(context, quizCreationVM),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: FormSubmitButton(
                          title: 'مراجعة ونشر',
                          onPressed: () {
                            if (!_formKey.currentState!.validate()) {
                              return;
                            }
                            StaticProvider.of<QuizCreationViewModel>(context)
                                .uploadImages(
                              context,
                              jwt: StaticProvider.of<UserViewModel>(context)
                                  .userModel
                                  ?.jwt,
                            )
                                .then((_) {
                              print(quizCreationVM.quiz?.toJson());
                              quizCreationVM
                                  .addQuiz(context,
                                      userPermissionsUser:
                                          StaticProvider.of<UserViewModel>(
                                                  context)
                                              .userModel
                                              ?.toUserPermissionsUser())
                                  .then(
                                (value) {
                                  if (value) {
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                  }
                                },
                              );
                            });
                          },
                          isEnabled: true,
                        ),
                      ),
                      SizedBox(
                        height: 24.0,
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Container _buildContent(
      BuildContext context, QuizCreationViewModel quizCreationVM) {
    return Container(
      decoration: CustomStyles.roundBorderedShadowBoxDecoration(
          contentColor: Theme.of(context).scaffoldBackgroundColor),
      margin: const EdgeInsets.symmetric(horizontal: 16.0),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          isCategorical
              ? Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: Column(
                        children: choiceTypes
                            .asMap()
                            .map(
                              (i, e) => MapEntry(
                                i,
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 16.0),
                                  child: OutlinedTextFormField(
                                    hintText: 'فئة ${i + 1}',
                                    borderColor: Color(0xFFFED766),
                                    suffixIcon: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          choiceTypes.remove(e);
                                        });
                                      },
                                      child: Icon(
                                        Icons.close,
                                        color: CustomStyles.warningColor,
                                      ),
                                    ),
                                    onChanged: (newVal) {
                                      setState(() {
                                        e.type = newVal;
                                      });
                                    },
                                    validator: (val) {
                                      if (val?.isEmpty ?? true) {
                                        return 'Field cannot be empty';
                                      }
                                    },
                                  ),
                                ),
                              ),
                            )
                            .values
                            .toList(),
                      ),
                    ),
                    TypeTile(
                      onPressed: () {
                        setState(() {
                          choiceTypes.add(ChoiceType(
                              id: (choiceTypes.length + 1).toString(),
                              type: ''));
                        });
                      },
                    ),
                    SizedBox(
                      height: 24.0,
                    ),
                    Divider(),
                    SizedBox(
                      height: 24.0,
                    ),
                  ],
                )
              : Container(),
          quizCreationVM.quiz != null
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: quizCreationVM.quiz!.questions
                      .asMap()
                      .map(
                        (questionIndex, question) {
                          return _buildQuestionMapEntry(
                              questionIndex, context, question, quizCreationVM);
                        },
                      )
                      .values
                      .toList(),
                )
              : Container(),
          GestureDetector(
            onTap: () {
              if ((quizCreationVM.quiz?.questions.length ?? 0) >=
                  QuizConstants.maxQuestions) {
                // 'No more questions can be added'
                SnackUtils.showSnack(
                    context, 'لا يمكن إضافة المزيد من الأسئلة');
                return;
              }
              quizCreationVM.addDummyQuestion();
              showChoicesMap[(quizCreationVM.quiz!.questions.length - 1)
                  .toString()] = false;
            },
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                border: Border.all(
                  width: 2,
                  color: CustomStyles.tabBorderColor,
                ),
              ),
              padding: EdgeInsets.only(
                  left: 20.0, right: 24.0, top: 12.0, bottom: 12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'إضافة سؤال جديد',
                    style: CustomStyles.boldHeadingText(),
                  ),
                  SvgPicture.asset('assets/plus-icon.svg'),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  MapEntry<int, Column> _buildQuestionMapEntry(
      int questionIndex,
      BuildContext context,
      QuestionModel question,
      QuizCreationViewModel quizCreationVM) {
    return MapEntry(
      questionIndex,
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              'سؤال ${questionIndex + 1}',
              style: CustomStyles.boldSmallText()
                  .copyWith(color: Theme.of(context).accentColor),
            ),
          ),
          SizedBox(
            height: 16.0,
          ),
          OutlinedTextFormField(
            hintText: 'انقر هنا لكتابة السؤال',
            borderColor: Color(0xFFFED766),
            onChanged: (newVal) {
              quizCreationVM.updateQuestion(
                question.id,
                newVal,
              );
            },
            validator: (val) {
              if (val?.isEmpty ?? true) {
                return 'Field cannot be empty';
              }
            },
          ),
          SizedBox(
            height: 16.0,
          ),
          quizCreationVM
                      .getQuestionImage(questionId: question.id)
                      ?.image
                      ?.file !=
                  null
              ? GestureDetector(
                  onTap: () {
                    handleQuestionImage(quizCreationVM, question);
                  },
                  child: Container(
                    child: Image.file(quizCreationVM
                        .getQuestionImage(questionId: question.id)!
                        .image!
                        .file),
                  ),
                )
              : AddQuestionImageContainer(
                  onPressed: () {
                    handleQuestionImage(quizCreationVM, question);
                  },
                ),
          SizedBox(
            height: 16,
          ),
          Column(
            children: question.choices
                .asMap()
                .map(
                  (i, e) => MapEntry(
                    i,
                    isCategorical
                        ? _buildCategoricalChoiceInput(
                            quizCreationVM, question, e)
                        : _buildNumericalChoiceInput(
                            i, quizCreationVM, question, e),
                  ),
                )
                .values
                .toList(),
          ),
          ChoiceTile(
            onPressed: () {
              if (question.choices.length >= QuizConstants.maxChoices) {
                // 'No more choices can be added'
                SnackUtils.showSnack(
                    context, 'لا يمكن إضافة المزيد من الخيارات');
                return;
              }
              quizCreationVM.addChoice(
                question.id,
                ChoiceModel(
                  id: '',
                  text: '',
                  type: '',
                  isCorrect: false,
                ),
              );
            },
          ),
          isCategorical
              ? Container()
              : Column(
                  children: [
                    SizedBox(
                      height: 16.0,
                    ),
                    GestureDetector(
                      onTap: () {
                        if (getChoices(question).isEmpty) {
                          // 'Please enter valid choices'
                          SnackUtils.showSnack(
                              context, 'الرجاء إدخال اختيارات صحيحة');
                          return;
                        }
                        if (!showChoicesMap[questionIndex.toString()]!) {
                          setState(() {
                            showChoicesMap[questionIndex.toString()] = true;
                          });
                        }
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                              width: 1, color: CustomStyles.tabColor),
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        padding: const EdgeInsets.symmetric(
                            vertical: 16.0, horizontal: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                showChoicesMap[questionIndex.toString()] ??
                                        false
                                    ? Container(
                                        width: 16,
                                      )
                                    : Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Icon(
                                          Icons.add_circle_rounded,
                                          size: 35,
                                          color: Theme.of(context).accentColor,
                                        ),
                                      ),
                                Text(
                                  'انقر هنا لكتابة الجواب',
                                  style: TextStyle(
                                    fontSize: 20.0,
                                  ),
                                ),
                              ],
                            ),
                            showChoicesMap[questionIndex.toString()] ?? false
                                ? _buildDropDown(quizCreationVM, question)
                                : Container(),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
          SizedBox(
            height: 36.0,
          ),
        ],
      ),
    );
  }

  void handleQuestionImage(
      QuizCreationViewModel quizCreationVM, QuestionModel question) {
    getImage().then((questionImageFile) {
      if (questionImageFile != null) {
        quizCreationVM.addQuestionImage(
            questionId: question.id, file: questionImageFile);
      }
    });
  }

  Padding _buildNumericalChoiceInput(
      int i,
      QuizCreationViewModel quizCreationVM,
      QuestionModel question,
      ChoiceModel e) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: OutlinedTextFormField(
        hintText: 'نتيجة ${i + 1}',
        borderColor: Color(0xFFFED766),
        prefixIcon: quizCreationVM
                    .getChoiceImage(questionId: question.id, choiceId: e.id)
                    ?.file !=
                null
            ? Container(
                margin: EdgeInsets.symmetric(horizontal: 8.0),
                child: GestureDetector(
                  onTap: () {
                    handleChoiceImage(quizCreationVM, question, e);
                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.13,
                      height: MediaQuery.of(context).size.width * 0.13,
                      child: Image.file(
                        quizCreationVM
                            .getChoiceImage(
                                questionId: question.id, choiceId: e.id)!
                            .file,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              )
            : AddImageIcon(
                onPressed: () {
                  handleChoiceImage(quizCreationVM, question, e);
                },
              ),
        suffixIcon: GestureDetector(
          onTap: () {
            quizCreationVM.removeChoice(question.id, e.id);
          },
          child: Icon(
            Icons.close,
            color: CustomStyles.warningColor,
          ),
        ),
        onChanged: (newVal) {
          quizCreationVM.updateChoice(
            question.id,
            e.id,
            newVal,
          );
        },
        validator: (val) {
          if (val?.isEmpty ?? true) {
            return 'Field cannot be empty';
          }
        },
      ),
    );
  }

  void handleChoiceImage(QuizCreationViewModel quizCreationVM,
      QuestionModel question, ChoiceModel e) {
    getImage().then((choiceImageFile) {
      if (choiceImageFile != null) {
        quizCreationVM.addChoiceImage(
            questionId: question.id, choiceId: e.id, file: choiceImageFile);
      }
    });
  }

  Padding _buildCategoricalChoiceInput(QuizCreationViewModel quizCreationVM,
      QuestionModel question, ChoiceModel choice) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
            color: Color(0xFFE6E6EA),
            width: 2,
            style: BorderStyle.solid,
          ),
        ),
        child: Column(
          children: [
            TextFormField(
              decoration: InputDecoration(
                prefixIcon: quizCreationVM
                            .getChoiceImage(
                                questionId: question.id, choiceId: choice.id)
                            ?.file !=
                        null
                    ? Container(
                        margin: EdgeInsets.symmetric(horizontal: 8.0),
                        child: GestureDetector(
                          onTap: () {
                            handleChoiceImage(quizCreationVM, question, choice);
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.13,
                              height: MediaQuery.of(context).size.width * 0.13,
                              child: Image.file(
                                quizCreationVM
                                    .getChoiceImage(
                                        questionId: question.id,
                                        choiceId: choice.id)!
                                    .file,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                      )
                    : AddImageIcon(
                        onPressed: () async {
                          getImage().then((choiceImageFile) {
                            if (choiceImageFile != null) {
                              quizCreationVM.addChoiceImage(
                                  questionId: question.id,
                                  choiceId: choice.id,
                                  file: choiceImageFile);
                            }
                          });
                        },
                      ),
                suffixIcon: GestureDetector(
                  onTap: () {
                    quizCreationVM.removeChoice(question.id, choice.id);
                  },
                  child: Icon(
                    Icons.close,
                    color: CustomStyles.warningColor,
                  ),
                ),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
              ),
              onChanged: (newVal) {
                quizCreationVM.updateChoice(
                  question.id,
                  choice.id,
                  newVal,
                );
              },
              validator: (val) {
                if (val?.isEmpty ?? true) {
                  return 'Field cannot be empty';
                }
              },
            ),
            SizedBox(
              height: 8.0,
            ),
            _buildTypesDropDown(quizCreationVM, question, choice),
          ],
        ),
      ),
    );
  }

  Container _buildTabs(
      BuildContext context, QuizCreationViewModel quizCreationVM) {
    return Container(
      color: CustomStyles.tabColor,
      child: Column(
        children: [
          SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8.0),
            scrollDirection: Axis.horizontal,
            child: Row(
              children: QuizCreationConstants.quizCreationTabs.map(
                (e) {
                  return RoundTabItem(
                    text: e.title,
                    onPressed: () {
                      setState(() {
                        selectedTab = e.id;
                      });
                    },
                    isSelected: selectedTabIds.contains(e.id),
                    lightTheme: true,
                  );
                },
              ).toList(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '${quizCreationVM.quiz?.questions.length.toString() ?? 0}/10',
                  style: CustomStyles.boldSmallText()
                      .copyWith(color: CustomStyles.warningColor),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Text(
                  'يجب إكمال المؤشر حتى يمكنك النشر',
                  style: CustomStyles.regularSmallText(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _buildDropDown(QuizCreationViewModel quizCreationVM, QuestionModel question) {
    List<ChoiceModel> choices = getChoices(question);
    if (choices.isEmpty) {
      return Container();
    }
    // None
    ChoiceModel noneChoice =
        ChoiceModel(id: '-1', text: 'لا أحد', type: '', isCorrect: false);

    choices.add(noneChoice);
    return Container(
      margin: EdgeInsets.only(top: 16.0),
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: CustomStyles.tabColor,
        ),
        borderRadius: BorderRadius.circular(
          15.0,
        ),
      ),
      child: DropdownButton<ChoiceModel>(
        value: choices.firstWhere((e) => e.isCorrect, orElse: () => noneChoice),
        isExpanded: true,
        iconSize: 30,
        underline: Container(),
        onChanged: (ChoiceModel? newValue) {
          if (newValue != null)
            quizCreationVM.setSingleChoiceAsCorrect(question.id, newValue.id);
        },
        items: choices.map<DropdownMenuItem<ChoiceModel>>((ChoiceModel value) {
          return DropdownMenuItem<ChoiceModel>(
            value: value,
            child: Text(
              value.text,
              style: TextStyle(
                  fontSize: 20.0, color: value.id == '-1' ? Colors.grey : null),
            ),
          );
        }).toList(),
      ),
    );
  }

  _buildTypesDropDown(QuizCreationViewModel quizCreationVM,
      QuestionModel question, ChoiceModel choice) {
    var dropdownChoiceTypes = choiceTypes.map((e) => e).toList();
    dropdownChoiceTypes.removeWhere((e) => e.type.replaceAll(' ', '') == '');
    dropdownChoiceTypes.add(ChoiceType(id: '-1', type: 'لا أحد'));
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      decoration: BoxDecoration(
        color: CustomStyles.tabBorderColor2,
        border: Border.all(
          width: 1,
          color: CustomStyles.tabColor,
        ),
        borderRadius: BorderRadius.circular(
          8.0,
        ),
      ),
      child: DropdownButton<ChoiceType>(
        value: (choice.type != null && choice.type != '')
            ? choiceTypes.firstWhere((c) => c.type == choice.type,
                orElse: () => dropdownChoiceTypes.last)
            : dropdownChoiceTypes.last,
        isExpanded: true,
        iconSize: 30,
        underline: Container(),
        onChanged: (ChoiceType? newValue) {
          if (newValue != null && newValue.id != '-1')
            quizCreationVM.updateChoiceType(
                question.id, choice.id, newValue.type);
        },
        items: dropdownChoiceTypes
            .map<DropdownMenuItem<ChoiceType>>((ChoiceType value) {
          return DropdownMenuItem<ChoiceType>(
            value: value,
            child: Text(
              value.type,
              style: TextStyle(
                  fontSize: 20.0, color: value.id == '-1' ? Colors.grey : null),
            ),
          );
        }).toList(),
      ),
    );
  }

  List<ChoiceModel> getChoices(QuestionModel question) {
    List<ChoiceModel> choices = question.choices.toList();

    choices.removeWhere((c) => c.text.trim() == '');
    return choices;
  }
}

class AddQuestionImageContainer extends StatelessWidget {
  const AddQuestionImageContainer({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          color: CustomStyles.tabBorderColor2,
          borderRadius: BorderRadius.circular(10.0),
        ),
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Center(
          child: Column(
            children: [
              SvgPicture.asset('assets/plus-rounded-icon.svg'),
              SizedBox(
                height: 4,
              ),
              Text(
                'رفع صورة',
                style: TextStyle(fontSize: 12.0),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ChoiceType {
  String id;
  String type;
  ChoiceType({
    required this.id,
    required this.type,
  });
}
