class NetworkEndpoints {
  static const String PROD_API_URL = 'https://api.saool.io';
  static const String DEV_API_URL = 'https://saool-dev.xyz';
  static const String ATTEMPTS_URL = 'https://saool.xyz';
  static const String WEBSITE_URL = 'https://saool.io';

  static const String API_URL = PROD_API_URL;
  static const String SIGN_UP_API = API_URL + '/auth/local/register';
  static const String SIGN_IN_API = API_URL + '/auth/local';
  static const String GET_USER_PROFILE_API = API_URL + '/users/me';
  static const String ADD_RATING = API_URL + '/ratings';
  static const String GET_RATING = API_URL + '/ratings';
  static const String POST_QUIZ_ATTEMPTS = ATTEMPTS_URL + '/quiz-attempts';
  static const String ADD_QUIZ = API_URL + '/quizzes';
  static const String USERS = API_URL + '/users';
  static const String UPDATE_QUIZ = API_URL + '/quizzes';
  static const String UPLOAD_FILE = API_URL + '/upload';
  static const String GOOGLE_FORM_QUIZ = API_URL + '/quizzes/googleform';
}
