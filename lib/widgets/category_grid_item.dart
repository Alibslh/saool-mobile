import 'package:flutter/material.dart';
import 'package:saool/constants/custom_styles.dart';
import 'package:saool/models/label.dart';

class CategoryGridItem extends StatelessWidget {
  const CategoryGridItem({
    Key? key,
    required this.title,
    required this.imageUrl,
    required this.labels,
    required this.onPressed,
    required this.type,
    required this.loadingAssetPath,
  }) : super(key: key);

  final String title;
  final String imageUrl;
  final List<Label> labels;
  final Function() onPressed;
  final String type;
  final String loadingAssetPath;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: MediaQuery.of(context).size.height * 0.32,
        width: (MediaQuery.of(context).size.width / 2) - 24,
        decoration: CustomStyles.roundShadowBoxDecoration(),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15),
                        bottomLeft: Radius.circular(0),
                        bottomRight: Radius.circular(0)),
                  ),
                  clipBehavior: Clip.antiAlias,
                  child: imageUrl.isEmpty
                      ? Image.asset(loadingAssetPath)
                      : Image.network(
                          imageUrl,
                          fit: BoxFit.cover,
                          frameBuilder: (BuildContext context, Widget child,
                              int? frame, bool wasSynchronouslyLoaded) {
                            if (frame != null && frame >= 0) {
                              return child;
                            }
                            return Image.asset(loadingAssetPath);
                          },
                        ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, right: 8.0, top: 28.0, bottom: 0),
                    child: Text(
                      title,
                      style: CustomStyles.regularSmallText()
                          .copyWith(fontSize: 16.0),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              top: MediaQuery.of(context).size.height * 0.18,
              child: Row(
                // children: labels
                children: [
                  type == "numerical"
                      ? Label(
                          title: 'تحدي أعلى نتيجة',
                          backgroundColor: Color(0xFFFED766),
                          textColor: Colors.black,
                        )
                      : Label(
                          title: 'تحدي التصنيف',
                          backgroundColor: Color(0xFF2AB7CA),
                          textColor: Colors.white,
                        ),
                ]
                    .map(
                      (e) => Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: MediaQuery.of(context).size.height * 0.04,
                        margin: EdgeInsets.symmetric(horizontal: 24),
                        decoration: BoxDecoration(
                          color: e.backgroundColor,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Center(
                          child: Text(
                            e.title,
                            style: CustomStyles.boldSmallText()
                                .copyWith(fontSize: 12, color: e.textColor),
                          ),
                        ),
                      ),
                    )
                    .toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
