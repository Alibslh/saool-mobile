import 'package:flutter/material.dart';
import 'package:saool/constants/custom_styles.dart';

class VerticalIconContainer extends StatelessWidget {
  const VerticalIconContainer({
    Key? key,
    this.isSelected = false,
    required this.title,
    required this.assetPath,
  }) : super(key: key);

  final bool isSelected;
  final String title;
  final String assetPath;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.25,
      height: 128,
      decoration: BoxDecoration(
        color: !isSelected ? Colors.white : null,
        gradient: isSelected
            ? LinearGradient(
                begin: Alignment.bottomRight,
                end: Alignment.topLeft,
                colors: [Color(0xFFFEB666), Color(0xFFFED766)])
            : null,
        borderRadius: BorderRadius.circular(15.0),
      ),
      padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
      child: Column(
        children: [
          Image.asset(assetPath),
          Text(title, style: CustomStyles.boldSmallText()),
        ],
      ),
    );
  }
}
