import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:saool/constants/constants.dart';

class LoadingContainer extends StatefulWidget {
  final Widget child;
  final LoadingContainerController loadingContainerController;
  final Color? indicatorColor;
  final double? size;

  const LoadingContainer({
    Key? key,
    required this.child,
    required this.loadingContainerController,
    this.indicatorColor,
    this.size,
  }) : super(key: key);

  @override
  _LoadingContainerState createState() => _LoadingContainerState();
}

class _LoadingContainerState extends State<LoadingContainer> {
  @override
  void initState() {
    super.initState();

    this.widget.loadingContainerController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return _buildContent(context);
  }

  _buildContent(BuildContext context) {
    switch (this.widget.loadingContainerController.state) {
      case ButtonState.loading:
        return Theme(
          data: Theme.of(context).copyWith(
              accentColor: this.widget.indicatorColor == null
                  ? Theme.of(context).accentColor
                  : this.widget.indicatorColor),
          child: SizedBox(
              width: widget.size,
              height: widget.size,
              child: CircularProgressIndicator()),
        );
      case ButtonState.success:
        return Icon(Icons.check_rounded,
            color: this.widget.indicatorColor == null
                ? Theme.of(context).accentColor
                : this.widget.indicatorColor);
      // cases: ButtonState.idle, ButtonState.error
      default:
        return this.widget.child;
    }
  }
}

class LoadingContainerController with ChangeNotifier {
  ButtonState _loadingContainerState = ButtonState.idle;

  LoadingContainerController() {
    _loadingContainerState = ButtonState.idle;
  }

  ButtonState get state => _loadingContainerState;

  void idle() {
    _loadingContainerState = ButtonState.idle;
    notifyListeners();
  }

  void loading() {
    _loadingContainerState = ButtonState.loading;
    notifyListeners();
  }

  void success() {
    _loadingContainerState = ButtonState.success;
    notifyListeners();
  }

  void error() {
    _loadingContainerState = ButtonState.error;
    notifyListeners();
  }

  bool isLoading() => _loadingContainerState == ButtonState.loading;
}
