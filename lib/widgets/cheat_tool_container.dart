import 'package:flutter/material.dart';
import 'package:saool/constants/custom_styles.dart';

class CheatToolContainer extends StatelessWidget {
  const CheatToolContainer({
    Key? key,
    required this.title,
    required this.assetPath,
    required this.isAvailable,
  }) : super(key: key);

  final bool isAvailable;
  final String title;
  final String assetPath;

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: isAvailable ? 1.0 : 0.30,
      child: Container(
        width: 96,
        height: 128,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.0),
        ),
        padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
        child: Column(
          children: [
            Image.asset(assetPath),
            Text(title, style: CustomStyles.boldSmallText()),
          ],
        ),
      ),
    );
  }
}
