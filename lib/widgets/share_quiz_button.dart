import 'package:flutter/cupertino.dart';
import 'package:saool/config/network_endpoints.dart';
import 'package:saool/constants/custom_styles.dart';
import 'package:saool/models/quiz_model_extended.dart';
import 'package:share_plus/share_plus.dart';

class ShareQuizButton extends StatelessWidget {
  const ShareQuizButton({
    Key? key,
    required this.quiz,
  }) : super(key: key);

  final QuizModelExtended? quiz;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (quiz?.id != null) {
          String quizUrl = NetworkEndpoints.WEBSITE_URL + '/quiz/' + quiz!.id;
          Share.share(quizUrl, subject: 'Saool Quiz');
        }
      },
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [Color(0xFF2AB7CA), Color(0xFFFED766)]),
            borderRadius: BorderRadius.circular(20)),
        padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'شارك الاختبار',
              style: CustomStyles.boldHeadingText(),
            ),
            Text(
              '🚀',
              style: TextStyle(fontSize: 44),
            ),
          ],
        ),
      ),
    );
  }
}
