import 'package:flutter/material.dart';

class CustomBackButton extends StatelessWidget {
  const CustomBackButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, {bool isRTL = true}) {
    return Directionality(
      textDirection: isRTL ? TextDirection.rtl : TextDirection.ltr,
      child: GestureDetector(
        onTap: () => Navigator.of(context).pop(),
        child: Row(
          mainAxisAlignment:
              isRTL ? MainAxisAlignment.start : MainAxisAlignment.end,
          children: [
            Text(
              '→',
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).accentColor,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              width: 4.0,
            ),
            Text(
              'العودة',
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).accentColor,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      // RichText(
      //   text: TextSpan(
      //     text: 'العودة',
      //     style: TextStyle(
      //       fontSize: 16,
      //       color: Theme.of(context).accentColor,
      //       fontWeight: FontWeight.bold,
      //     ),
      //     children: [
      //       TextSpan(
      //         text: '→',
      //         style: TextStyle(
      //           fontSize: 20,
      //         ),
      //       )
      //     ],
      //   ),
      // ),
    );
  }
}
