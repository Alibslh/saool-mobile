import 'package:flutter/material.dart';
import 'package:saool/constants/custom_styles.dart';

class RoundTabItem extends StatelessWidget {
  const RoundTabItem({
    Key? key,
    required this.text,
    required this.onPressed,
    this.isSelected = false,
    this.lightTheme = false,
  }) : super(key: key);

  final String text;
  final Function() onPressed;
  final bool isSelected;
  final bool lightTheme;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: MediaQuery.of(context).size.width * 0.45,
        height: MediaQuery.of(context).size.height * 0.06,
        padding: EdgeInsets.symmetric(horizontal: 16),
        margin: EdgeInsets.symmetric(horizontal: 4),
        decoration: BoxDecoration(
          color: isSelected
              ? Colors.white
              : (lightTheme
                  ? CustomStyles.tabColor
                  : Theme.of(context).accentColor),
          borderRadius: BorderRadius.circular(20),
          border: Border.all(
            width: 2,
            color: lightTheme
                ? (isSelected
                    ? CustomStyles.tabBorderColor
                    : CustomStyles.tabBorderColor2)
                : Colors.white,
          ),
        ),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4.0),
            child: Text(
              text,
              style: CustomStyles.boldSmallText().copyWith(
                  fontSize: 12,
                  color: lightTheme
                      ? Colors.black
                      : (isSelected ? Colors.black : Colors.white)),
              maxLines: 1,
            ),
          ),
        ),
      ),
    );
  }
}
