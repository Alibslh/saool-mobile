import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OutlinedTextFormField extends StatelessWidget {
  const OutlinedTextFormField({
    Key? key,
    required this.hintText,
    this.prefixIcon,
    this.suffixIcon,
    this.onChanged,
    this.validator,
    this.obscureText = false,
    this.borderColor,
    this.maxLines,
    this.textInputFormatters,
    this.initialValue,
  }) : super(key: key);

  final String hintText;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final Function(String)? onChanged;
  final String? Function(String?)? validator;
  final bool obscureText;
  final Color? borderColor;
  final int? maxLines;
  final List<TextInputFormatter>? textInputFormatters;
  final String? initialValue;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: maxLines ?? 1,
      onChanged: onChanged,
      initialValue: initialValue,
      inputFormatters: textInputFormatters ?? [],
      validator: validator,
      obscureText: obscureText,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(
          fontSize: 20,
          color: Color(0xFF919191),
        ),
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
          borderSide: BorderSide(
            color: Color(0xFFE6E6EA),
            width: 2,
            style: BorderStyle.solid,
          ),
        ),
        focusedBorder: borderColor != null
            ? OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  color: borderColor!,
                  width: 2,
                  style: BorderStyle.solid,
                ),
              )
            : null,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
          borderSide: BorderSide(
            color: Color(0xFFE6E6EA),
            width: 2,
            style: BorderStyle.solid,
          ),
        ),
      ),
    );
  }
}
