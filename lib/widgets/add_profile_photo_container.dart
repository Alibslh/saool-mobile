import 'package:flutter/material.dart';
import 'package:saool/constants/custom_styles.dart';

class AddProfilePhotoContainer extends StatelessWidget {
  const AddProfilePhotoContainer({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              blurRadius: 15,
              offset: Offset(0, 2),
              color: Colors.black.withOpacity(0.16),
              spreadRadius: 0,
            ),
          ],
        ),
        padding: EdgeInsets.all(16.0),
        child: Text(
          'تغيير الصورة',
          style: CustomStyles.boldSmallText(),
        ),
      ),
    );
  }
}