import 'package:flutter/material.dart';
import 'package:saool/constants/custom_styles.dart';

class ComingSoonWidget extends StatelessWidget {
  const ComingSoonWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 16.0),
        child: Text(
          'Coming Soon',
          style: CustomStyles.extraBoldText(),
        ),
      ),
    );
  }
}
