import 'package:flutter/material.dart';
import 'package:saool/constants/custom_styles.dart';

class TypeTile extends StatelessWidget {
  const TypeTile({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          color: CustomStyles.tabBorderColor2,
          borderRadius: BorderRadius.circular(15.0),
        ),
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
        child: Center(
          child: Row(
            children: [
              Icon(
                Icons.add_circle_rounded,
                size: 35,
                color: Theme.of(context).accentColor,
              ),
              SizedBox(
                width: 16,
              ),
              Text(
                // category : فئة
                'إضافة فئة',
                style: TextStyle(
                  fontSize: 20.0,
                  color: Color(0xFF737373),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
