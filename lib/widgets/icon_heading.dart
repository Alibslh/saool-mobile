import 'package:flutter/material.dart';
import 'package:saool/constants/custom_styles.dart';

class IconHeading extends StatelessWidget {
  const IconHeading({
    Key? key,
    required this.title,
    required this.iconPath,
    required this.onPressed,
  }) : super(key: key);

  final String title;
  final String iconPath;
  final void Function() onPressed;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        decoration: CustomStyles.roundShadowBoxDecoration(),
        child: Padding(
          padding: const EdgeInsets.only(right: 16.0, top: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Image.asset(
                  iconPath,
                  width: 120,
                  height: 120,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
