import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:saool/constants/custom_styles.dart';

class AddImageIcon extends StatelessWidget {
  const AddImageIcon({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      child: GestureDetector(
        onTap: onPressed,
        child: Container(
          decoration: BoxDecoration(
            color: CustomStyles.tabBorderColor2,
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: SvgPicture.asset('assets/add-photo-icon.svg'),
        ),
      ),
    );
  }
}
