import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:saool/constants/constants.dart';
import 'package:saool/utils/common_utils.dart';
import 'package:saool/views/quiz_list_page.dart';

class HomeCategoryContainer extends StatelessWidget {
  const HomeCategoryContainer({
    Key? key,
    required this.title,
    this.iconImageUrl1,
    this.iconImageUrl2,
    this.isDense = false,
    required this.imageUrl,
    required this.id,
    this.onTap,
  }) : super(key: key);
  final String title;
  final String? iconImageUrl1;
  final String? iconImageUrl2;
  final bool isDense;
  final String? imageUrl;
  final String id;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    final RegExp emojiRegExp = RegExp(
        r'(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])');
    String? emoji = emojiRegExp.allMatches(title).last.group(0);
    // INFO: Handling case for 'anime category'
    if (id == '8') {
      emoji = '🦸🏼';
    }
    final String titleWithoutEmoji = title.replaceAll(emojiRegExp, '');
    final bool isSvg = imageUrl != null && (imageUrl!.split('.').last == 'svg');
    double width = MediaQuery.of(context).size.width * (isDense ? 0.283 : 0.44);
    double height =
        MediaQuery.of(context).size.height * (isDense ? 0.113 : 0.16);
    return GestureDetector(
      onTap: () {
        if (onTap != null) onTap!();
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => QuizListPage(
                  categoryId: id,
                )));
      },
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          gradient: Constants.getLinearGradient(),
          borderRadius: BorderRadius.circular(14.0),
        ),
        child: Stack(
          children: [
            isSvg
                ? Container(
                    width: width,
                    height: height,
                    child: SvgPicture.network(
                      CommonUtils.toHostUrl(imageUrl!),
                      fit: BoxFit.cover,
                    ),
                  )
                : imageUrl != null
                    ? Positioned(
                        left: 0,
                        bottom: 8,
                        child: Container(
                          width: isDense ? width / 1.8 : width / 1.6,
                          height: isDense ? height / 1.8 : height / 1.6,
                          decoration: BoxDecoration(
                            // INFO: Removed as no need to round images because we will receive correct cateogory images 
                            // shape: BoxShape.circle,
                            image: DecorationImage(
                                image: NetworkImage(
                                  CommonUtils.toHostUrl(imageUrl!),
                                ),
                            ),
                          ),
                        ),
                      )
                    : Container(),
            Container(
              padding:
                  EdgeInsets.only(left: 2.0, right: 8.0, top: 8.0, bottom: 4.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Container(
                      width: width / 2,
                      height: height / 4,
                      child: Text(
                        titleWithoutEmoji,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                            fontSize: isDense ? 12 : 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  emoji != null
                      ? Text(
                          emoji,
                          style: TextStyle(
                              fontSize: isDense
                                  ? (MediaQuery.of(context).size.height * 0.03)
                                  : (MediaQuery.of(context).size.height *
                                      0.058)),
                        )
                      : Container(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
