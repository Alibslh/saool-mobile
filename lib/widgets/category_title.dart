import 'package:flutter/material.dart';
import 'package:saool/constants/custom_styles.dart';

class CategoryTitle extends StatelessWidget {
  const CategoryTitle({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      alignment: Alignment.centerRight,
      child: Text(
        title,
        style: CustomStyles.extraBoldText().copyWith(fontSize: 22),
      ),
    );
  }
}
