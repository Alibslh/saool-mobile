import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:saool/constants/custom_styles.dart';

import 'custom_back_button.dart';

class OnboardingView extends StatelessWidget {
  const OnboardingView({
    Key? key,
    required this.formTitle,
    required this.children,
    required this.iconHeading,
  }) : super(key: key);

  final String formTitle;
  final List<Widget> children;
  final Widget iconHeading;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.only(
            left: 24, right: 24, top: MediaQuery.of(context).viewPadding.top),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                      alignment: Alignment.center,
                      child: SvgPicture.asset('assets/logo-2.svg')),
                  SizedBox(
                    height: 42,
                  ),
                  CustomBackButton(),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    'تسجيل حساب جديد في سؤول',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 24,
            ),
            Divider(
              color: Colors.grey[400],
            ),
            SizedBox(
              height: 42,
            ),
            Container(
              decoration: CustomStyles.roundShadowBoxDecoration(),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Stack(
                  children: [
                    Container(child: Image.asset('assets/bg-celebrate.png')),
                    Padding(
                      padding: const EdgeInsets.only(top: 48.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            formTitle,
                            style: TextStyle(
                              fontSize: 28,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Text(
                            'إنشاء حساب يعطيك القدرة إنك تنشئ اختباراتك الخاصة والاحتفاظ بقائمة الاختبارات اللي اختبرت سابقاً',
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Column(
                            children: children,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 24,
            ),
            iconHeading,
            SizedBox(
              height: 100,
            ),
          ],
        ),
      ),
    );
  }
}
