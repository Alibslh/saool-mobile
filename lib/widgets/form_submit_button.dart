import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class FormSubmitButton extends StatelessWidget {
  const FormSubmitButton({
    Key? key,
    required this.title,
    required this.onPressed,
    this.isEnabled = true,
    this.iconPath,
  }) : super(key: key);

  final String title;
  final void Function() onPressed;
  final bool isEnabled;
  final String? iconPath;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: !isEnabled ? Color(0xFFEFEFEF) : null,
            gradient: !isEnabled
                ? null
                : LinearGradient(
                    colors: [
                      Color(0xFFFED766),
                      Color(0xFFFEB666),
                    ],
                  )),
        padding: EdgeInsets.symmetric(horizontal: 36, vertical: 12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            SvgPicture.asset(
              isEnabled
                  ? (iconPath ?? 'assets/form-submit-button.svg')
                  : 'assets/next-faded-icon.svg',
              width: 50,
              height: 50,
            ),
          ],
        ),
      ),
    );
  }
}
