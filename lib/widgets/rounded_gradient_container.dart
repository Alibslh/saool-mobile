import 'package:flutter/material.dart';

class RoundedGradientContainer extends StatelessWidget {
  const RoundedGradientContainer({
    Key? key,
    required this.child,
    this.onPressed,
  }) : super(key: key);

  final Widget child;
  final Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            gradient: LinearGradient(
              colors: [
                Color(0xFFFED766),
                Color(0xFFFEB666),
              ],
            )),
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12.0),
        child: child,
      ),
    );
  }
}
