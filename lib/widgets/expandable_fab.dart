import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

@immutable
class ExpandableFab extends StatefulWidget {
  const ExpandableFab({
    Key? key,
    required this.distance,
    required this.children,
    required this.onToggle,
    required this.controller,
  }) : super(key: key);

  final double distance;
  final List<Widget> children;
  final Function(bool) onToggle;
  final AnimationController controller;

  @override
  _ExpandableFabState createState() => _ExpandableFabState();
}

class _ExpandableFabState extends State<ExpandableFab> {
  late final Animation<double> _expandAnimation;
  bool _open = false;

  List<double> rightValues = [-90, 0, 90];

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() {
      if (widget.controller.status == AnimationStatus.reverse) {
        _open = false;
      }
    });
    _open = false;

    _expandAnimation = CurvedAnimation(
      curve: Curves.fastOutSlowIn,
      reverseCurve: Curves.easeOutQuad,
      parent: widget.controller,
    );
  }

  @override
  void dispose() {
    widget.controller.dispose();
    super.dispose();
  }

  void _toggle() {
    // if (!_open){
    //   showDialog(context: context, builder: (context)=>Container());
    // }
    // else{
    //   Navigator.pop(context);
    // }
    setState(() {
      _open = !_open;
      if (_open) {
        widget.controller.forward();
      } else {
        widget.controller.reverse();
      }
    });
    widget.onToggle(_open);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: Stack(
        alignment: Alignment.bottomCenter,
        clipBehavior: Clip.none,
        children: [
          Positioned(bottom: 0, child: _buildTapToCloseFab()),
          ..._buildExpandingActionButtons(),
          Positioned(bottom: 0, child: _buildTapToOpenFab()),
        ],
      ),
    );
  }

  Widget _buildTapToCloseFab() {
    return Center(
      child: GestureDetector(
        onTap: _toggle,
        child: SvgPicture.asset(
          'assets/nav-add-2.svg',
        ),
      ),
    );
  }

  List<Widget> _buildExpandingActionButtons() {
    final children = <Widget>[];
    final count = widget.children.length;
    final step = 180.0 / (count - 1);
    for (var i = 0, angleInDegrees = 0.0;
        i < count;
        i++, angleInDegrees += step) {
      children.add(
        _ExpandingActionButton(
          directionInDegrees: angleInDegrees,
          maxDistance: widget.distance,
          progress: _expandAnimation,
          child: widget.children[i],
          right: rightValues[i],
        ),
      );
    }
    return children;
  }

  Widget _buildTapToOpenFab() {
    return IgnorePointer(
      ignoring: _open,
      child: AnimatedContainer(
        transformAlignment: Alignment.center,
        transform: Matrix4.diagonal3Values(
          _open ? 0.7 : 1.0,
          _open ? 0.7 : 1.0,
          1.0,
        ),
        duration: const Duration(milliseconds: 250),
        curve: const Interval(0.0, 0.5, curve: Curves.easeOut),
        child: AnimatedOpacity(
          opacity: _open ? 0.0 : 1.0,
          curve: const Interval(0.25, 1.0, curve: Curves.easeInOut),
          duration: const Duration(milliseconds: 250),
          child: GestureDetector(
            onTap: _toggle,
            child: Container(
              // TODO: Replace this button image with one with the correct size, to avoid it being clicked unnecessarily
              // color: Colors.amber,
              child: SvgPicture.asset(
                'assets/nav-add.svg',
              ),
            ),
          ),
        ),
      ),
    );
  }
}

@immutable
class _ExpandingActionButton extends StatelessWidget {
  _ExpandingActionButton({
    Key? key,
    required this.directionInDegrees,
    required this.maxDistance,
    required this.progress,
    required this.child,
    required this.right,
  }) : super(key: key);

  final double directionInDegrees;
  final double maxDistance;
  final Animation<double> progress;
  final Widget child;
  final double right;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: progress,
      builder: (context, child) {
        final offset = Offset.fromDirection(
          directionInDegrees * (math.pi / 180.0),
          progress.value * maxDistance,
        );
        return Positioned(
          right: -30 + (MediaQuery.of(context).size.width / 2) + right,
          bottom: 80.0 + offset.dy,
          child: Transform.rotate(
            angle: (1.0 - progress.value) * math.pi / 2,
            child: child!,
          ),
        );
      },
      child: (progress.status == AnimationStatus.reverse ||
              progress.status == AnimationStatus.dismissed)
          ? Container()
          : child,
    );
  }
}

@immutable
class ActionButton extends StatelessWidget {
  const ActionButton({
    Key? key,
    this.onPressed,
    required this.icon,
    required this.label,
  }) : super(key: key);

  final VoidCallback? onPressed;
  final Widget icon;
  final String label;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Material(
          shape: const CircleBorder(),
          clipBehavior: Clip.antiAlias,
          color: Colors.white,
          elevation: 2.0,
          child: IconTheme.merge(
            data: theme.accentIconTheme,
            child: IconButton(
              iconSize: 45,
              // padding: EdgeInsets.all(20.0),
              onPressed: onPressed,
              icon: icon,
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          label,
          style: TextStyle(
              color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}

@immutable
class FakeItem extends StatelessWidget {
  const FakeItem({
    Key? key,
    required this.isBig,
  }) : super(key: key);

  final bool isBig;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
      height: isBig ? 128.0 : 36.0,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(8.0)),
        color: Colors.grey.shade300,
      ),
    );
  }
}
