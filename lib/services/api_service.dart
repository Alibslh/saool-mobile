import 'dart:convert';
import 'dart:io';

import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:saool/config/network_endpoints.dart';
import 'package:saool/constants/file_upload_constants.dart';
import 'package:saool/models/profile_settings_model.dart';
import 'package:saool/models/quiz_model_extended.dart';
import 'package:saool/models/requests/add_rating_request.dart';
import 'package:saool/models/requests/google_form_request.dart';
import 'package:saool/models/requests/quiz_attempt_request.dart';
import 'package:saool/models/requests/sign_in_request.dart';
import 'package:saool/models/requests/sign_up_request.dart';
import 'package:saool/models/requests/update_quiz_image_request.dart';
import 'package:saool/models/requests/upload_file_request.dart';
import 'package:saool/models/responses/base_response.dart';
import 'package:saool/models/responses/error_response.dart';
import 'package:saool/models/responses/get_categories_response.dart';
import 'package:saool/models/responses/get_quiz_response.dart';
import 'package:saool/models/responses/get_quizzes_response.dart';
import 'package:saool/models/responses/get_rating_response.dart';
import 'package:saool/models/responses/get_user_profile_response.dart';
import 'package:saool/models/responses/graphql_error_response.dart';
import 'package:saool/models/responses/sign_in_response.dart';
import 'package:saool/models/responses/sign_up_response.dart';
import 'package:http/http.dart' as http;
import 'package:saool/models/responses/upload_file_response.dart';
import 'package:saool/models/user_permissions_user.dart';

import 'network_service.dart';

class ApiService {
  Future<BaseResponse> signUp({required SignUpRequest request}) async {
    http.Response response = await NetworkService()
        .post(url: NetworkEndpoints.SIGN_UP_API, body: request.toJson());
    String json = response.body;
    BaseResponse baseResponse;
    if (response.statusCode == 200) {
      final SignUpResponse signUpResponse = SignUpResponse.fromJson(json);
      baseResponse = signUpResponse;
    } else {
      final ErrorResponse errorResponse = ErrorResponse.fromJson(json);
      baseResponse = errorResponse;
    }
    return baseResponse;
  }

  Future<BaseResponse> signIn({required SignInRequest request}) async {
    http.Response response = await NetworkService()
        .post(url: NetworkEndpoints.SIGN_IN_API, body: request.toJson());
    String json = response.body;
    BaseResponse baseResponse;
    if (response.statusCode == 200) {
      final SignInResponse signInResponse = SignInResponse.fromJson(json);
      baseResponse = signInResponse;
    } else {
      final ErrorResponse errorResponse = ErrorResponse.fromJson(json);
      baseResponse = errorResponse;
    }
    return baseResponse;
  }

  Future<BaseResponse> getUserProfile({required String jwt}) async {
    http.Response response = await NetworkService().get(
        url: NetworkEndpoints.GET_USER_PROFILE_API,
        requestHeaders: {'Authorization': 'Bearer ' + jwt});
    String json = response.body;
    BaseResponse baseResponse;
    if (response.statusCode == 200) {
      final GetUserProfileResponse getUserProfileResponse =
          GetUserProfileResponse.fromJson(json);
      baseResponse = getUserProfileResponse;
    } else {
      try {
        final ErrorResponse errorResponse = ErrorResponse.fromJson2(json);
        baseResponse = errorResponse;
      } catch (e) {
        return ErrorResponse(
            statusCode: response.statusCode, error: '', messages: []);
      }
    }
    return baseResponse;
  }

  Future<BaseResponse> getPublicUserProfile(
      {required String jwt, required String id}) async {
    http.Response response = await NetworkService().get(
      url: NetworkEndpoints.USERS + '/$id',
      requestHeaders: {'Authorization': 'Bearer ' + jwt},
    );
    String json = response.body;
    BaseResponse baseResponse;
    if (response.statusCode == 200) {
      final GetUserProfileResponse getUserProfileResponse =
          GetUserProfileResponse.fromJson(json);
      baseResponse = getUserProfileResponse;
    } else {
      try {
        final ErrorResponse errorResponse = ErrorResponse.fromJson2(json);
        baseResponse = errorResponse;
      } catch (e) {
        return ErrorResponse(
            statusCode: response.statusCode, error: '', messages: []);
      }
    }
    return baseResponse;
  }

  Future<BaseResponse> getQuizzes() async {
    final QueryResult queryResult = await NetworkService().postGraphQL(
      url: '${NetworkEndpoints.API_URL}/graphql',
      query: """
  query {
    quizzes(limit: 5000, start: 0) {
      id
      type
      title
      category {
        name
        id
        order
        image {
        url
        }
      }
      image {
        url
      },
      users_permissions_user{
        id
      }
    }
  }
""",
    );
    BaseResponse baseResponse;
    if (queryResult.data != null) {
      final GetQuizzesResponse getQuizzesResponse =
          GetQuizzesResponse.fromMap(queryResult.data ?? Map());
      baseResponse = getQuizzesResponse;
    } else {
      baseResponse = GraphQLErrorResponse();
    }
    return baseResponse;
  }

  Future<BaseResponse> getQuiz({required String id}) async {
    final QueryResult queryResult = await NetworkService().postGraphQL(
      url: '${NetworkEndpoints.API_URL}/graphql',
      query: """
  query {
      quizzes(where:{id:$id}){
          id
          type
          title
          keyword
          extended_description
          description
          quiz_attempts{
            result
            questions_count
          }
          questions{
              id
              statement
              type

              point
              image {
                  url
              }
              choices{
                  id
                  text
                  type
                  is_correct
              }
          }
          category{
              name
              id
          }
          image{
              url
          }
          users_permissions_user{
            id,
            username,
            email
          }
          categorical_result{
              name
              image{
              url
              }
          }
      }
  }
""",
    );
    BaseResponse baseResponse;
    if (queryResult.data != null) {
      final GetQuizResponse getQuizResponse =
          GetQuizResponse.fromMap(queryResult.data ?? Map());
      baseResponse = getQuizResponse;
    } else {
      baseResponse = GraphQLErrorResponse();
    }
    return baseResponse;
  }

  Future<BaseResponse> getCategories() async {
    final QueryResult queryResult = await NetworkService().postGraphQL(
      url: '${NetworkEndpoints.API_URL}/graphql',
      query: """
  query {
    categories{
        id
        order
        name
        image{
            url
        }
    }
  }
""",
    );
    BaseResponse baseResponse;
    if (queryResult.data != null) {
      final GetCategoriesResponse getCategoriesResponse =
          GetCategoriesResponse.fromMap(queryResult.data ?? Map());
      baseResponse = getCategoriesResponse;
    } else {
      baseResponse = GraphQLErrorResponse();
    }
    return baseResponse;
  }

  Future<bool> addRating({required AddRatingRequest request}) async {
    http.Response response = await NetworkService()
        .post(url: NetworkEndpoints.ADD_RATING, body: request.toJson());
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<BaseResponse> getRating({required String quizId}) async {
    http.Response response = await NetworkService()
        .get(url: NetworkEndpoints.GET_RATING + '?quiz=$quizId');
    String json = response.body;
    BaseResponse baseResponse;
    if (response.statusCode == 200) {
      final GetRatingResponse getRatingResponse =
          GetRatingResponse.fromJson(json);
      baseResponse = getRatingResponse;
    } else {
      final ErrorResponse errorResponse = ErrorResponse.fromJson2(json);
      baseResponse = errorResponse;
    }
    return baseResponse;
  }

  Future<bool> postQuizAttempts({required QuizAttemptRequest request}) async {
    http.Response response = await NetworkService()
        .post(url: NetworkEndpoints.POST_QUIZ_ATTEMPTS, body: request.toJson());
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> addQuiz({required QuizModelExtended quiz}) async {
    http.Response response = await NetworkService()
        .post(url: NetworkEndpoints.ADD_QUIZ, body: quiz.toJson());
    if (response.statusCode == 200) {
      print(response.body);
      return true;
    } else {
      return false;
    }
  }

  Future<UserPermissionsUser?> updateProfile(
      {required String userId,
      required String jwt,
      required ProfileSettingsModel profileSettingsModel}) async {
    http.Response response = await NetworkService().put(
        url: NetworkEndpoints.USERS + '/$userId',
        body: profileSettingsModel.toValidJson(),
        requestHeaders: {'Authorization': 'Bearer ' + jwt});
    if (response.statusCode == 200) {
      return UserPermissionsUser.fromJson(response.body);
    }
  }

  Future<UploadFileResponse?> uploadFile({
    required UploadFileRequest uploadFileRequest,
    required String filename,
    required ImageFormat imageFormat,
    required String jwt,
  }) async {
    http.Response response = await NetworkService().uploadFile(
        uploadFileRequest: uploadFileRequest, filename: filename, jwt: jwt);
    if (response.statusCode == 200) {
      UploadFileResponse uploadFileResponse =
          UploadFileResponse.fromJson(response.body);
      return uploadFileResponse;
    }
  }

  Future<UploadFileResponse?> uploadFiles({
    required List<File> files,
    required String filename,
    required ImageFormat imageFormat,
    required String jwt,
  }) async {
    http.Response response = await NetworkService()
        .uploadFiles(files: files, filename: filename, jwt: jwt);
    if (response.statusCode == 200) {
      UploadFileResponse uploadFileResponse =
          UploadFileResponse.fromJson(response.body);
      return uploadFileResponse;
    }
  }

  Future<String?> addGoogleFormQuiz(
      {required GoogleFormRequest request}) async {
    http.Response response = await NetworkService()
        .post(url: NetworkEndpoints.GOOGLE_FORM_QUIZ, body: request.toJson());
    if (response.statusCode == 200) {
      return response.body.isNotEmpty
          ? ((json.decode(response.body)["id"]) as int).toString()
          : null;
    }
  }

  Future<bool> updateQuiz(
      {required String quizId,
      required UpdateQuizImageRequest updateQuizImageRequest}) async {
    http.Response response = await NetworkService().put(
      url: NetworkEndpoints.UPDATE_QUIZ + '/$quizId',
      body: updateQuizImageRequest.toJson(),
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  // Future<bool> uploadFile(
  //     {required UploadFileRequest uploadFileRequest}) async {
  //   http.Response response = await NetworkService().post(
  //     url: NetworkEndpoints.UPLOAD_FILE,
  //     body: uploadFileRequest.toJson(),
  //   );
  //   if (response.statusCode == 200) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
}
