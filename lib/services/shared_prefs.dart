import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static Future<bool> saveJwt(String jwt) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString('jwt', jwt);
  }

  static Future<String?> getJwt() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString('jwt');
  }

  static Future<bool> removeJwt() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.remove('jwt');
  }
}
