import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:saool/config/network_endpoints.dart';
import 'package:saool/models/requests/upload_file_request.dart';

class NetworkService {
  Future<http.Response> get(
      {required String url, Map<String, String>? requestHeaders}) async {
    try {
      Map<String, String> headers = new Map<String, String>();
      // headers['Host'] = 'api.saool.io';
      headers['Content-Type'] = 'application/json';
      if (requestHeaders != null) {
        headers.addAll(requestHeaders);
      }
      http.Response response = await http.get(
        Uri.parse(url),
        headers: headers,
      );
      return response;
    } catch (e) {
      print(e);
      return http.Response('', 995);
    }
  }

  Future<http.Response> post(
      {required String url,
      String? body,
      Map<String, String>? requestHeaders}) async {
    try {
      Map<String, String> headers = new Map<String, String>();
      // headers['Host'] = 'api.saool.io';
      headers['Content-Type'] = 'application/json';
      if (requestHeaders != null) {
        headers.addAll(requestHeaders);
      }
      http.Response response = await http.post(
        Uri.parse(url),
        body: body,
        headers: headers,
      );
      return response;
    } catch (e) {
      print(e);
      return http.Response('', 995);
    }
  }

  Future<http.Response> put(
      {required String url,
      String? body,
      Map<String, String>? requestHeaders}) async {
    try {
      Map<String, String> headers = new Map<String, String>();
      // headers['Host'] = 'api.saool.io';
      headers['Content-Type'] = 'application/json';
      if (requestHeaders != null) {
        headers.addAll(requestHeaders);
      }
      http.Response response = await http.put(
        Uri.parse(url),
        body: body,
        headers: headers,
      );
      return response;
    } catch (e) {
      print(e);
      return http.Response('', 995);
    }
  }

  Future<http.Response> uploadFile(
      {required UploadFileRequest uploadFileRequest,
      required String filename,
      required String jwt}) async {
    try {
      var request = http.MultipartRequest(
          "POST", Uri.parse(NetworkEndpoints.UPLOAD_FILE));
      // request.fields['field'] = uploadFileRequest.field;
      // request.fields['ref'] = uploadFileRequest.ref;
      // request.fields['refId'] = uploadFileRequest.refId;
      var image = await http.MultipartFile.fromPath(
        "files",
        uploadFileRequest.files.path,
        // filename: filename,
      );
      request.files.add(image);
      Map<String, String> headers = new Map<String, String>();
      // headers['Host'] = 'api.saool.io';
      headers['Content-Type'] = 'application/json';
      headers['Authorization'] = 'Bearer ' + jwt;
      request.headers.addAll(headers);
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      return response;
    } catch (e) {
      print(e);
      return http.Response('', 995);
    }
  }

  Future<http.Response> uploadFiles(
      {required List<File> files,
      required String filename,
      required String jwt}) async {
    try {
      var request = http.MultipartRequest(
          "POST", Uri.parse(NetworkEndpoints.UPLOAD_FILE));
      // request.fields['field'] = uploadFileRequest.field;
      // request.fields['ref'] = uploadFileRequest.ref;
      // request.fields['refId'] = uploadFileRequest.refId;
      for (var f in files) {
        var image = await http.MultipartFile.fromPath(
          "files",
          f.path,
          // filename: filename,
        );
        request.files.add(image);
      }
      Map<String, String> headers = new Map<String, String>();
      headers['Host'] = 'api.saool.io';
      headers['Content-Type'] = 'application/json';
      headers['Authorization'] = 'Bearer ' + jwt;
      request.headers.addAll(headers);
      var streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);
      return response;
    } catch (e) {
      print(e);
      return http.Response('', 995);
    }
  }

  // GraphQL
  Future<dynamic> postGraphQL(
      {required String url, required String query}) async {
    final HttpLink link = HttpLink(url);

    GraphQLClient client =
        GraphQLClient(link: link, cache: GraphQLCache(store: InMemoryStore()));

    QueryOptions queryOptions = QueryOptions(
      document: gql(query),
    );

    var result = await client.query(queryOptions);
    return result;
  }
}
