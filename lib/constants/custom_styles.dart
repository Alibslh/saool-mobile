import 'package:flutter/material.dart';

class CustomStyles {
  static const Color secondaryTextColor = Color(0xFF2AB7CA);
  static const Color tertiaryTextColor = Color(0xFFFE4A49);
  static const Color tabColor = Color(0xFFE6E6EA);
  static const Color tabBorderColor = Color(0xFFFED766);
  static const Color tabBorderColor2 = Color(0xFFF4F4F8);
  static const Color warningColor = Color(0xFFFE4A49);

  static BoxDecoration roundShadowBoxDecoration() {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(15),
      boxShadow: [
        BoxShadow(
          blurRadius: 34,
          offset: Offset(0, 7),
          color: Colors.black.withOpacity(0.16),
          spreadRadius: -7,
        ),
      ],
    );
  }

  static BoxDecoration roundSimpleBoxDecoration() {
    return BoxDecoration(
      color: Color(0xFFF4F4F8),
      borderRadius: BorderRadius.circular(15),
    );
  }

  static BoxDecoration roundBorderedShadowBoxDecoration({Color? contentColor}) {
    return BoxDecoration(
      color: contentColor,
      borderRadius: BorderRadius.circular(15),
      border: Border.all(
        width: 1,
        color: Color(0xFFEEEEEE),
      ),
      boxShadow: [
        BoxShadow(
          blurRadius: 25,
          offset: Offset(0, 7),
          color: Colors.black.withOpacity(0.06),
          spreadRadius: -5,
        ),
      ],
    );
  }

  static TextStyle regularText() => TextStyle(
        fontSize: 18,
      );

  static TextStyle regularSmallText() => TextStyle(
        fontSize: 14,
      );

  static TextStyle boldSmallText() => TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
      );

  static TextStyle boldHeadingText() => TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.bold,
      );

  static TextStyle extraBoldText() => TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.w800,
      );
}
