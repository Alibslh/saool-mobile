enum ImageFormat { LARGE, MEDIUM, SMALL, THUMBNAIL }

class FileUploadConstants {
  static const String profilePicture = 'profile_picture';
  static const String quizCreationPicture = 'quizCreationPicture';
}
