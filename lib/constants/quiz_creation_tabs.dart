import 'package:saool/views/create_quiz_step_one_page.dart';

class QuizCreationConstants {
  static List<QuizCreationTabs> quizCreationTabs = [
    QuizCreationTabs(id: '1', title: 'عن الاختبار', onPressed: () {}),
    QuizCreationTabs(id: '2', title: 'أسئلة الاختبار', onPressed: () {}),
    QuizCreationTabs(id: '3', title: 'النشر', onPressed: () {}),
  ];
}
