class QuizTypeConstants {
  static const String categoricalType = 'categorical';
  static const String numericalType = 'numerical';
}
