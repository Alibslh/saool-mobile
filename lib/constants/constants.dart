import 'package:flutter/material.dart';

enum ButtonState { idle, loading, success, error }

class Constants {
  static int _index = 0;

  static List<LinearGradient> linearGradients = [
    LinearGradient(
        begin: Alignment.bottomRight,
        end: Alignment.topLeft,
        colors: [Color(0xFFFE4A49), Color(0xFFFF7E7E)]),
    LinearGradient(
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        colors: [Color(0xFFFEAB66), Color(0xFFFED766)]),
    LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [Color(0xFFFED766), Color(0xFFFEF0C4)]),
    LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [Color(0xFF2AB7CA), Color(0xFFFED766)]),
    LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [Color(0xFFE6E6EA), Color(0xFFE6EFFF)]),
    LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [Color(0xFFFc4a970), Color(0xFFFED766)]),
    LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [Colors.green, Colors.lightGreenAccent]),
    LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [Colors.orange, Colors.orangeAccent]),
    LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [Colors.deepOrange, Colors.deepOrangeAccent]),
  ];

  static LinearGradient getLinearGradient() {
    if (_index >= linearGradients.length) {
      _index = 0;
    }
    return linearGradients[_index++];
  }

  static resetIndex() => _index = 0;

  static int imageLoadArtIndex = 0;

  static const List<String> imageLoadArtList = [
    'assets/image-load-art-1.png',
    'assets/image-load-art-2.png',
    'assets/image-load-art-3.png',
  ];

  static String getImageLoadArt() {
    if (imageLoadArtIndex >= imageLoadArtList.length) {
      imageLoadArtIndex = 0;
    }
    return imageLoadArtList[imageLoadArtIndex++];
  }
}
