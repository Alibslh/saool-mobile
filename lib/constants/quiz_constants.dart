class QuizConstants {
  static const int maxChoices = 6;
  static const int maxQuestions = 10;
  // Question Type
  static const String textTypeQuestion = 'text';
  static const String imageTypeQuestion = 'image';
}
