class RatingTypes {
  static List<RatingType> ratingTypes = [
    RatingType(value: 5, emoji: '😍', text: 'رهيب!', count: 122000),
    RatingType(value: 4, emoji: '😊', text: 'جميل', count: 123),
    RatingType(value: 3, emoji: '🙆', text: 'OK..', count: 21),
    RatingType(value: 2, emoji: '🌚', text: 'GG', count: 6),
    RatingType(value: 1, emoji: '💩', text: 'خاايس', count: 12),
  ];
}

class RatingType {
  final int value;
  final String emoji;
  final String text;
  final int count;
  RatingType({
    required this.value,
    required this.emoji,
    required this.text,
    required this.count,
  });
}
