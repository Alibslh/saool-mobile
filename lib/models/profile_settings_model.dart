import 'dart:convert';

import 'package:saool/utils/common_utils.dart';

import 'image_model.dart';

class ProfileSettingsModel {
  String? name;
  String? username;
  String? email;
  String? password;
  ImageModel? profileImage;
  String? twitter;
  String? snapchat;
  String? instagram;
  String? facebook;

  ProfileSettingsModel({
    this.name,
    this.username,
    this.email,
    this.password,
    this.profileImage,
    this.twitter,
    this.snapchat,
    this.instagram,
    this.facebook,
  });

  ProfileSettingsModel copyWith({
    String? name,
    String? username,
    String? email,
    String? password,
    ImageModel? profileImage,
    String? twitter,
    String? snapchat,
    String? instagram,
    String? facebook,
  }) {
    return ProfileSettingsModel(
      name: name ?? this.name,
      username: username ?? this.username,
      email: email ?? this.email,
      password: password ?? this.password,
      profileImage: profileImage ?? this.profileImage,
      twitter: twitter ?? this.twitter,
      snapchat: snapchat ?? this.snapchat,
      instagram: instagram ?? this.instagram,
      facebook: facebook ?? this.facebook,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'username': username,
      'email': email,
      'password': password,
      'profile_image': profileImage?.toMap(),
      'twitter': twitter,
      'snapchat': snapchat,
      'instagram': instagram,
      'facebook': facebook,
    };
  }

  Map<String, dynamic> toValidMap() {
    Map<String, dynamic> map = {};
    if (name != null && !CommonUtils.hasOnlyWhitespace(name!)) {
      map['name'] = name;
    }
    if (username != null && !CommonUtils.hasOnlyWhitespace(username!)) {
      map['username'] = username;
    }
    if (email != null && !CommonUtils.hasOnlyWhitespace(email!)) {
      map['email'] = email;
    }
    if (password != null && !CommonUtils.hasOnlyWhitespace(password!)) {
      map['password'] = password;
    }
    if (profileImage != null &&
        !CommonUtils.hasOnlyWhitespace(profileImage!.url)) {
      map['profile_image'] = {
        'id': profileImage!.id,
        'url': profileImage!.url,
      };
    }
    if (twitter != null && !CommonUtils.hasOnlyWhitespace(twitter!)) {
      map['twitter'] = twitter;
    }
    if (snapchat != null && !CommonUtils.hasOnlyWhitespace(snapchat!)) {
      map['snapchat'] = snapchat;
    }
    if (instagram != null && !CommonUtils.hasOnlyWhitespace(instagram!)) {
      map['instagram'] = instagram;
    }
    if (facebook != null && !CommonUtils.hasOnlyWhitespace(facebook!)) {
      map['facebook'] = facebook;
    }
    return map;
  }

  factory ProfileSettingsModel.fromMap(Map<String, dynamic> map) {
    return ProfileSettingsModel(
      name: map['name'],
      username: map['username'],
      email: map['email'],
      password: map['password'],
      profileImage: ImageModel.fromMap(map['profile_image']),
      twitter: map['twitter'],
      snapchat: map['snapchat'],
      instagram: map['instagram'],
      facebook: map['facebook'],
    );
  }

  String toJson() => json.encode(toMap());

  String toValidJson() => json.encode(toValidMap());

  factory ProfileSettingsModel.fromJson(String source) =>
      ProfileSettingsModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ProfileSettingsModel(name: $name, username: $username, email: $email, password: $password, profileImage: $profileImage, twitter: $twitter, snapchat: $snapchat, instagram: $instagram, facebook: $facebook)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProfileSettingsModel &&
        other.name == name &&
        other.username == username &&
        other.email == email &&
        other.password == password &&
        other.profileImage == profileImage &&
        other.twitter == twitter &&
        other.snapchat == snapchat &&
        other.instagram == instagram &&
        other.facebook == facebook;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        username.hashCode ^
        email.hashCode ^
        password.hashCode ^
        profileImage.hashCode ^
        twitter.hashCode ^
        snapchat.hashCode ^
        instagram.hashCode ^
        facebook.hashCode;
  }
}
