import 'dart:convert';

class QuizAttemptModel {
  final int? result;
  final int? questionsCount;

  QuizAttemptModel({
    this.result,
    this.questionsCount,
  });

  QuizAttemptModel copyWith({
    int? result,
    int? questionsCount,
  }) {
    return QuizAttemptModel(
      result: result ?? this.result,
      questionsCount: questionsCount ?? this.questionsCount,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'result': result,
      'questions_count': questionsCount,
    };
  }

  factory QuizAttemptModel.fromMap(Map<String, dynamic> map) {
    return QuizAttemptModel(
      result: map['result'],
      questionsCount: map['questions_count'],
    );
  }

  String toJson() => json.encode(toMap());

  factory QuizAttemptModel.fromJson(String source) =>
      QuizAttemptModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'QuizAttemptModel(result: $result, questionsCount: $questionsCount)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is QuizAttemptModel &&
        other.result == result &&
        other.questionsCount == questionsCount;
  }

  @override
  int get hashCode => result.hashCode ^ questionsCount.hashCode;
}
