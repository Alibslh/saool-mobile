class Category {
  final String id;
  final String title;
  final double? offset;

  Category({required this.id, required this.title, this.offset});
}
