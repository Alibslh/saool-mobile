import 'dart:convert';

class RatingModel {
  int? value;
  String? text;

  RatingModel({
    this.value = 5,
    this.text,
  });

  RatingModel copyWith({
    int? value,
    String? text,
  }) {
    return RatingModel(
      value: value ?? this.value,
      text: text ?? this.text,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'value': value,
      'text': text,
    };
  }

  factory RatingModel.fromMap(Map<String, dynamic> map) {
    return RatingModel(
      value: map['value'],
      text: map['text'],
    );
  }

  String toJson() => json.encode(toMap());

  factory RatingModel.fromJson(String source) =>
      RatingModel.fromMap(json.decode(source));

  @override
  String toString() => 'RatingModel(value: $value, text: $text)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RatingModel && other.value == value && other.text == text;
  }

  @override
  int get hashCode => value.hashCode ^ text.hashCode;
}
