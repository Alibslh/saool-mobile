import 'dart:convert';

import 'package:saool/models/image_model.dart';

class UpdateQuizImageRequest {
  final ImageModel image;

  UpdateQuizImageRequest({
    required this.image,
  });

  UpdateQuizImageRequest copyWith({
    ImageModel? image,
  }) {
    return UpdateQuizImageRequest(
      image: image ?? this.image,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'image': [
        image.toMap(),
      ],
    };
  }

  factory UpdateQuizImageRequest.fromMap(Map<String, dynamic> map) {
    return UpdateQuizImageRequest(
      image: ImageModel.fromMap(map['image']),
    );
  }

  String toJson() => json.encode(toMap());

  factory UpdateQuizImageRequest.fromJson(String source) =>
      UpdateQuizImageRequest.fromMap(json.decode(source));

  @override
  String toString() => 'UpdateQuizImageRequest(image: $image)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UpdateQuizImageRequest && other.image == image;
  }

  @override
  int get hashCode => image.hashCode;
}
