import 'dart:convert';

class QuizAttemptRequest {
  final int result;
  final String quiz;
  final int questionsCount;
  final String sessionId;
  final String? usersPermissionsUser;
  final String? platform;

  QuizAttemptRequest({
    required this.result,
    required this.quiz,
    required this.questionsCount,
    required this.sessionId,
    required this.usersPermissionsUser,
    required this.platform,
  });

  QuizAttemptRequest copyWith({
    int? result,
    String? quiz,
    int? questionsCount,
    String? sessionId,
    String? usersPermissionsUser,
    String? platform,
  }) {
    return QuizAttemptRequest(
        result: result ?? this.result,
        quiz: quiz ?? this.quiz,
        questionsCount: questionsCount ?? this.questionsCount,
        sessionId: sessionId ?? this.sessionId,
        usersPermissionsUser: usersPermissionsUser ?? this.usersPermissionsUser,
        platform: "mobile");
  }

  Map<String, dynamic> toMap() {
    return {
      'result': result,
      'quiz': quiz,
      'questions_count': questionsCount,
      'session_id': sessionId,
      'users_permissions_user': usersPermissionsUser,
      'platform': platform,
    };
  }

  factory QuizAttemptRequest.fromMap(Map<String, dynamic> map) {
    return QuizAttemptRequest(
        result: map['result'],
        quiz: map['quiz'],
        questionsCount: map['questions_count'],
        sessionId: map['session_id'],
        usersPermissionsUser: map['users_permissions_user'],
        platform: map['platform']);
  }

  String toJson() => json.encode(toMap());

  factory QuizAttemptRequest.fromJson(String source) =>
      QuizAttemptRequest.fromMap(json.decode(source));

  @override
  String toString() {
    return 'QuizAttemptRequest(result: $result, quiz: $quiz, questionsCount: $questionsCount, sessionId: $sessionId, usersPermissionsUser: $usersPermissionsUser)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is QuizAttemptRequest &&
        other.result == result &&
        other.quiz == quiz &&
        other.questionsCount == questionsCount &&
        other.sessionId == sessionId &&
        other.usersPermissionsUser == usersPermissionsUser &&
        other.platform == platform;
  }

  @override
  int get hashCode {
    return result.hashCode ^
        quiz.hashCode ^
        questionsCount.hashCode ^
        sessionId.hashCode ^
        usersPermissionsUser.hashCode ^
        platform.hashCode;
  }
}
