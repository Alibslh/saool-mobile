import 'dart:convert';

class SignUpRequest {
  final String username;
  final String email;
  final String password;

  SignUpRequest({
    required this.username,
    required this.email,
    required this.password,
  });

  SignUpRequest copyWith({
    String? username,
    String? email,
    String? password,
  }) {
    return SignUpRequest(
      username: username ?? this.username,
      email: email ?? this.email,
      password: password ?? this.password,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'username': username,
      'email': email,
      'password': password,
    };
  }

  factory SignUpRequest.fromMap(Map<String, dynamic> map) {
    return SignUpRequest(
      username: map['username'],
      email: map['email'],
      password: map['password'],
    );
  }

  String toJson() => json.encode(toMap());

  factory SignUpRequest.fromJson(String source) =>
      SignUpRequest.fromMap(json.decode(source));

  @override
  String toString() =>
      'SignUpRequest(username: $username, email: $email, password: $password)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SignUpRequest &&
        other.username == username &&
        other.email == email &&
        other.password == password;
  }

  @override
  int get hashCode => username.hashCode ^ email.hashCode ^ password.hashCode;
}
