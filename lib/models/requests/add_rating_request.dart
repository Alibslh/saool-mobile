import 'dart:convert';

class AddRatingRequest {
  final int value;
  final String quizId;
  final String sessionId;
  final String? text;

  AddRatingRequest({
    required this.value,
    required this.quizId,
    required this.sessionId,
    this.text,
  });

  AddRatingRequest copyWith({
    int? value,
    String? quizId,
    String? sessionId,
    String? text,
  }) {
    return AddRatingRequest(
      value: value ?? this.value,
      quizId: quizId ?? this.quizId,
      sessionId: sessionId ?? this.sessionId,
      text: text ?? this.text,
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map<String, dynamic>();
    map['value'] = value;
    map['quiz'] = {"id": quizId};
    map['session_id'] = sessionId;
    if (text != null) map['text'] = text;
    return map;
  }

  factory AddRatingRequest.fromMap(Map<String, dynamic> map) {
    return AddRatingRequest(
      value: map['value'],
      quizId: map['quiz']["id"],
      sessionId: map['session_id'],
      text: map['text'],
    );
  }

  String toJson() => json.encode(toMap());

  factory AddRatingRequest.fromJson(String source) =>
      AddRatingRequest.fromMap(json.decode(source));

  @override
  String toString() {
    return 'AddRatingRequest(value: $value, quizId: $quizId, sessionId: $sessionId, text: $text)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AddRatingRequest &&
        other.value == value &&
        other.quizId == quizId &&
        other.sessionId == sessionId &&
        other.text == text;
  }

  @override
  int get hashCode {
    return value.hashCode ^
        quizId.hashCode ^
        sessionId.hashCode ^
        text.hashCode;
  }
}
