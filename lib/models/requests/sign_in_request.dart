import 'dart:convert';

class SignInRequest {
  final String identifier;
  final String password;
  SignInRequest({
    required this.identifier,
    required this.password,
  });

  SignInRequest copyWith({
    String? identifier,
    String? password,
  }) {
    return SignInRequest(
      identifier: identifier ?? this.identifier,
      password: password ?? this.password,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'identifier': identifier,
      'password': password,
    };
  }

  factory SignInRequest.fromMap(Map<String, dynamic> map) {
    return SignInRequest(
      identifier: map['identifier'],
      password: map['password'],
    );
  }

  String toJson() => json.encode(toMap());

  factory SignInRequest.fromJson(String source) =>
      SignInRequest.fromMap(json.decode(source));

  @override
  String toString() =>
      'SignInRequest(identifier: $identifier, password: $password)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SignInRequest &&
        other.identifier == identifier &&
        other.password == password;
  }

  @override
  int get hashCode => identifier.hashCode ^ password.hashCode;
}
