import 'dart:convert';

class GoogleFormRequest {
  final String link;
  final String category;
  final String usersPermissionsUser;

  GoogleFormRequest({
    required this.link,
    required this.category,
    required this.usersPermissionsUser,
  });

  GoogleFormRequest copyWith({
    String? link,
    String? category,
    String? usersPermissionsUser,
  }) {
    return GoogleFormRequest(
      link: link ?? this.link,
      category: category ?? this.category,
      usersPermissionsUser: usersPermissionsUser ?? this.usersPermissionsUser,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'link': link,
      'category': category,
      'users_permissions_user': usersPermissionsUser,
    };
  }

  factory GoogleFormRequest.fromMap(Map<String, dynamic> map) {
    return GoogleFormRequest(
      link: map['link'],
      category: map['category'],
      usersPermissionsUser: map['users_permissions_user'],
    );
  }

  String toJson() => json.encode(toMap());

  factory GoogleFormRequest.fromJson(String source) =>
      GoogleFormRequest.fromMap(json.decode(source));

  @override
  String toString() =>
      'GoogleFormRequest(link: $link, category: $category, usersPermissionsUser: $usersPermissionsUser)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GoogleFormRequest &&
        other.link == link &&
        other.category == category &&
        other.usersPermissionsUser == usersPermissionsUser;
  }

  @override
  int get hashCode =>
      link.hashCode ^ category.hashCode ^ usersPermissionsUser.hashCode;
}
