import 'dart:convert';
import 'dart:io';

class UploadFileRequest {
  final File files;
  final String refId;
  final String ref;
  final String field;

  UploadFileRequest({
    required this.files,
    required this.refId,
    required this.ref,
    required this.field,
  });

  UploadFileRequest copyWith({
    File? files,
    String? refId,
    String? ref,
    String? field,
  }) {
    return UploadFileRequest(
      files: files ?? this.files,
      refId: refId ?? this.refId,
      ref: ref ?? this.ref,
      field: field ?? this.field,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'files': files,
      'refId': refId,
      'ref': ref,
      'field': field,
    };
  }

  factory UploadFileRequest.fromMap(Map<String, dynamic> map) {
    return UploadFileRequest(
      files: map['files'],
      refId: map['refId'],
      ref: map['ref'],
      field: map['field'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UploadFileRequest.fromJson(String source) =>
      UploadFileRequest.fromMap(json.decode(source));

  @override
  String toString() {
    return 'UploadFileRequest(files: $files, refId: $refId, ref: $ref, field: $field)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UploadFileRequest &&
        other.files == files &&
        other.refId == refId &&
        other.ref == ref &&
        other.field == field;
  }

  @override
  int get hashCode {
    return files.hashCode ^ refId.hashCode ^ ref.hashCode ^ field.hashCode;
  }
}
