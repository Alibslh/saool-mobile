import 'dart:convert';

class QuizCreationModel {
  String title;
  String description;

  QuizCreationModel({
    required this.title,
    required this.description,
  });

  QuizCreationModel copyWith({
    String? title,
    String? description,
  }) {
    return QuizCreationModel(
      title: title ?? this.title,
      description: description ?? this.description,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'description': description,
    };
  }

  factory QuizCreationModel.fromMap(Map<String, dynamic> map) {
    return QuizCreationModel(
      title: map['title'],
      description: map['description'],
    );
  }

  String toJson() => json.encode(toMap());

  factory QuizCreationModel.fromJson(String source) =>
      QuizCreationModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'QuizCreationModel(title: $title, description: $description)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is QuizCreationModel &&
        other.title == title &&
        other.description == description;
  }

  @override
  int get hashCode => title.hashCode ^ description.hashCode;
}
