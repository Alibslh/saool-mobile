import 'dart:convert';
import 'dart:io';

import 'package:saool/models/category_model.dart';

class GoogleFormModel {
  String googleFormLink;
  String imageName;
  File? image;
  CategoryModel? category;

  GoogleFormModel({
    required this.googleFormLink,
    required this.imageName,
    this.image,
    this.category,
  });

  GoogleFormModel copyWith({
    String? googleFormLink,
    String? imageName,
    File? image,
    CategoryModel? category,
  }) {
    return GoogleFormModel(
      googleFormLink: googleFormLink ?? this.googleFormLink,
      imageName: imageName ?? this.imageName,
      image: image ?? this.image,
      category: category ?? this.category,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'googleFormLink': googleFormLink,
      'imageName': imageName,
      'image': image,
      'category': category?.toMap(),
    };
  }

  factory GoogleFormModel.fromMap(Map<String, dynamic> map) {
    return GoogleFormModel(
      googleFormLink: map['googleFormLink'],
      imageName: map['imageName'],
      image: map['image'],
      category: CategoryModel.fromMap(map['category']),
    );
  }

  String toJson() => json.encode(toMap());

  factory GoogleFormModel.fromJson(String source) =>
      GoogleFormModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'GoogleFormModel(googleFormLink: $googleFormLink, imageName: $imageName, image: $image, category: $category)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GoogleFormModel &&
        other.googleFormLink == googleFormLink &&
        other.imageName == imageName &&
        other.image == image &&
        other.category == category;
  }

  @override
  int get hashCode {
    return googleFormLink.hashCode ^
        imageName.hashCode ^
        image.hashCode ^
        category.hashCode;
  }
}
