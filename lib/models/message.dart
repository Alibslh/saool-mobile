import 'dart:convert';

class Message {
  final String id;
  final String message;
  Message({
    required this.id,
    required this.message,
  });

  Message copyWith({
    String? id,
    String? message,
  }) {
    return Message(
      id: id ?? this.id,
      message: message ?? this.message,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'message': message,
    };
  }

  factory Message.fromMap(Map<String, dynamic> map) {
    return Message(
      id: map['id'],
      message: map['message'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Message.fromJson(String source) =>
      Message.fromMap(json.decode(source));

  @override
  String toString() => 'Message(id: $id, message: $message)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Message && other.id == id && other.message == message;
  }

  @override
  int get hashCode => id.hashCode ^ message.hashCode;
}
