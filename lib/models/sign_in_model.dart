import 'dart:convert';

class SignInModel {
  String identifier;
  String password;

  SignInModel({
    required this.identifier,
    required this.password,
  });

  SignInModel copyWith({
    String? identifier,
    String? password,
  }) {
    return SignInModel(
      identifier: identifier ?? this.identifier,
      password: password ?? this.password,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'identifier': identifier,
      'password': password,
    };
  }

  factory SignInModel.fromMap(Map<String, dynamic> map) {
    return SignInModel(
      identifier: map['identifier'],
      password: map['password'],
    );
  }

  String toJson() => json.encode(toMap());

  factory SignInModel.fromJson(String source) =>
      SignInModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'SignInModel(identifier: $identifier, password: $password)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SignInModel &&
        other.identifier == identifier &&
        other.password == password;
  }

  @override
  int get hashCode => identifier.hashCode ^ password.hashCode;
}
