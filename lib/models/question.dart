import 'answer.dart';

class Question {
  final String id;
  final String quizId;
  final String question;
  final List<Answer> answers;
  final int questionNo;
  final String correctAnswerIndex;
  final String? imageUrl;

  Question({
    required this.id,
    required this.quizId,
    required this.question,
    required this.answers,
    required this.questionNo,
    required this.correctAnswerIndex,
    required this.imageUrl,
  });
}
