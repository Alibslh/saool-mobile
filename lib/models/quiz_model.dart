import 'dart:convert';

import 'package:saool/constants/constants.dart';
import 'package:saool/models/category_model.dart';

class QuizModel {
  final String id;
  final String type;
  final String title;
  final String imageUrl;
  final CategoryModel category;
  final String? userId;
  String loadingAssetPath;

  QuizModel({
    required this.id,
    required this.type,
    required this.title,
    required this.imageUrl,
    required this.category,
    this.userId,
    this.loadingAssetPath = '',
  }) {
    loadingAssetPath = Constants.getImageLoadArt();
  }

  QuizModel copyWith({
    String? id,
    String? type,
    String? title,
    String? imageUrl,
    CategoryModel? category,
    String? userId,
  }) {
    return QuizModel(
      id: id ?? this.id,
      type: type ?? this.type,
      title: title ?? this.title,
      imageUrl: imageUrl ?? this.imageUrl,
      category: category ?? this.category,
      userId: userId ?? this.userId,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'type': type,
      'title': title,
      'imageUrl': imageUrl,
      'categoryModel': category.toMap(),
      'users_permissions_user': {'id': userId},
    };
  }

  factory QuizModel.fromMap(Map<String, dynamic> map) {
    return QuizModel(
      id: map['id'].toString(),
      type: map['type'],
      title: map['title'],
      imageUrl: map['image']?['url'] ?? '',
      category: CategoryModel.fromMap(map['category']),
      userId: map['users_permissions_user']?['id'],
    );
  }

  String toJson() => json.encode(toMap());

  factory QuizModel.fromJson(String source) =>
      QuizModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'QuizModel(id: $id, type: $type, title: $title, imageUrl: $imageUrl, category: $category, userId: $userId)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is QuizModel &&
        other.id == id &&
        other.type == type &&
        other.title == title &&
        other.imageUrl == imageUrl &&
        other.category == category &&
        other.userId == userId;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        type.hashCode ^
        title.hashCode ^
        imageUrl.hashCode ^
        category.hashCode ^
        userId.hashCode;
  }
}
