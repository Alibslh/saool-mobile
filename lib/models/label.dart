import 'package:flutter/material.dart';

class Label {
  final String title;
  final Color backgroundColor;
  final Color textColor;

  Label({
    required this.title,
    required this.backgroundColor,
    required this.textColor,
  });
}
