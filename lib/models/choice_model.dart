import 'dart:convert';

import 'package:saool/models/image_model.dart';

class ChoiceModel {
  final String id;
  final String text;
  final String? type;
  final bool isCorrect;
  final ImageModel? image;

  ChoiceModel({
    required this.id,
    required this.text,
    required this.type,
    required this.isCorrect,
    this.image,
  });

  ChoiceModel copyWith({
    String? id,
    String? text,
    String? type,
    bool? isCorrect,
    ImageModel? image,
  }) {
    return ChoiceModel(
      id: id ?? this.id,
      text: text ?? this.text,
      type: type ?? this.type,
      isCorrect: isCorrect ?? this.isCorrect,
      image: image ?? this.image,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'text': text,
      'type': type,
      'is_correct': isCorrect,
      'image': image?.toMap(),
    };
  }

  factory ChoiceModel.invalid() {
    return ChoiceModel(
      id: '-1',
      text: '',
      type: '',
      isCorrect: false,
      image: null,
    );
  }
  factory ChoiceModel.fromMap(Map<String, dynamic> map) {
    return ChoiceModel(
      id: map['id'],
      text: map['text'],
      type: map['type'],
      isCorrect: map['is_correct'] ?? false,
      image: map['image'] != null
          ? ((map['image'] as List).isNotEmpty
              ? (ImageModel.fromMap(map['image'][0]))
              : null)
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ChoiceModel.fromJson(String source) =>
      ChoiceModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ChoiceModel(id: $id, text: $text, type: $type, isCorrect: $isCorrect, image: $image)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ChoiceModel &&
        other.id == id &&
        other.text == text &&
        other.type == type &&
        other.isCorrect == isCorrect &&
        other.image == image;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        text.hashCode ^
        type.hashCode ^
        isCorrect.hashCode ^
        image.hashCode;
  }
}
