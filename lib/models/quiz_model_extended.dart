import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:saool/constants/quiz_constants.dart';
import 'package:saool/constants/quiz_type_constants.dart';
import 'package:saool/models/category_model.dart';
import 'package:saool/models/choice_model.dart';
import 'package:saool/models/question_model.dart';
import 'package:saool/models/quiz_creation_model.dart';
import 'package:saool/models/user_permissions_user.dart';

import 'categorical_result.dart';

class QuizModelExtended {
  final String id;
  final String type;
  final String title;
  final String keyword;
  final String description;
  final String extendedDescription;
  final int quizAttemptsCount;
  final List<QuestionModel> questions;
  final CategoryModel category;
  final String imageUrl;
  final List<CategoricalResult>? categoricalResult;
  final UserPermissionsUser? userPermissionsUser;

  QuizModelExtended({
    required this.id,
    required this.type,
    required this.title,
    required this.keyword,
    required this.description,
    required this.extendedDescription,
    required this.quizAttemptsCount,
    required this.questions,
    required this.category,
    required this.imageUrl,
    this.userPermissionsUser,
    this.categoricalResult,
  });

  factory QuizModelExtended.fromQuizCreationModel(
          QuizCreationModel quizCreationModel, bool isCategorical) =>
      QuizModelExtended(
        id: '1',
        type: isCategorical
            ? QuizTypeConstants.categoricalType
            : QuizTypeConstants.numericalType,
        title: quizCreationModel.title,
        keyword: '',
        description: '',
        extendedDescription: quizCreationModel.description,
        quizAttemptsCount: 0,
        questions: [
          QuestionModel(
            id: '1',
            statement: '',
            // TODO: Update for image type
            type: QuizConstants.textTypeQuestion,
            point: 10,
            choices: [
              ChoiceModel(
                id: '1',
                text: '',
                type: '',
                isCorrect: false,
              ),
            ],
          ),
        ],
        category: CategoryModel(
          id: '1',
          name: '',
          order: 0,
          imageUrl: '',
        ),
        imageUrl: '',
      );

  factory QuizModelExtended.dummy(bool isCategorical) => QuizModelExtended(
        id: '1',
        type: isCategorical
            ? QuizTypeConstants.categoricalType
            : QuizTypeConstants.numericalType,
        title: '',
        keyword: '',
        description: '',
        extendedDescription: '',
        quizAttemptsCount: 0,
        questions: [
          QuestionModel(
            id: '1',
            statement: '',
            // TODO: Update for image type
            type: QuizConstants.textTypeQuestion,
            point: 10,
            choices: [
              ChoiceModel(
                id: '1',
                text: '',
                type: '',
                isCorrect: false,
              ),
            ],
          ),
        ],
        category: CategoryModel(
          id: '1',
          name: '',
          order: 0,
          imageUrl: '',
        ),
        imageUrl: '',
      );

  QuizModelExtended copyWith({
    String? id,
    String? type,
    String? title,
    String? keyword,
    String? description,
    String? extendedDescription,
    int? quizAttemptsCount,
    List<QuestionModel>? questions,
    CategoryModel? category,
    String? imageUrl,
    List<CategoricalResult>? categoricalResult,
    UserPermissionsUser? userPermissionsUser,
  }) {
    return QuizModelExtended(
      id: id ?? this.id,
      type: type ?? this.type,
      title: title ?? this.title,
      keyword: keyword ?? this.keyword,
      description: description ?? this.description,
      extendedDescription: extendedDescription ?? this.extendedDescription,
      quizAttemptsCount: quizAttemptsCount ?? this.quizAttemptsCount,
      questions: questions ?? this.questions,
      category: category ?? this.category,
      imageUrl: imageUrl ?? this.imageUrl,
      categoricalResult: categoricalResult ?? this.categoricalResult,
      userPermissionsUser: userPermissionsUser ?? this.userPermissionsUser,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'type': type,
      'title': title,
      'keyword': keyword,
      'description': description,
      'extended_description': extendedDescription,
      'quizAttemptsCount': quizAttemptsCount,
      'questions': questions.map((x) => x.toMap()).toList(),
      'category': category.toMap(),
      'imageUrl': imageUrl,
      'users_permissions_user': userPermissionsUser?.toMap(),
      'categorical_result': categoricalResult?.map((x) => x.toMap()).toList(),
    };
  }

  factory QuizModelExtended.fromMap(Map<String, dynamic> map) {
    return QuizModelExtended(
      id: map['id'],
      type: map['type'],
      title: map['title'],
      keyword: map['keyword'],
      extendedDescription: map['extended_description'] ?? '',
      description: map['description'],
      quizAttemptsCount: (map['quiz_attempts'] as List).isNotEmpty
          ? (map['quiz_attempts']?[0]?['result'])
          : 0,
      questions: List<QuestionModel>.from(
          map['questions']?.map((x) => QuestionModel.fromMap(x))),
      category: CategoryModel.fromMap(map['category']),
      imageUrl: map['image']?['url'] ?? '',
      userPermissionsUser: map['users_permissions_user'] != null
          ? UserPermissionsUser.fromMap(map['users_permissions_user'])
          : null,
      categoricalResult: map['categorical_result'] == null
          ? null
          : List<CategoricalResult>.from(map['categorical_result']
              ?.map((x) => CategoricalResult.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory QuizModelExtended.fromJson(String source) =>
      QuizModelExtended.fromMap(json.decode(source));

  @override
  String toString() {
    return 'QuizModelExtended(id: $id, type: $type, title: $title, keyword: $keyword, description: $description, extendedDescription: $extendedDescription, quizAttemptsCount: $quizAttemptsCount, questions: $questions, category: $category, imageUrl: $imageUrl, categoricalResult: $categoricalResult, userPermissionsUser: $userPermissionsUser)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is QuizModelExtended &&
        other.id == id &&
        other.type == type &&
        other.title == title &&
        other.keyword == keyword &&
        other.description == description &&
        other.extendedDescription == extendedDescription &&
        other.quizAttemptsCount == quizAttemptsCount &&
        listEquals(other.questions, questions) &&
        other.category == category &&
        other.imageUrl == imageUrl &&
        listEquals(other.categoricalResult, categoricalResult) &&
        other.userPermissionsUser == userPermissionsUser;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        type.hashCode ^
        title.hashCode ^
        keyword.hashCode ^
        description.hashCode ^
        extendedDescription.hashCode ^
        quizAttemptsCount.hashCode ^
        questions.hashCode ^
        category.hashCode ^
        imageUrl.hashCode ^
        userPermissionsUser.hashCode;
  }
}
