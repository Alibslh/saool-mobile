import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:saool/models/image_model.dart';

import 'choice_model.dart';

class QuestionModel {
  final String id;
  final String statement;
  final String? type;
  final double? point;
  final ImageModel? image;
  final List<ChoiceModel> choices;

  QuestionModel({
    required this.id,
    required this.statement,
    required this.type,
    required this.point,
    this.image,
    required this.choices,
  });

  QuestionModel copyWith({
    String? id,
    String? statement,
    String? type,
    double? point,
    ImageModel? image,
    List<ChoiceModel>? choices,
  }) {
    return QuestionModel(
      id: id ?? this.id,
      statement: statement ?? this.statement,
      type: type ?? this.type,
      point: point ?? this.point,
      image: image ?? this.image,
      choices: choices ?? this.choices,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'statement': statement,
      'type': type,
      'point': point,
      'image': image?.toMap(),
      'choices': choices.map((x) => x.toMap()).toList(),
    };
  }

  factory QuestionModel.fromMap(Map<String, dynamic> map) {
    // INFO: The choices are being shuffled before assigning them to the question
    List<ChoiceModel> newChoices = List<ChoiceModel>.from(
        map['choices']?.map((x) => ChoiceModel.fromMap(x)));
    newChoices.shuffle();
    return QuestionModel(
      id: map['id'],
      statement: map['statement'],
      type: map['type'],
      point: map['point'] != null ? ((map['point'] as int).toDouble()) : null,

      image: (map['image'] as List).isNotEmpty
          ? (ImageModel.fromMap(map['image'][0]))
          : null,
      // image:
      //     (map['image'] as List).isNotEmpty ? (map['image']?[0]['url']) : null,
      choices: newChoices,
    );
  }

  String toJson() => json.encode(toMap());

  factory QuestionModel.fromJson(String source) =>
      QuestionModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'QuestionModel(id: $id, statement: $statement, type: $type, point: $point, image: $image, choices: $choices)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is QuestionModel &&
        other.id == id &&
        other.statement == statement &&
        other.type == type &&
        other.point == point &&
        other.image == image &&
        listEquals(other.choices, choices);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        statement.hashCode ^
        type.hashCode ^
        point.hashCode ^
        image.hashCode ^
        choices.hashCode;
  }
}
