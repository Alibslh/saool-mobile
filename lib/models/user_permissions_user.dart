import 'dart:convert';

class UserPermissionsUser {
  final String id;
  final String username;
  final String email;
  final String? instagram;
  final String? twitter;
  final String? snapchat;
  final String? facebook;
  final String? name;
  final String? profileImageUrl;

  UserPermissionsUser({
    required this.id,
    required this.username,
    required this.email,
    this.instagram,
    this.twitter,
    this.snapchat,
    this.facebook,
    this.name,
    this.profileImageUrl,
  });

  UserPermissionsUser copyWith({
    String? id,
    String? username,
    String? email,
    String? instagram,
    String? twitter,
    String? snapchat,
    String? facebook,
    String? name,
    String? profileImageUrl,
  }) {
    return UserPermissionsUser(
      id: id ?? this.id,
      username: username ?? this.username,
      email: email ?? this.email,
      instagram: instagram ?? this.instagram,
      twitter: twitter ?? this.twitter,
      snapchat: snapchat ?? this.snapchat,
      facebook: facebook ?? this.facebook,
      name: name ?? this.name,
      profileImageUrl: profileImageUrl ?? this.profileImageUrl,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'username': username,
      'email': email,
      'instagram': instagram,
      'twitter': twitter,
      'snapchat': snapchat,
      'facebook': facebook,
      'name': name,
      'profileImageUrl': profileImageUrl,
    };
  }

  factory UserPermissionsUser.fromMap(Map<String, dynamic> map) {
    return UserPermissionsUser(
      id: map['id'].toString(),
      username: map['username'],
      email: map['email'],
      instagram: map['instagram'],
      twitter: map['twitter'],
      snapchat: map['snapchat'],
      facebook: map['facebook'],
      name: map['name'],
      profileImageUrl: map['profile_image']?['url'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UserPermissionsUser.fromJson(String source) =>
      UserPermissionsUser.fromMap(json.decode(source));

  @override
  String toString() {
    return 'UserPermissionsUser(id: $id, username: $username, email: $email, instagram: $instagram, twitter: $twitter, snapchat: $snapchat, facebook: $facebook, name: $name, profileImageUrl: $profileImageUrl)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UserPermissionsUser &&
        other.id == id &&
        other.username == username &&
        other.email == email &&
        other.instagram == instagram &&
        other.twitter == twitter &&
        other.snapchat == snapchat &&
        other.facebook == facebook &&
        other.name == name &&
        other.profileImageUrl == profileImageUrl;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        username.hashCode ^
        email.hashCode ^
        instagram.hashCode ^
        twitter.hashCode ^
        snapchat.hashCode ^
        facebook.hashCode ^
        name.hashCode ^
        profileImageUrl.hashCode;
  }
}
