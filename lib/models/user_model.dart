import 'dart:convert';

import 'package:saool/models/user_permissions_user.dart';

class UserModel {
  final String id;
  final String username;
  final String email;
  final String jwt;
  final UserPermissionsUser? userPermissionsUser;

  UserModel({
    required this.id,
    required this.username,
    required this.email,
    required this.jwt,
    this.userPermissionsUser,
  });

  UserModel copyWith({
    String? id,
    String? username,
    String? email,
    String? jwt,
    UserPermissionsUser? userPermissionsUser,
  }) {
    return UserModel(
      id: id ?? this.id,
      username: username ?? this.username,
      email: email ?? this.email,
      jwt: jwt ?? this.jwt,
      userPermissionsUser: userPermissionsUser ?? this.userPermissionsUser,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'username': username,
      'email': email,
      'jwt': jwt,
      'userPermissionsUser': userPermissionsUser?.toMap(),
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      id: map['id'],
      username: map['username'],
      email: map['email'],
      jwt: map['jwt'],
      userPermissionsUser: (map['userPermissionsUser'] != null)
          ? UserPermissionsUser.fromMap(map['userPermissionsUser'])
          : null,
    );
  }

  UserPermissionsUser toUserPermissionsUser() =>
      UserPermissionsUser(id: id, username: username, email: email);

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'UserModel(id: $id, username: $username, email: $email, jwt: $jwt, userPermissionsUser: $userPermissionsUser)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UserModel &&
        other.id == id &&
        other.username == username &&
        other.email == email &&
        other.jwt == jwt &&
        other.userPermissionsUser == userPermissionsUser;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        username.hashCode ^
        email.hashCode ^
        jwt.hashCode ^
        userPermissionsUser.hashCode;
  }

  factory UserModel.fromResponse(Map<String, dynamic> map) => UserModel(
        jwt: map['jwt'],
        id: map['user']['id'].toString(),
        username: map['user']['username'],
        email: map['user']['email'],
        userPermissionsUser: UserPermissionsUser(
          id: map['user']['id'].toString(),
          username: map['user']['username'],
          email: map['user']['email'],
        ),
      );

  factory UserModel.fromGetUserProfileResponse(Map<String, dynamic> map) =>
      UserModel(
        jwt: '',
        id: map['id'].toString(),
        username: map['username'],
        email: map['email'],
        userPermissionsUser: UserPermissionsUser(
          id: map['id'].toString(),
          username: map['username'],
          email: map['email'],
          instagram: map['instagram'],
          twitter: map['twitter'],
          snapchat: map['snapchat'],
          facebook: map['facebook'],
          name: map['name'],
          profileImageUrl: map['profile_image']?['url'],
        ),
      );
}
