import 'dart:convert';

import 'package:saool/models/user_model.dart';

import 'base_response.dart';

class SignInResponse extends BaseResponse {
  final UserModel userModel;

  SignInResponse({
    required this.userModel,
  });

  SignInResponse copyWith({
    UserModel? userModel,
  }) {
    return SignInResponse(
      userModel: userModel ?? this.userModel,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'userModel': userModel.toMap(),
    };
  }

  factory SignInResponse.fromMap(Map<String, dynamic> map) {
    return SignInResponse(
      userModel: UserModel.fromResponse(map),
    );
  }

  String toJson() => json.encode(toMap());

  factory SignInResponse.fromJson(String source) =>
      SignInResponse.fromMap(json.decode(source));

  @override
  String toString() => 'SignInResponse(userModel: $userModel)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SignInResponse && other.userModel == userModel;
  }

  @override
  int get hashCode => userModel.hashCode;
}
