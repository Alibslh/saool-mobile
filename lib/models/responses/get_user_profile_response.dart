import 'dart:convert';

import 'package:saool/models/user_model.dart';

import 'base_response.dart';

class GetUserProfileResponse extends BaseResponse {
  final UserModel userModel;

  GetUserProfileResponse({
    required this.userModel,
  });

  GetUserProfileResponse copyWith({
    UserModel? userModel,
  }) {
    return GetUserProfileResponse(
      userModel: userModel ?? this.userModel,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'userModel': userModel.toMap(),
    };
  }

  factory GetUserProfileResponse.fromMap(Map<String, dynamic> map) {
    return GetUserProfileResponse(
      userModel: UserModel.fromGetUserProfileResponse(map),
    );
  }

  String toJson() => json.encode(toMap());

  factory GetUserProfileResponse.fromJson(String source) =>
      GetUserProfileResponse.fromMap(json.decode(source));

  @override
  String toString() => 'GetUserProfileResponse(userModel: $userModel)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetUserProfileResponse && other.userModel == userModel;
  }

  @override
  int get hashCode => userModel.hashCode;
}
