import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:saool/models/rating_model.dart';
import 'package:saool/models/responses/base_response.dart';

class GetRatingResponse extends BaseResponse {
  List<RatingModel> ratings;
  GetRatingResponse({
    required this.ratings,
  });

  GetRatingResponse copyWith({
    List<RatingModel>? ratings,
  }) {
    return GetRatingResponse(
      ratings: ratings ?? this.ratings,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'ratings': ratings.map((x) => x.toMap()).toList(),
    };
  }

  factory GetRatingResponse.fromMap(Map<String, dynamic> map) {
    return GetRatingResponse(
      ratings: List<RatingModel>.from(
          map['ratings']?.map((x) => RatingModel.fromMap(x))),
    );
  }
  factory GetRatingResponse.fromList(List<dynamic> list) {
    return GetRatingResponse(
      ratings: List<RatingModel>.from(list.map((x) => RatingModel.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory GetRatingResponse.fromJson(String source) =>
      GetRatingResponse.fromList(json.decode(source));

  @override
  String toString() => 'GetRatingResponse(ratings: $ratings)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetRatingResponse && listEquals(other.ratings, ratings);
  }

  @override
  int get hashCode => ratings.hashCode;
}
