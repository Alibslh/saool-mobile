import 'dart:convert';

import 'base_response.dart';

class SignUpResponse extends BaseResponse {
  final String id;
  final String jwt;

  SignUpResponse({
    required this.id,
    required this.jwt,
  });

  SignUpResponse copyWith({
    String? id,
    String? jwt,
  }) {
    return SignUpResponse(
      id: id ?? this.id,
      jwt: jwt ?? this.jwt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'jwt': jwt,
    };
  }

  factory SignUpResponse.fromMap(Map<String, dynamic> map) {
    return SignUpResponse(
      id: map['user']['id'].toString(),
      jwt: map['jwt'],
    );
  }

  String toJson() => json.encode(toMap());

  factory SignUpResponse.fromJson(String source) =>
      SignUpResponse.fromMap(json.decode(source));

  @override
  String toString() => 'SignUpResponse(id: $id, jwt: $jwt)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SignUpResponse && other.id == id && other.jwt == jwt;
  }

  @override
  int get hashCode => id.hashCode ^ jwt.hashCode;
}
