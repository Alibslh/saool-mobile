import 'package:saool/models/responses/base_response.dart';

class StatusResponse {
  final int statusCode;
  final BaseResponse baseResponse;

  StatusResponse({
    required this.statusCode,
    required this.baseResponse,
  });
}
