import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:saool/models/responses/base_response.dart';

import '../message.dart';

class ErrorResponse extends BaseResponse {
  final int statusCode;
  final String error;
  final List<Message> messages;
  ErrorResponse({
    required this.statusCode,
    required this.error,
    required this.messages,
  });

  ErrorResponse copyWith({
    int? statusCode,
    String? error,
    List<Message>? messages,
  }) {
    return ErrorResponse(
      statusCode: statusCode ?? this.statusCode,
      error: error ?? this.error,
      messages: messages ?? this.messages,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'statusCode': statusCode,
      'error': error,
      'messages': messages.map((x) => x.toMap()).toList(),
    };
  }

  factory ErrorResponse.fromMap(Map<String, dynamic> map) {
    return ErrorResponse(
      statusCode: map['statusCode'],
      error: map['error'],
      messages: List<Message>.from(
          map['message'][0]['messages']?.map((x) => Message.fromMap(x))),
    );
  }

  factory ErrorResponse.fromMap2(Map<String, dynamic> map) {
    return ErrorResponse(
      statusCode: map['statusCode'],
      error: map['error'],
      messages: [Message(id: '1', message: map['message'])],
    );
  }

  String toJson() => json.encode(toMap());

  factory ErrorResponse.fromJson(String source) =>
      ErrorResponse.fromMap(json.decode(source));

  factory ErrorResponse.fromJson2(String source) =>
      ErrorResponse.fromMap2(json.decode(source));

  @override
  String toString() =>
      'ApiResponse(statusCode: $statusCode, error: $error, messages: $messages)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ErrorResponse &&
        other.statusCode == statusCode &&
        other.error == error &&
        listEquals(other.messages, messages);
  }

  @override
  int get hashCode => statusCode.hashCode ^ error.hashCode ^ messages.hashCode;
}
