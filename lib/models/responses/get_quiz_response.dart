import 'dart:convert';

import '../quiz_model_extended.dart';
import 'base_response.dart';

class GetQuizResponse extends BaseResponse {
  final QuizModelExtended quiz;
  GetQuizResponse({
    required this.quiz,
  });

  GetQuizResponse copyWith({
    QuizModelExtended? quiz,
  }) {
    return GetQuizResponse(
      quiz: quiz ?? this.quiz,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'quiz': quiz.toMap(),
    };
  }

  factory GetQuizResponse.fromMap(Map<String, dynamic> map) {
    return GetQuizResponse(
      quiz: QuizModelExtended.fromMap(map['quizzes'][0]),
    );
  }

  String toJson() => json.encode(toMap());

  factory GetQuizResponse.fromJson(String source) =>
      GetQuizResponse.fromMap(json.decode(source));

  @override
  String toString() => 'GetQuizResponse(quiz: $quiz)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetQuizResponse && other.quiz == quiz;
  }

  @override
  int get hashCode => quiz.hashCode;
}
