import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:saool/models/category_model.dart';
import 'package:saool/models/responses/base_response.dart';

class GetCategoriesResponse extends BaseResponse {
  final List<CategoryModel> categories;

  GetCategoriesResponse({
    required this.categories,
  });

  GetCategoriesResponse copyWith({
    List<CategoryModel>? categories,
  }) {
    return GetCategoriesResponse(
      categories: categories ?? this.categories,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'categories': categories.map((x) => x.toMap()).toList(),
    };
  }

  factory GetCategoriesResponse.fromMap(Map<String, dynamic> map) {
    return GetCategoriesResponse(
      categories: List<CategoryModel>.from(
          map['categories']?.map((x) => CategoryModel.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory GetCategoriesResponse.fromJson(String source) =>
      GetCategoriesResponse.fromMap(json.decode(source));

  @override
  String toString() => 'GetCategoriesResponse(categories: $categories)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetCategoriesResponse &&
        listEquals(other.categories, categories);
  }

  @override
  int get hashCode => categories.hashCode;
}
