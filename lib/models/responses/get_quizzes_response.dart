import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:saool/models/quiz_model.dart';

import 'base_response.dart';

class GetQuizzesResponse extends BaseResponse {
  final List<QuizModel> quizzes;

  GetQuizzesResponse({
    required this.quizzes,
  });

  GetQuizzesResponse copyWith({
    List<QuizModel>? quizzes,
  }) {
    return GetQuizzesResponse(
      quizzes: quizzes ?? this.quizzes,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'quizzes': quizzes.map((x) => x.toMap()).toList(),
    };
  }

  factory GetQuizzesResponse.fromMap(Map<String, dynamic> map) {
    List<QuizModel> quizModels = [];
    for (var x in map['quizzes']) {
      if (x['category'] != null) {
        quizModels.add(QuizModel.fromMap(x));
      }
    }

    return GetQuizzesResponse(
      quizzes: quizModels,
      // quizzes: List<QuizModel>.from(
      //     map['quizzes']?.map((x) => QuizModel.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory GetQuizzesResponse.fromJson(String source) =>
      GetQuizzesResponse.fromMap(json.decode(source));

  @override
  String toString() => 'GetQuizzesResponse(quizzes: $quizzes)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetQuizzesResponse && listEquals(other.quizzes, quizzes);
  }

  @override
  int get hashCode => quizzes.hashCode;
}
