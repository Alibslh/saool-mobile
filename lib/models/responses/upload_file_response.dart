import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'base_response.dart';

class UploadFileResponse extends BaseResponse {
  final String id;
  final String name;
  final String url;
  final Map<String, ImageDetails> formats;

  UploadFileResponse({
    required this.id,
    required this.name,
    required this.url,
    required this.formats,
  });

  UploadFileResponse copyWith({
    String? id,
    String? name,
    String? url,
    Map<String, ImageDetails>? formats,
  }) {
    return UploadFileResponse(
      id: id ?? this.id,
      name: name ?? this.name,
      url: url ?? this.url,
      formats: formats ?? this.formats,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'url': url,
      'formats': formats,
    };
  }

  factory UploadFileResponse.fromMap(Map<String, dynamic> map) {
    return UploadFileResponse(
      id: map['id'],
      name: map['name'],
      url: map['url'],
      formats: Map<String, ImageDetails>.from(map['formats']),
    );
  }

  factory UploadFileResponse.fromList(List<dynamic> list) {
    Map<String, dynamic> map = list.first;
    Map<String, ImageDetails> newFormats = {};
    for (var e in (map['formats'] as Map).entries) {
      newFormats[e.key] = ImageDetails.fromMap(e.value);
    }

    return UploadFileResponse(
      id: map['id'].toString(),
      name: map['name'],
      url: map['url'],
      formats: newFormats,
    );
  }

  String toJson() => json.encode(toMap());

  factory UploadFileResponse.fromJson(String source) =>
      UploadFileResponse.fromList(json.decode(source));

  @override
  String toString() {
    return 'UploadFileResponse(id: $id, name: $name, url: $url, formats: $formats)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UploadFileResponse &&
        other.id == id &&
        other.name == name &&
        other.url == url &&
        mapEquals(other.formats, formats);
  }

  @override
  int get hashCode {
    return id.hashCode ^ name.hashCode ^ url.hashCode ^ formats.hashCode;
  }
}

class ImageDetails {
  final String url;
  final String ext;

  ImageDetails({
    required this.url,
    required this.ext,
  });

  ImageDetails copyWith({
    String? url,
    String? ext,
  }) {
    return ImageDetails(
      url: url ?? this.url,
      ext: ext ?? this.ext,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'url': url,
      'ext': ext,
    };
  }

  factory ImageDetails.fromMap(Map<String, dynamic> map) {
    return ImageDetails(
      url: map['url'],
      ext: map['ext'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ImageDetails.fromJson(String source) =>
      ImageDetails.fromMap(json.decode(source));

  @override
  String toString() => 'ImageDetails(url: $url, ext: $ext)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ImageDetails && other.url == url && other.ext == ext;
  }

  @override
  int get hashCode => url.hashCode ^ ext.hashCode;
}
