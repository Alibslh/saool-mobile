import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';

import 'package:saool/models/image_model.dart';

class QuizImages {
  List<QuestionImage>? questionImages;
  QuizImages({
    this.questionImages,
  });

  void addQuestionImage({required String questionId, required File file}) {
    if (questionImages == null) {
      questionImages = [];
    }

    // check if element with specified questionId already exists, if so, just update it
    int index = questionImages!.indexWhere((e) => e.id == questionId);
    if (index != -1) {
      // already exists
      questionImages![index] = questionImages![index].copyWith(
        id: questionId,
        image: ImageFileModel(file: file),
      );
    } else {
      questionImages!.add(
        QuestionImage(
          id: questionId,
          image: ImageFileModel(file: file),
        ),
      );
    }
  }

  QuestionImage? getQuestionImage({required String questionId}) {
    QuestionImage? questionImage = questionImages?.firstWhere(
        (e) => e.id == questionId,
        orElse: () => QuestionImage(id: '-1'));
    if (questionImage != null && questionImage.id != '-1') {
      return questionImage;
    }
  }

  void addChoiceImage({
    required String questionId,
    required String choiceId,
    required File file,
  }) {
    QuestionImage? questionImage = questionImages?.firstWhere(
        (e) => e.id == questionId,
        orElse: () => QuestionImage(id: '-1'));

    ChoiceImage imageToAdd =
        ChoiceImage(id: choiceId, image: ImageFileModel(file: file));

    if (questionImage == null || questionImage.id == '-1') {
      questionImages = [
        QuestionImage(
          id: questionId,
          choiceImages: [imageToAdd],
        )
      ];
      return;
    } else {
      // valid question image found, so just add the choice image to it
      if (questionImage.choiceImages == null) {
        questionImage.choiceImages = [imageToAdd];
        return;
      } else {
        // check if the choice image with the specified id already exists, if so just update it
        int index =
            questionImage.choiceImages!.indexWhere((e) => e.id == choiceId);
        if (index != -1) {
          questionImage.choiceImages![index] = imageToAdd;
        } else {
          questionImage.choiceImages!.add(imageToAdd);
        }
      }
    }
  }

  ChoiceImage? getChoiceImage(
      {required String questionId, required String choiceId}) {
    QuestionImage? questionImage = questionImages?.firstWhere(
        (e) => e.id == questionId,
        orElse: () => QuestionImage(id: '-1'));
    if (questionImage != null &&
        questionImage.id != '-1' &&
        questionImage.choiceImages != null) {
      for (var c in questionImage.choiceImages!) {
        if (c.id == choiceId) {
          return c;
        }
      }
    }
  }

  QuizImages copyWith({
    List<QuestionImage>? questionImages,
  }) {
    return QuizImages(
      questionImages: questionImages ?? this.questionImages,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'questionImages': questionImages?.map((x) => x.toMap()).toList(),
    };
  }

  factory QuizImages.fromMap(Map<String, dynamic> map) {
    return QuizImages(
      questionImages: List<QuestionImage>.from(
          map['questionImages']?.map((x) => QuestionImage.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory QuizImages.fromJson(String source) =>
      QuizImages.fromMap(json.decode(source));

  @override
  String toString() => 'QuizImages(questionImages: $questionImages)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is QuizImages &&
        listEquals(other.questionImages, questionImages);
  }

  @override
  int get hashCode => questionImages.hashCode;
}

class QuestionImage {
  final String id;
  ImageFileModel? image;
  List<ChoiceImage>? choiceImages;
  QuestionImage({
    required this.id,
    this.image,
    this.choiceImages,
  });

  QuestionImage copyWith({
    String? id,
    ImageFileModel? image,
    List<ChoiceImage>? choiceImages,
  }) {
    return QuestionImage(
      id: id ?? this.id,
      image: image ?? this.image,
      choiceImages: choiceImages ?? this.choiceImages,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'image': image?.toMap(),
      'choiceImages': choiceImages?.map((x) => x.toMap()).toList(),
    };
  }

  factory QuestionImage.fromMap(Map<String, dynamic> map) {
    return QuestionImage(
      id: map['id'],
      image: ImageFileModel.fromMap(map['image']),
      choiceImages: List<ChoiceImage>.from(
          map['choiceImages']?.map((x) => ChoiceImage.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory QuestionImage.fromJson(String source) =>
      QuestionImage.fromMap(json.decode(source));

  @override
  String toString() =>
      'QuestionImage(id: $id, image: $image, choiceImages: $choiceImages)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is QuestionImage &&
        other.id == id &&
        other.image == image &&
        listEquals(other.choiceImages, choiceImages);
  }

  @override
  int get hashCode => id.hashCode ^ image.hashCode ^ choiceImages.hashCode;
}

class ChoiceImage {
  final String id;
  ImageFileModel? image;

  ChoiceImage({
    required this.id,
    this.image,
  });

  get file => image?.file;

  ChoiceImage copyWith({
    String? id,
    ImageFileModel? image,
  }) {
    return ChoiceImage(
      id: id ?? this.id,
      image: image ?? this.image,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'image': image?.toMap(),
    };
  }

  factory ChoiceImage.fromMap(Map<String, dynamic> map) {
    return ChoiceImage(
      id: map['id'],
      image: ImageFileModel.fromMap(map['image']),
    );
  }

  String toJson() => json.encode(toMap());

  factory ChoiceImage.fromJson(String source) =>
      ChoiceImage.fromMap(json.decode(source));

  @override
  String toString() => 'ChoiceImage(id: $id, image: $image)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ChoiceImage && other.id == id && other.image == image;
  }

  @override
  int get hashCode => id.hashCode ^ image.hashCode;
}

class ImageFileModel {
  File file;
  ImageModel? imageModel;

  ImageFileModel({
    required this.file,
    this.imageModel,
  });

  ImageFileModel copyWith({
    File? file,
    ImageModel? imageModel,
  }) {
    return ImageFileModel(
      file: file ?? this.file,
      imageModel: imageModel ?? this.imageModel,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'file': file,
      'imageModel': imageModel?.toMap(),
    };
  }

  factory ImageFileModel.fromMap(Map<String, dynamic> map) {
    return ImageFileModel(
      file: map['file'],
      imageModel: ImageModel.fromMap(map['imageModel']),
    );
  }

  String toJson() => json.encode(toMap());

  factory ImageFileModel.fromJson(String source) =>
      ImageFileModel.fromMap(json.decode(source));

  @override
  String toString() => 'ImageFileModel(file: $file, imageModel: $imageModel)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ImageFileModel &&
        other.file == file &&
        other.imageModel == imageModel;
  }

  @override
  int get hashCode => file.hashCode ^ imageModel.hashCode;
}
