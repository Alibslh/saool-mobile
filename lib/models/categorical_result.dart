import 'dart:convert';

import 'package:saool/models/image_model.dart';

class CategoricalResult {
  final String? id;
  final String name;
  final ImageModel image;

  CategoricalResult({
    this.id,
    required this.name,
    required this.image,
  });

  CategoricalResult copyWith({
    String? id,
    String? name,
    ImageModel? image,
  }) {
    return CategoricalResult(
      id: id ?? this.id,
      name: name ?? this.name,
      image: image ?? this.image,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'image': image.toMap(),
    };
  }

  factory CategoricalResult.fromMap(Map<String, dynamic> map) {
    return CategoricalResult(
      id: map['id'],
      name: map['name'],
      image: ImageModel.fromMap(map['image']),
    );
  }

  String toJson() => json.encode(toMap());

  factory CategoricalResult.fromJson(String source) =>
      CategoricalResult.fromMap(json.decode(source));

  @override
  String toString() => 'CategoricalResult(id: $id, name: $name, image: $image)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CategoricalResult &&
        other.id == id &&
        other.name == name &&
        other.image == image;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ image.hashCode;
}
