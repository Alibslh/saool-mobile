class Answer {
  final String id;
  final String text;

  Answer({
    required this.id,
    required this.text,
  });
}