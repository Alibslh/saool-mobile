import 'dart:io';

import 'package:flutter/material.dart';
import 'package:saool/constants/file_upload_constants.dart';
import 'package:saool/models/image_model.dart';
import 'package:saool/models/profile_settings_model.dart';
import 'package:saool/models/requests/sign_in_request.dart';
import 'package:saool/models/requests/sign_up_request.dart';
import 'package:saool/models/requests/upload_file_request.dart';
import 'package:saool/models/responses/base_response.dart';
import 'package:saool/models/responses/error_response.dart';
import 'package:saool/models/responses/get_user_profile_response.dart';
import 'package:saool/models/responses/sign_in_response.dart';
import 'package:saool/models/responses/sign_up_response.dart';
import 'package:saool/models/responses/upload_file_response.dart';
import 'package:saool/models/sign_in_model.dart';
import 'package:saool/models/sign_up_model.dart';
import 'package:saool/models/user_model.dart';
import 'package:saool/models/user_permissions_user.dart';
import 'package:saool/services/api_service.dart';
import 'package:saool/services/shared_prefs.dart';
import 'package:saool/utils/snack_utils.dart';

import '../services/api_service.dart';

class UserViewModel with ChangeNotifier {
  UserModel? userModel;
  UserModel? publicUserModel;

  Future<bool> signUp(BuildContext context, SignUpModel signUpModel) async {
    if (signUpModel.username.length <= 0 &&
        signUpModel.email.length <= 0 &&
        signUpModel.password.length <= 0) {
      return false;
    }
    BaseResponse _baseResponse = await ApiService().signUp(
      request: SignUpRequest(
        username: signUpModel.username,
        email: signUpModel.email,
        password: signUpModel.password,
      ),
    );
    print(_baseResponse);
    if (_baseResponse is SignUpResponse) {
      // save jwt in shared prefs
      SharedPrefs.saveJwt(_baseResponse.jwt);
      SnackUtils.showSnack(context, 'العمل ناجح');

      return true;
    } else {
      // _baseResponse is ErrorResponse
      ErrorResponse errorResponse = _baseResponse as ErrorResponse;
      SnackUtils.showSnack(context, errorResponse.messages.first.message,
          isRTL: false);
      return false;
    }
  }

  Future<bool> signIn(BuildContext context, SignInModel signInModel) async {
    if (signInModel.identifier.length <= 0 &&
        signInModel.password.length <= 0) {
      return false;
    }
    BaseResponse _baseResponse = await ApiService().signIn(
      request: SignInRequest(
        identifier: signInModel.identifier,
        password: signInModel.password,
      ),
    );
    print(_baseResponse);
    if (_baseResponse is SignInResponse) {
      SignInResponse signInResponse = _baseResponse;
      userModel =
          signInResponse.userModel.copyWith(jwt: _baseResponse.userModel.jwt);
      notifyListeners();
      // save jwt in shared prefs
      SharedPrefs.saveJwt(_baseResponse.userModel.jwt);
      SnackUtils.showSnack(context, 'العمل ناجح');

      return true;
    } else {
      // _baseResponse is ErrorResponse
      ErrorResponse errorResponse = _baseResponse as ErrorResponse;

      SnackUtils.showSnack(context, errorResponse.messages.first.message,
          isRTL: false);

      return false;
    }
  }

  Future<bool> signOut(BuildContext context) async {
    if (userModel == null) {
      SnackUtils.showSnack(context, 'فشل العمل');
      return false;
    }
    // remove jwt in shared prefs
    SharedPrefs.removeJwt();
    userModel = null;
    notifyListeners();
    return true;
  }

  Future<bool> getUserProfile(BuildContext context, String jwt) async {
    BaseResponse _baseResponse = await ApiService().getUserProfile(jwt: jwt);

    print(_baseResponse);
    if (_baseResponse is GetUserProfileResponse) {
      // SnackUtils.showSnack(context, 'العمل ناجح');
      GetUserProfileResponse getUserProfileResponse = _baseResponse;
      userModel = getUserProfileResponse.userModel.copyWith(jwt: jwt);
      notifyListeners();

      return true;
    } else {
      return false;
    }
  }

  Future<bool> getPublicUserProfile(
      BuildContext context, String id, String jwt) async {
    if (publicUserModel != null) {
      publicUserModel = null;
      notifyListeners();
    }
    BaseResponse _baseResponse =
        await ApiService().getPublicUserProfile(id: id, jwt: jwt);

    print(_baseResponse);
    if (_baseResponse is GetUserProfileResponse) {
      GetUserProfileResponse getUserProfileResponse = _baseResponse;
      publicUserModel = getUserProfileResponse.userModel;
      notifyListeners();

      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateProfile(
      BuildContext context, ProfileSettingsModel profileSettingsModel) async {
    if (profileSettingsModel.toValidMap().isEmpty) {
      // 'Please add some fields to be updated'
      SnackUtils.showSnack(context, 'الرجاء إضافة بعض الحقول لتحديثها');
      return false;
    }
    if (userModel == null || userModel?.id == null || userModel?.jwt == null) {
      print(
          'userModel does not have the required data to proceed with the request');
      return false;
    }
    UserPermissionsUser? userPermissionsUser = await ApiService().updateProfile(
      userId: userModel!.id,
      jwt: userModel!.jwt,
      profileSettingsModel: profileSettingsModel,
    );

    if (userPermissionsUser != null) {
      // update the userModel with the new values
      print('userPermissionsUser: ' + userPermissionsUser.toString());
      userModel = userModel?.copyWith(
        username: userPermissionsUser.username,
        email: userPermissionsUser.email,
        userPermissionsUser: userPermissionsUser,
      );
      notifyListeners();
      SnackUtils.showSnack(context, 'العمل ناجح');
    } else {
      SnackUtils.showSnack(context, 'فشل العمل');
    }
    return userPermissionsUser != null;
  }

  Future<ImageModel?> uploadFile(
    BuildContext context,
    UploadFileRequest uploadFileRequest, {
    required String filename,
    ImageFormat imageFormat = ImageFormat.LARGE,
  }) async {
    if (userModel?.jwt == null) {
      return null;
    }
    UploadFileResponse? uploadFileResponse = await ApiService().uploadFile(
        uploadFileRequest: uploadFileRequest,
        filename: filename,
        imageFormat: imageFormat,
        jwt: userModel!.jwt);
    print(uploadFileResponse);
    if (uploadFileResponse != null) {
      // 'Press Submit button to update the image'
      SnackUtils.showSnack(context, 'اضغط على زر إرسال لتحديث الصورة');
      return ImageModel(id: uploadFileResponse.id, url: uploadFileResponse.url);
    } else {
      SnackUtils.showSnack(context, 'فشل العمل');
    }
  }

  Future<ImageModel?> uploadFiles(
    BuildContext context,
    List<File> files, {
    required String filename,
    ImageFormat imageFormat = ImageFormat.LARGE,
  }) async {
    if (userModel?.jwt == null) {
      return null;
    }
    UploadFileResponse? uploadFileResponse = await ApiService().uploadFiles(
        files: files,
        filename: filename,
        imageFormat: imageFormat,
        jwt: userModel!.jwt);
    print(uploadFileResponse);
    if (uploadFileResponse != null) {
      SnackUtils.showSnack(context, 'العمل ناجح');
      return ImageModel(id: uploadFileResponse.id, url: uploadFileResponse.url);
    } else {
      SnackUtils.showSnack(context, 'فشل العمل');
    }
  }

  Future<void> initialize(BuildContext context) async {
    String? savedJwt = await SharedPrefs.getJwt();
    print('savedJwt: $savedJwt');
    if (savedJwt != null) {
      bool success = await getUserProfile(context, savedJwt);
      if (!success) {
        // remove the jwt
        await SharedPrefs.removeJwt();
        return;
      }
    }
    // else, user did not sign up or sign in yet
    return;
  }

  String? getUserName() {
    return userModel?.username;
  }

  String? getPublicUserName() {
    return publicUserModel?.username;
  }

  bool isSignedIn() => userModel != null;
}
