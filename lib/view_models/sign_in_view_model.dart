import 'package:flutter/material.dart';
import 'package:saool/models/requests/sign_in_request.dart';
import 'package:saool/models/responses/base_response.dart';
import 'package:saool/models/responses/error_response.dart';
import 'package:saool/models/responses/sign_in_response.dart';
import 'package:saool/models/sign_in_model.dart';
import 'package:saool/models/user_model.dart';
import 'package:saool/services/api_service.dart';
import 'package:saool/utils/snack_utils.dart';

class SignInViewModel with ChangeNotifier {
  UserModel? userModel;
  Future<bool> signIn(BuildContext context, SignInModel signInModel) async {
    if (signInModel.identifier.length <= 0 &&
        signInModel.password.length <= 0) {
      return false;
    }
    BaseResponse _baseResponse = await ApiService().signIn(
      request: SignInRequest(
        identifier: signInModel.identifier,
        password: signInModel.password,
      ),
    );
    print(_baseResponse);
    if (_baseResponse is SignInResponse) {
      SnackUtils.showSnack(context, 'العمل ناجح');
      SignInResponse signInResponse = _baseResponse;
      userModel = signInResponse.userModel;
      notifyListeners();

      return true;
    } else {
      // _baseResponse is ErrorResponse
      ErrorResponse errorResponse = _baseResponse as ErrorResponse;
      SnackUtils.showSnack(context, errorResponse.messages.first.message,
          isRTL: false);
      return false;
    }
  }
}
