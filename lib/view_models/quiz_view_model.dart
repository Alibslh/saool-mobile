import 'package:flutter/material.dart';
import 'package:saool/constants/quiz_type_constants.dart';
import 'package:saool/models/category_model.dart';
import 'package:saool/models/quiz_attempt_model.dart';
import 'package:saool/models/quiz_model.dart';
import 'package:saool/models/quiz_model_extended.dart';
import 'package:saool/models/rating_model.dart';
import 'package:saool/models/requests/add_rating_request.dart';
import 'package:saool/models/requests/quiz_attempt_request.dart';
import 'package:saool/models/responses/base_response.dart';
import 'package:saool/models/responses/get_quiz_response.dart';
import 'package:saool/models/responses/get_quizzes_response.dart';
import 'package:saool/models/responses/get_rating_response.dart';
import 'package:saool/services/api_service.dart';
import 'package:saool/utils/snack_utils.dart';
import 'package:saool/utils/static_provider.dart';
import 'package:saool/view_models/user_view_model.dart';

class QuizViewModel with ChangeNotifier {
  List<QuizModel>? quizzes;
  QuizModelExtended? quiz;
  int questionNo = 0;
  int correctAnswersCount = 0;
  Map<String, String>? selectedChoices;
  List<RatingModel>? ratings;

  Future<bool> getQuizzes(BuildContext context) async {
    BaseResponse _baseResponse = await ApiService().getQuizzes();

    print(_baseResponse);
    if (_baseResponse is GetQuizzesResponse) {
      GetQuizzesResponse getQuizzesResponse = _baseResponse;
      quizzes = getQuizzesResponse.quizzes;
      notifyListeners();

      return true;
    } else {
      return false;
    }
  }

  Future<bool> getQuiz(BuildContext context, String id) async {
    if (quiz != null && quiz!.id == id) {
      return true;
    } else {
      quiz = null;
      notifyListeners();
    }
    BaseResponse _baseResponse = await ApiService().getQuiz(id: id);

    print(_baseResponse);
    if (_baseResponse is GetQuizResponse) {
      GetQuizResponse getQuizResponse = _baseResponse;
      quiz = getQuizResponse.quiz;
      notifyListeners();

      return true;
    } else {
      return false;
    }
  }

  Future<bool> addRating(BuildContext context, RatingModel ratingModel) async {
    if (ratingModel.value == null || quiz == null) {
      return false;
    }
    bool success = await ApiService().addRating(
        request: AddRatingRequest(
      value: ratingModel.value!,
      quizId: quiz!.id,
      sessionId: StaticProvider.of<UserViewModel>(context).userModel?.id ?? '',
      text: ratingModel.text,
    ));
    if (success) {
      SnackUtils.showSnack(context, 'العمل ناجح');
    } else {
      SnackUtils.showSnack(context, 'فشل العمل');
    }
    return success;
  }

  Future<bool> getRating(BuildContext context, String quizId) async {
    BaseResponse _baseResponse = await ApiService().getRating(quizId: quizId);

    print(_baseResponse);
    if (_baseResponse is GetRatingResponse) {
      GetRatingResponse getRatingResponse = _baseResponse;
      ratings = getRatingResponse.ratings.map((e) => e).toList();
      notifyListeners();

      return true;
    } else {
      return false;
    }
  }

  Future<bool> postQuizAttempts(
      BuildContext context, QuizAttemptModel quizAttemptModel, String? userId) async {
    if (quizAttemptModel.result == null ||
        quizAttemptModel.questionsCount == null ||
        quiz == null) {
      return false;
    }
    bool success = await ApiService().postQuizAttempts(
        request: QuizAttemptRequest(
      result: quizAttemptModel.result!,
      quiz: quiz!.id,
      questionsCount: quizAttemptModel.questionsCount!,
      sessionId: StaticProvider.of<UserViewModel>(context).userModel?.id ?? '',
      usersPermissionsUser: userId,
      platform: 'mobile'
    ));
    print('postQuizAttempts: $success');
    return success;
  }

  // Others
  void resetVariables() {
    questionNo = 0;
    correctAnswersCount = 0;
    selectedChoices = null;
    notifyListeners();
  }

  void incrementCorrectAnswersCount() {
    correctAnswersCount++;
    notifyListeners();
  }

  bool incrementQuestionNo() {
    if (quiz?.questions.length != null &&
        questionNo < (quiz!.questions.length - 1)) {
      questionNo += 1;
      notifyListeners();
      return true;
    }
    return false;
  }

  void setSelectedAnswer(String questionId, String choiceId) {
    if (selectedChoices == null) {
      selectedChoices = {};
    }
    selectedChoices![questionId] = choiceId;
    if (quiz?.type == QuizTypeConstants.categoricalType)
      print(
          '$questionNo: $questionId: ${quiz!.questions.firstWhere((e) => e.id == questionId).choices.firstWhere((c) => c.id == choiceId).type}');
    notifyListeners();
  }

  List<CategoryModel>? getCategories() {
    return quizzes?.map((e) => e.category).toSet().toList();
  }

  List<CategoryModel>? getQuizCategories(List<QuizModel>? newQuizzes) {
    return newQuizzes?.map((e) => e.category).toSet().toList();
  }

  String? getCategoryImageUrl(String catId) {
    return quizzes?.firstWhere((e) => e.category.id == catId).imageUrl;
  }

  String getCategoricalResult() {
    if (quiz?.questions == null || selectedChoices == null) {
      return '';
    }
    Map<String, int> categoricalResults = {};
    for (int i = 0; i < quiz!.questions.length; ++i) {
      var q = quiz!.questions[i];
      String? choiceType =
          q.choices.firstWhere((c) => c.id == selectedChoices![q.id]).type;
      if (choiceType == null) {
        return '';
      }
      categoricalResults[choiceType] =
          (categoricalResults[choiceType] ?? 0) + 1;
    }
    if (categoricalResults.isEmpty) {
      return '';
    }
    return _getMax(categoricalResults);
  }

  String _getMax(Map<String, int> categoricalResults) {
    int max = categoricalResults.values.first;
    String maxKey = categoricalResults.keys.first;
    for (var e in categoricalResults.entries) {
      if (e.value > max) {
        max = e.value;
        maxKey = e.key;
      }
    }
    return maxKey;
  }
}
