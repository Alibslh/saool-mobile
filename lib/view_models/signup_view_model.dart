import 'package:flutter/material.dart';
import 'package:saool/models/requests/sign_up_request.dart';
import 'package:saool/models/responses/base_response.dart';
import 'package:saool/models/responses/error_response.dart';
import 'package:saool/models/responses/sign_up_response.dart';
import 'package:saool/models/sign_up_model.dart';
import 'package:saool/services/api_service.dart';
import 'package:saool/utils/snack_utils.dart';

class SignUpViewModel with ChangeNotifier {
  Future<bool> signUp(BuildContext context, SignUpModel signUpModel) async {
    if (signUpModel.username.length <= 0 &&
        signUpModel.email.length <= 0 &&
        signUpModel.password.length <= 0) {
      return false;
    }
    BaseResponse _baseResponse = await ApiService().signUp(
      request: SignUpRequest(
        username: signUpModel.username,
        email: signUpModel.email,
        password: signUpModel.password,
      ),
    );
    print(_baseResponse);
    if (_baseResponse is SignUpResponse) {
      SnackUtils.showSnack(context, 'العمل ناجح');
      return true;
    } else {
      // _baseResponse is ErrorResponse
      ErrorResponse errorResponse = _baseResponse as ErrorResponse;
      SnackUtils.showSnack(context, errorResponse.messages.first.message,
          isRTL: false);
      return false;
    }
  }
}
