import 'dart:io';

import 'package:flutter/material.dart';
import 'package:saool/constants/file_upload_constants.dart';
import 'package:saool/constants/quiz_constants.dart';
import 'package:saool/constants/quiz_type_constants.dart';
import 'package:saool/models/category_model.dart';
import 'package:saool/models/choice_model.dart';
import 'package:saool/models/google_form_model.dart';
import 'package:saool/models/image_model.dart';
import 'package:saool/models/question_model.dart';
import 'package:saool/models/quiz_images.dart';
import 'package:saool/models/quiz_model_extended.dart';
import 'package:saool/models/requests/google_form_request.dart';
import 'package:saool/models/requests/update_quiz_image_request.dart';
import 'package:saool/models/requests/upload_file_request.dart';
import 'package:saool/models/responses/base_response.dart';
import 'package:saool/models/responses/get_categories_response.dart';
import 'package:saool/models/responses/upload_file_response.dart';
import 'package:saool/models/user_permissions_user.dart';
import 'package:saool/services/api_service.dart';
import 'package:saool/utils/snack_utils.dart';

class QuizCreationViewModel with ChangeNotifier {
  QuizModelExtended? quiz;
  QuizImages quizImages;
  List<CategoryModel>? categories;

  QuizCreationViewModel({required this.quizImages});

  Future<bool> addQuiz(BuildContext context,
      {required UserPermissionsUser? userPermissionsUser}) async {
    if (!validate(context)) {
      return false;
    }
    if (userPermissionsUser == null) {
      print('userPermissionsUser is null');
      return false;
    }
    // INFO: Adding userId in the quiz
    bool success = await ApiService().addQuiz(
        quiz: quiz!.copyWith(userPermissionsUser: userPermissionsUser));

    print('addQuiz: $success');
    if (success) {
      SnackUtils.showSnack(context, 'العمل ناجح');
    } else {
      SnackUtils.showSnack(context, 'فشل العمل');
    }
    return success;
  }

  void setQuiz(QuizModelExtended newQuiz) {
    quiz = newQuiz;
    resetQuizImages();
    notifyListeners();
  }

  void resetQuizImages() {
    quizImages = QuizImages();
  }

  bool addChoice(String questionId, ChoiceModel choiceModel) {
    if (quiz != null) {
      var question = quiz!.questions.firstWhere((e) => e.id == questionId);

      question.choices.add(
        choiceModel.copyWith(
          id: (question.choices.length + 1).toString(),
        ),
      );
      notifyListeners();
      return true;
    }
    return false;
  }

  bool addDummyQuestion() {
    if (quiz != null) {
      quiz!.questions.add(
        QuestionModel(
          id: (quiz!.questions.length + 1).toString(),
          statement: '',
          type: QuizConstants.textTypeQuestion,
          point: 10,
          choices: [
            ChoiceModel(
              id: '1',
              text: '',
              type: '',
              isCorrect: false,
            ),
          ],
        ),
      );
      notifyListeners();
      return true;
    }
    return false;
  }

  bool removeChoice(String questionId, String choiceId) {
    if (quiz != null) {
      var choices =
          quiz!.questions.firstWhere((e) => e.id == questionId).choices;
      var choiceToRemove = choices.firstWhere((c) => c.id == choiceId);
      choices.remove(choiceToRemove);
      notifyListeners();
      return true;
    }
    return false;
  }

  void setSingleChoiceAsCorrect(String questionId, String choiceId) {
    if (quiz == null) return;
    quiz = quiz!.copyWith(
        questions: quiz!.questions
            .map((e) => (e.id == questionId)
                ? e.copyWith(
                    choices: e.choices
                        .map((c) => (c.id == choiceId)
                            ? c.copyWith(isCorrect: true)
                            : c.copyWith(isCorrect: false))
                        .toList())
                : e)
            .toList());
    notifyListeners();
  }

  void updateChoice(String questionId, String choiceId, String value) {
    if (quiz == null) return;
    quiz = quiz!.copyWith(
        questions: quiz!.questions
            .map((e) => (e.id == questionId)
                ? e.copyWith(
                    choices: e.choices
                        .map((c) =>
                            (c.id == choiceId) ? c.copyWith(text: value) : c)
                        .toList())
                : e)
            .toList());
    notifyListeners();
  }

  void updateChoiceType(String questionId, String choiceId, String type) {
    if (quiz == null) return;
    quiz = quiz!.copyWith(
        questions: quiz!.questions
            .map((e) => (e.id == questionId)
                ? e.copyWith(
                    choices: e.choices
                        .map((c) =>
                            (c.id == choiceId) ? c.copyWith(type: type) : c)
                        .toList())
                : e)
            .toList());
    notifyListeners();
  }

  void updateQuestion(String questionId, String value) {
    if (quiz == null) return;
    quiz = quiz!.copyWith(
        questions: quiz!.questions
            .map((e) => (e.id == questionId) ? e.copyWith(statement: value) : e)
            .toList());
    notifyListeners();
  }

  bool validate(BuildContext context) {
    if (quiz == null) return false;
    if (quiz!.questions.length < 2) {
      // 'Please add at least two questions'
      SnackUtils.showSnack(context, 'الرجاء إضافة سؤالين على الأقل');
      return false;
    }
    for (var q in quiz!.questions) {
      if (q.choices.length < 2) {
        // 'Please add at least two choices'
        SnackUtils.showSnack(context, 'الرجاء إضافة خيارين على الأقل');
        return false;
      }
      if (isCategorical()) {
        if (q.choices
            .any((e) => e.type == null || e.type!.replaceAll(' ', '') == '')) {
          // The question q has a type that is null or empty
          // 'Please select a valid category for each choice'
          SnackUtils.showSnack(context, 'الرجاء تحديد فئة صالحة لكل اختيار');
          return false;
        }
      } else {
        if (!q.choices.any((e) => e.isCorrect == true)) {
          // The question q does not have any choice that is marked as correct
          // 'Please mark at least one choice to be correct'
          SnackUtils.showSnack(
              context, 'يرجى تحديد خيار واحد على الأقل ليكون صحيحًا');
          return false;
        }
      }
    }
    return true;
  }

  bool isCategorical() =>
      (quiz != null && quiz!.type == QuizTypeConstants.categoricalType);

  void addQuestionImage({required String questionId, required File file}) {
    quizImages.addQuestionImage(questionId: questionId, file: file);
    notifyListeners();
  }

  QuestionImage? getQuestionImage({required String questionId}) {
    return quizImages.getQuestionImage(questionId: questionId);
  }

  void addChoiceImage({
    required String questionId,
    required String choiceId,
    required File file,
  }) {
    quizImages.addChoiceImage(
        questionId: questionId, choiceId: choiceId, file: file);
    notifyListeners();
  }

  ChoiceImage? getChoiceImage(
      {required String questionId, required String choiceId}) {
    return quizImages.getChoiceImage(
        questionId: questionId, choiceId: choiceId);
  }

  List<File>? getImageFiles() {
    List<File> files = [];
    if (quizImages.questionImages == null ||
        quizImages.questionImages!.isEmpty) {
      return null;
    }
    for (var q in quizImages.questionImages!) {
      if (q.image != null) {
        files.add(q.image!.file);
        if (q.choiceImages != null) {
          for (var c in q.choiceImages!) {
            if (c.image != null) {
              files.add(c.image!.file);
            }
          }
        }
      }
    }
    return files.isNotEmpty ? files : null;
  }

  void setImageUrls(List<ImageModel> images) {
    if (images.isEmpty) {
      return;
    }
    int index = 0;
    for (var q in quizImages.questionImages!) {
      if (q.image != null) {
        // files.add(q.image!.file);
        if (index >= images.length) {
          return;
        }
        q.image!.imageModel = images[index++];
        if (q.choiceImages != null) {
          for (var c in q.choiceImages!) {
            if (c.image != null) {
              // files.add(c.image!.file);
              if (index >= images.length) {
                return;
              }
              c.image!.imageModel = images[index++];
            }
          }
        }
      }
    }
  }

  Future<void> uploadImages(
    BuildContext context, {
    required String? jwt,
  }) async {
    String filename = FileUploadConstants.quizCreationPicture;
    if (quizImages.questionImages == null ||
        quizImages.questionImages!.isEmpty) {
      return null;
    }
    for (var q in quizImages.questionImages!) {
      if (q.image != null) {
        ImageModel? questionImageModel = await uploadFile(
          context,
          UploadFileRequest(
              files: q.image!.file, refId: '', ref: '', field: ''),
          jwt: jwt,
          filename: filename,
        );
        if (questionImageModel != null) {
          q.image!.imageModel = questionImageModel;
        }
        if (q.choiceImages != null) {
          for (var c in q.choiceImages!) {
            if (c.image != null) {
              ImageModel? choiceImageModel = await uploadFile(
                context,
                UploadFileRequest(
                    files: c.image!.file, refId: '', ref: '', field: ''),
                jwt: jwt,
                filename: filename,
              );
              if (choiceImageModel != null) {
                c.image!.imageModel = choiceImageModel;
              }
            }
          }
        }
      }
    }
    setQuizModelImages();
    notifyListeners();
  }

  Future<ImageModel?> uploadFile(
    BuildContext context,
    UploadFileRequest uploadFileRequest, {
    required String? jwt,
    required String filename,
    ImageFormat imageFormat = ImageFormat.LARGE,
  }) async {
    if (jwt == null) {
      return null;
    }
    UploadFileResponse? uploadFileResponse = await ApiService().uploadFile(
        uploadFileRequest: uploadFileRequest,
        filename: filename,
        imageFormat: imageFormat,
        jwt: jwt);
    print(uploadFileResponse);
    if (uploadFileResponse != null) {
      // SnackUtils.showSnack(context, 'العمل ناجح');
      return ImageModel(id: uploadFileResponse.id, url: uploadFileResponse.url);
    } else {
      // SnackUtils.showSnack(context, 'فشل العمل');
    }
  }

  void setQuizModelImages() {
    if (quiz == null ||
        quizImages.questionImages == null ||
        quizImages.questionImages!.isEmpty) {
      return;
    }
    for (var q in quizImages.questionImages!) {
      if (q.image != null) {
        int questionIndex = quiz!.questions.indexWhere((e) => e.id == q.id);
        if (questionIndex != -1) {
          quiz!.questions[questionIndex] = quiz!.questions[questionIndex]
              .copyWith(image: q.image!.imageModel);
          if (q.choiceImages != null) {
            for (var c in q.choiceImages!) {
              if (c.image != null) {
                int choiceIndex = quiz!.questions[questionIndex].choices
                    .indexWhere((e) => e.id == c.id);
                if (choiceIndex != -1) {
                  quiz!.questions[questionIndex].choices[choiceIndex] = quiz!
                      .questions[questionIndex].choices[choiceIndex]
                      .copyWith(image: c.image!.imageModel);
                }
              }
            }
          }
        }
      }
    }
  }

  Future<bool> addGoogleForm({
    required BuildContext context,
    required GoogleFormModel googleFormModel,
    required String? jwt,
    required String? userId,
  }) async {
    if (googleFormModel.image == null) {
      // 'Please add an image'
      SnackUtils.showSnack(context, 'الرجاء إضافة صورة');
      return false;
    }
    if (googleFormModel.category == null) {
      // 'Please select a category'
      SnackUtils.showSnack(context, 'الرجاء تحديد الفئة');
      return false;
    }
    if (jwt == null || userId == null) {
      // 'Please login first'
      SnackUtils.showSnack(context, 'الرجاء تسجيل الدخول أولا');
      return false;
    }
    ImageModel? imageModel = await uploadFile(
      context,
      UploadFileRequest(
          files: googleFormModel.image!, refId: '', ref: '', field: ''),
      jwt: jwt,
      // TODO: Make sure filename is used for Google Form Quiz Image
      filename: googleFormModel.imageName,
    );
    var googleFormRequest = GoogleFormRequest(
      link: googleFormModel.googleFormLink,
      category: googleFormModel.category!.id,
      usersPermissionsUser: userId,
    );
    String? createdQuizId =
        await ApiService().addGoogleFormQuiz(request: googleFormRequest);
    print(createdQuizId);

    // TODO: Update image for the returned quiz Id
    if (createdQuizId == null || imageModel == null) {
      return false;
    }
    return ApiService().updateQuiz(
        quizId: createdQuizId,
        updateQuizImageRequest: UpdateQuizImageRequest(image: imageModel));
  }

  Future<bool> getCategories(BuildContext context) async {
    BaseResponse _baseResponse = await ApiService().getCategories();

    print(_baseResponse);
    if (_baseResponse is GetCategoriesResponse) {
      GetCategoriesResponse getCategoriesResponse = _baseResponse;
      categories = getCategoriesResponse.categories;
      notifyListeners();

      return true;
    } else {
      return false;
    }
  }
}
