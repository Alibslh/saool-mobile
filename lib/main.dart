import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:saool/models/quiz_images.dart';
import 'package:saool/view_models/quiz_creation_view_model.dart';
import 'view_models/quiz_view_model.dart';

import 'view_models/user_view_model.dart';
import 'views/splash_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UserViewModel()),
        ChangeNotifierProvider(create: (_) => QuizViewModel()),
        ChangeNotifierProvider(
            create: (_) => QuizCreationViewModel(quizImages: QuizImages())),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          fontFamily: GoogleFonts.almarai().fontFamily,
          primarySwatch: Colors.blue,
          accentColor: Color(0xFF2AB7CA),
          scaffoldBackgroundColor: Colors.white,
        ),
        home: Directionality(
          textDirection: TextDirection.rtl,
          child: SplashPage(),
        ),
      ),
    );
  }
}
