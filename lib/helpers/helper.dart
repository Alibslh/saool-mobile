import 'dart:math' as math;

class Helper {
  static double toRadians(double angle) => (math.pi / 180) * angle;
}
