import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

mixin StaticProvider implements Provider {
  static T of<T>(BuildContext context) =>
      Provider.of<T>(context, listen: false);
}
