import 'dart:math' as math;

import 'package:saool/config/network_endpoints.dart';
import 'package:saool/constants/file_upload_constants.dart';
import 'package:saool/models/responses/upload_file_response.dart';

class CommonUtils {
  static double toRadians(double angle) => (math.pi / 180) * angle;

  static String toHostUrl(String url) =>
      url.isEmpty ? '' : NetworkEndpoints.API_URL + url;

  static bool hasOnlyWhitespace(String str) {
    return str
            .replaceAll(RegExp(r'\u{200e}', unicode: true), '')
            .replaceAll(' ', '')
            .length ==
        0;
  }

  static String? getImageUrlWithFormat(
      {required UploadFileResponse uploadFileResponse,
      required ImageFormat imageFormat}) {
    ImageDetails? imageDetails;
    switch (imageFormat) {
      case ImageFormat.LARGE:
        imageDetails = uploadFileResponse.formats['large'];
        break;
      case ImageFormat.MEDIUM:
        imageDetails = uploadFileResponse.formats['medium'];
        break;
      case ImageFormat.SMALL:
        imageDetails = uploadFileResponse.formats['small'];
        break;
      case ImageFormat.THUMBNAIL:
        imageDetails = uploadFileResponse.formats['thumbnail'];
        break;
    }
    if (imageDetails != null) {
      return toHostUrl(imageDetails.url);
      // return toHostUrl(imageDetails.url + imageDetails.ext);
    }
  }

  static String getNumericalResultImageUrl(double percentage, int index) {
    String url = 'https://saool.io/assets/images/gifs/';
    String ext = '.gif';
    if (percentage <= 30) {
      url += 'low/low';
    } else if (percentage <= 70) {
      url += 'medium/medium';
    } else {
      url += 'high/high';
    }
    url += '$index$ext';
    return url;
  }

  static String getCategoricalResultImageUrl(int index) {
    return 'https://saool.io/assets/images/gifs/wow/wow$index.gif';
  }
}
